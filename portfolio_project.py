# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api


class PortfolioProject(models.Model):
    _name = 'portfolio.project'

    name = fields.Char(string="Name",
                       required=True)
    date = fields.Char(string="Date",
                       required=True)
    location = fields.Char(string="Location",
                           required=True)
    value = fields.Char(string="Value",
                        required=True)
    client = fields.Char(string="Client",
                         required=True)
    category = fields.Char(string="Category",
                           required=True)
    link = fields.Char(string="Link")
    image_ids = fields.Many2many('ir.attachment',
                                 string="Images")
    title = fields.Char(string="Title",
                        required=True)
    description = fields.Text(string="Description",
                              required=True)
    sub_title = fields.Char(string="Sub Title")
    description_sub = fields.Text(string="More Information")
