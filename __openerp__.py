# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'THERMEVO Website Management',
    'version': '1.0',
    'author': 'THERMEVO',
    'website': 'https://THERMEVO.de/',
    'category': 'THERMEVO Website Management',
    'depends': [
        'base',
        'web',
        'website_sale',
        'document',
        'account',
    ],
    'sequence': 8,
    'description': "",
    'data': [
        'data/thermevo_website_group_data.xml',
        'data/robots.xml',

        'views/layout.xml',
        'views/thermevo_website_source.xml',
        'views/thermevo_website_management.xml',
        'views/top_menu.xml',

        'web/home_page.xml',
        'web/home_page_new.xml',
        'web/confirmation.xml',
        'web/login.xml',
        'web/forgot_password.xml',
        'web/sign_in.xml',
        'web/sign_in_verification.xml',
        'web/registration.xml',
        'web/personal_profile.xml',
        'web/company_registered_office.xml',
        'web/company_info_delivary_address.xml',
        'web/employee_info_page.xml',
        'web/banking_detail.xml',
        'web/billing_information.xml',
        'web/order_confirmation.xml',
        'web/messages.xml',
        'web/settings.xml',
        'web/contact_us_new.xml',
        'web/contact_call.xml',
        'web/contact_question.xml',
        'web/about_us.xml',
        'web/legal_stuff.xml',
        'web/privacy_policy.xml',
        'web/calculate_uf_value.xml',
        'web/upload_your_scheme.xml',
        'web/banned.xml',
        'web/403.xml',
        'web/404.xml',
        'web/503.xml',
        'web/foam.xml',
        'web/promo.xml',
        'web/start_new_project.xml',
        'web/portfolio.xml',
        'web/portfolio_page.xml',
        'web/come_work_with_us.xml',
        'web/promo_prize_page.xml',
        'web/thermal_break_community.xml',

        'thermevo_mail_verification_template.xml',
        'promo_page.xml',
        'thermevo_welcome_letter_template.xml',
        'thermevo_trigger_letter.xml',
        'res_config.xml',
        'home_page_slider_views.xml',
        'portfolio_project_views.xml',
        'come_work_with_us_views.xml',

        'security/ir.model.access.csv',
        'security/thermevo_groups.xml',
    ],
    # 'qweb': ['static/src/xml/qweb_thermevo_website.xml'],
    # 'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
