/**
 * Created by tadej on 13.02.16.
 */
$(document).ready(function () {
    $("#foam_get_sample").click(function () {
        var first_name = $("#profile-first-name").val();
        var last_name = $("#profile-surname").val();
        var email = $("#profile-email").val();
        if (email == '' || first_name == '' || last_name == ''){
            $("#error_notif").css('display', 'block');
        }
        if (email == '') {
            $("#foam_profile_email").addClass('error');
            $("#email_error").css('display', 'block');
            $("#profile-email").addClass('error-box');
            return false
        }
        if (email) {
            isValidEmailAddress(email);
            if (isValidEmailAddress(email) == true) {
                $("#foam_profile_email").addClass('error');
                $("#email_error").css('display', 'none');
                $("#profile-email").removeClass('error-box');
            }
            if (isValidEmailAddress(email) == false) {
                $("#foam_profile_email").addClass('error');
                $("#email_error").css('display', 'block');
                $("#profile-email").addClass('error-box');
                return false
            }
        }
        if (first_name == '') {
            $("#foam_profile_first_name").addClass('error');
            $("#first_name_error").css('display', 'block');
            $("#profile-first-name").addClass('error-box');
            return false
        }
        if (first_name) {
            $("#foam_profile_first_name").removeClass('error');
            $("#first_name_error").css('display', 'none');
            $("#profile-first-name").removeClass('error-box');
        }

        if (last_name == '') {
            $("#foam_profile_surname").addClass('error');
            $("#surname_error").css('display', 'block');
            $("#profile-surname").addClass('error-box');
            return false
        }
        if (last_name) {
            $("#foam_profile_surname").removeClass('error');
            $("#surname_error").css('display', 'none');
            $("#profile-surname").removeClass('error-box');
        }

        openerp.jsonRpc("/page/ultimate-thermal-performance-of-aluminium-windows-with-THERMEVO-Evobreak-Pur-FOAM-solutions/get_sample_foam", 'call', {
            'first_name': first_name,
            'last_name': last_name,
            'email': email
        })

    });
});