$(document).ready(function () {

	// store user location
	if($('#location').length > 0){
		$.getJSON("https://jsonip.com/?callback=?", function (data) {
	        openerp.jsonRpc("/page/userlocation", 'call', {
				'ipaddr' : data.ip,
			}).then(function (data) {
				if(data){
					var geocoder;
					geocoder = new google.maps.Geocoder();
					lat = data['lat']
					long = data['long']
					var latlng = new google.maps.LatLng(lat, long);
					geocoder.geocode(
						{'latLng': latlng},
						function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								if (results[0]) {
									var add= results[0].formatted_address ;
									var  value=add.split(",");
									count=value.length;
									country=value[count-1];
									state=value[count-2];
									city=value[count-3];
									document.getElementById("location").value = city +", "+country
								}
							}
						}
					);
				}
			});
	    });
	}

	//uploaded drawing amount validation while submitting to cart
    $('.user_cart.user_product').on('click', function() {
    	var form = $(this).parent('div').parent('form');
    	var quantity = $(this).parent().prev('div').find('.quntity.user.product');
    	var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
    	var preventSubmit = false;
    	var formDiv = $(quantity).parent('div');
    	var qvalue = $(quantity).val();
    	var location = $('#location').val();
    	$(this).parent('div').find('.quotation.user_location').val(location)
    	if(numericReg.test(qvalue)) {
    		if($(quantity).val() < 5000){
    			$(formDiv).addClass('error-length');
    			$(formDiv).removeClass('error');
    			event.preventDefault();
    		}else{
    			$(formDiv).removeClass('error-length');
    			$(formDiv).removeClass('error');
    		}
        }else{
        	$(formDiv).addClass('error');
        	$(formDiv).removeClass('error-length');
        	event.preventDefault();
    	}
    });

    $('.upload_button.delete_user_product').on('click',function(){
    	var href = $(this).attr('href');
    	$('.popup-bg.upload_figure_page').css('display', 'block');
    	$('#delete_popup_for_product').css('display', 'block');
    	$('#delete_popup_for_product').ready(function(){
    		var confirm =  $(this).find('.popup-red-btn.confirmation_yes');
    		var cancel =  $(this).find('.popup-red-btn.confirmation_no');
    		$(confirm).on('click',function(){
    			$('.popup-bg.upload_figure_page').css('display', 'none');
    	    	$('#delete_popup_for_product').css('display', 'none');
    			window.location = href;
    		});
    		$(cancel).on('click',function(){
    			$('.popup-bg.upload_figure_page').css('display', 'none');
    	    	$('#delete_popup_for_product').css('display', 'none');
    		});
    		event.preventDefault();
    	});
    });

	//click on attach file button on upload your scheme page
    $(document).on('click', '.upload_scheme_attach .uploaded_file_close', function(){
      $(this).closest('.attached_files').remove();
      location.reload();
      return false;
    });
    //submit sorting form on ordered goods page
    $('#sort-articles').change(function(){
        $('#sort_ordered_goods_form_id').submit();
    });

    //submit sorting form on issued invoice page
    $('#sort-articles').change(function(){
        $('#sort_issued_invoice_form_id').submit();
    });

    $('#order-articles').change(function(){
        $('#order_issued_invoice_form_id').submit();
    });

    //submit sorting form on issued invoice page
    $('#sort-articles').change(function(){
        $('#sort_delivery_order_form_id').submit();
    });

    $('#order-articles').change(function(){
        $('#order_delivery_order_form_id').submit();
    });

    //submit sorting form on list of order page
    $('#sort-articles').change(function(){
        $('#sort_list_of_order_form_id').submit();
    });

    //submit sorting form on price quotation page
    $('#sort-articles').change(function(){
        $('#sort_price_quotation_form_id').submit();
    });

    //submit sorting form on price quotation page
    $('#order-articles').change(function(){
        $('#order_price_quotation_form_id').submit();
    });

    //submit sorting form on price quotation page
    $('#sort-articles-uploaded-drawings').change(function(){
        $('#sort_drawing_data').submit();
    });

	//check vat verification on banking detail page
    $(".adress-form").find('#vat_number').on('change', function() {
         if($(this).val()){
                openerp.jsonRpc("/page/delivery_address/checkvat", 'call', {
                    'vat' :$(this).val(),
                }).then(function (data) {
                    if(!data){
                        $('#vat_verify').val(data);
                        $('#vat_verify').parent().addClass("error");
                        $("#error_from_check_vat").css("display","block")
                    }else{
                        $('#vat_verify').val(data);
                        $('#vat_verify').parent().removeClass("error");
                        $("#error_from_check_vat").css("display","none")
                    }
                });
            }
    });
    $('#banking_detail_form').submit(function(){
        var vat_verify_field = $('#vat_verify').val();
        if (vat_verify_field =='true'){
            return true
        }
        if (vat_verify_field =='false'){
            event.preventDefault();
        }
		return true;
	});

    $('.add-adress').on('click', function() {
      $('.adress-form #shipping_zip').val('');
      $('.adress-form #country_delivery').val('');
      $('.adress-form #shipping_city').val('');
      $('.adress-form #shipping_street').val('');
      $('.adress-form #new_address').val('Create new Address');
      $('.adress-form #link_partner').val('');
      return false;
    });
    $('.list-adresses-links a').on('click', function() {
      var partner_id = $(this).attr('data-shipping_partner_id');
      var postcode = $(this).attr('data-shipping_zip');
      var country  = $(this).attr('data-shipping_country_id');
      var city     = $(this).attr('data-shipping_city');
      var adress   = $(this).attr('data-shipping_street');
      var title    = $(this).text();
      var shipping_create_date=$('#shipping_create_date').val();
      $('.adress-form #shipping_partner_id').val(partner_id);
      $('.adress-form #link_partner').val('Edit old Address');
      $('.adress-form #new_address').val('');
      $('.adress-form #shipping_zip').val(postcode);
      $('.adress-form #country_delivery').val(country);
      $('.adress-form #shipping_city').val(city);
      $('.adress-form #shipping_street').val(adress);
      $('.list-adresses-links a').removeClass('active');
      $('.adress-form h6').html(title+"<br />"+shipping_create_date);
      $(this).addClass('active');
      return false;
    });

        //click on attach file button on contact call page
        $(document).on('click', '.contact_call_attach .uploaded_file_close', function(){
          $(this).closest('.attached_files').remove();
          location.reload();
          return false;
        });

        //submit message form on change
		$('#sort-messages').change(function(){
			$('#message_form_id').submit();
		});

		openerp.jsonRpc("/page/messages/check_unread_messages", 'call', {
            }).then(function (data) {
            	manageMessageCounts(data);
            });

        $('form.js_add_cart_variants input[type="radio"]').on('change', function() {
        var attribute_value = $('input[type=radio]:checked').map(function(_, el) {
                return $(el).val();
            }).get();
            var get_value=attribute_value.toString().split(",");
            $('#taken_id').val(get_value[0]);
        });

         //Advance Filter Button
         $('#apply_filter').on('click', function (e) {
            e.preventDefault();
            $("#loader_image").css("display","block");
            var max=$('#id2_max_width').val();
            if(max == '')
            {
               var max_place=$('#id2_max_width').attr('placeholder');
               $('#maximum_width').val(max_place);
            }
            else
            {
              $('#maximum_width').val(max);
            }
            var min=$('#id1_min_width').val();
            if(min == '')
            {
               var min_place=$('#id1_min_width').attr('placeholder');
               $('#minimu_width').val(min_place);
            }
            else
            {
               $('#minimu_width').val(min);
            }
            var minimu_width=$('#minimu_width').val();
            var maximum_width=$('#maximum_width').val();
            var category_id=$('#category_id').val();
            if(category_id === undefined){
                category_id="not_category";
            }
            var foam_check=$('#with-foam').is(':checked')
            var product_data = $('.geometry-item input[type=checkbox]:checked').map(function(_, el) {
                return $(el).val();
            }).get();
            var query_param={'minimu_width':minimu_width,
                    'maximum_width':maximum_width,
                    'category_id':category_id,
                    'product_data':product_data,
                    'foam_check':foam_check}
            var display_json_query=jQuery.param(query_param);

            openerp.jsonRpc("/shop/advance_search_filter", 'call', {
                    'minimu_width':minimu_width,
                    'maximum_width':maximum_width,
                    'category_id':category_id,
                    'product_data':product_data,
                    'foam_check':foam_check,
                    }).then(function (data) {
                        $("#loader_image").css("display","none");
                        list_length=JSON.stringify(data['length']);
                        var poroduct_attach = "";
                        if('products' in data)
                        {
                            $( ".list-products" ).empty();
                            var prodcut_info=data['products'];
                            var category_name=data['prod_cat'];
                            var cat_info=data['cat_info'];
                            var prod_info=JSON.stringify(data['prod_info']);
                            var info_product=data['prod_info'];
                            $("#loader_image").css("display","none");
                            $.each( JSON.parse(prod_info), function( id_product, id_cat){
                                 poroduct_attach = '<div class="product">';
                                 poroduct_attach += '<div class="oe_list oe_product_cart" data-publish="';
                                 poroduct_attach += get_website_published(prodcut_info,id_product,list_length);
                                 poroduct_attach += " and 'on' or 'off'>";
                                 poroduct_attach += '<div class="product" itemscope="itemscope" itemtype="https://schema.org/Product">';
                                 poroduct_attach += '<a itemprop="url" href="'+"/shop/product/";
                                 poroduct_attach += get_fullname(prodcut_info,id_product,list_length);
                                 poroduct_attach += '">';
                                 poroduct_attach += '<span class="img"><img itemprop="image"  alt="ttt"  class="img img-responsive" src="/website/image/product.template/';
                                 poroduct_attach +=id_product;
                                 poroduct_attach +='/image/300x300';
//                               poroduct_attach +=  get_image(prodcut_info,id_product,list_length);
                                 poroduct_attach += '"/></span>';
                                 poroduct_attach += '<span class="title">';
                                 poroduct_attach += get_main_category(id_product,id_cat,cat_info,list_length,prodcut_info,info_product);
                                 poroduct_attach += '</span>';
                                 poroduct_attach += '<span class="desc">';
                                 poroduct_attach +=  get_category_info(id_product,id_cat,cat_info,list_length,prodcut_info,info_product);
                                 poroduct_attach += get_product_name(prodcut_info,id_product,list_length);
                                 poroduct_attach += '</span>';
                                 poroduct_attach +='<span class="article">Artikel ';
                                 poroduct_attach += get_article_name(prodcut_info,id_product,list_length);
                                 poroduct_attach += "</span></a></div></div></div>";
                                 $(".list-products").append(poroduct_attach);
                            });
                        }
                        if('error' in data){
                            if(data['error'] == 'no_product'){
                                $( ".list-products" ).empty();
                                poroduct_attach = '<div id="cabinet-container">';
                                poroduct_attach += '<div id="not_available" class="message-error">';
                                poroduct_attach += '<div class="close"></div>';
                                poroduct_attach += 'No product available';
                                poroduct_attach += '</div></div>';
                                $(".list-products").append(poroduct_attach);
                            }
                        }
                    });
                    location.hash = display_json_query;
        });

        //save notification
        $('#save_notification').on('click', function (e) {
            var chk_fname=$( "#chk_first_name_sign" ).is(':checked');
            var chk_sname=$("#chk_surname_sign" ).is(':checked');
            var chk_company=$("#chk_company_sign" ).is(':checked');
            var new_post=$("#new_post_chk" ).is(':checked');
            var new_comment=$("#new_comment_chk" ).is(':checked');
            var already_comment=$("#already_comment_chk" ).is(':checked');
            if(new_post){$('#new_post_chk').val('True');}
            if(new_comment){$('#new_comment_chk').val('True');}
            if(already_comment){$('#already_comment_chk').val('True');}
        });

    //Set signature when input field's value is changed
    $('#save_signature').on('click', function (e) {
        var chk_fname=$( "#chk_first_name_sign" ).is(':checked');
        var chk_sname=$("#chk_surname_sign" ).is(':checked');
        var chk_company=$("#chk_company_sign" ).is(':checked');
        if(chk_fname){$('#chk_first_name_sign').val('True');}
        if(chk_sname){$('#chk_surname_sign').val('True');}
        if(chk_company){$('#chk_company_sign').val('True');}
    });

    $('.chk_sign').bind("change",function () {
        $('#sample_signature').removeAttr('value');
        var first_name_sign = $("#first_name_sign").val();
        var surname_sign=$("#surname_sign").val();
        var company_sign=$("#company_sign").val();

        $('#signature-settings-form').each(function(i, obj){
            var attach_sign="";
            var chk_fname=$( "div.form-item" ).find( "#chk_first_name_sign" ).is(':checked');
            var chk_sname=$( "div.form-item" ).find( "#chk_surname_sign" ).is(':checked');
            var chk_company=$( "div.form-item" ).find( "#chk_company_sign" ).is(':checked');

            if(chk_fname){attach_sign=first_name_sign;}
            if(chk_sname){attach_sign += ' '+surname_sign;}
            if(chk_company){attach_sign += ','+company_sign;}
            $('#sample_signature').attr('value', attach_sign);
        });
    });

    $('#first_name_sign').on('keyup', function (e){
       var first_name_sign = $("#first_name_sign").val();
       var surname_sign=$("#surname_sign").val();
       var company_sign=$("#company_sign").val();
       $('#signature-settings-form').each(function(i, obj){
            var attach_sign="";
            var chk_fname=$( "div.form-item" ).find( "#chk_first_name_sign" ).is(':checked');
            var chk_sname=$( "div.form-item" ).find( "#chk_surname_sign" ).is(':checked');
            var chk_company=$( "div.form-item" ).find( "#chk_company_sign" ).is(':checked');

            if(chk_fname){attach_sign=first_name_sign;}
            if(chk_sname){attach_sign += ' '+surname_sign;}
            if(chk_company){attach_sign += ','+company_sign;}
            $('#sample_signature').attr('value', attach_sign);
       });
    });

    $('#surname_sign').on('keyup', function (e){
       var first_name_sign = $("#first_name_sign").val();
       var surname_sign=$("#surname_sign").val();
       var company_sign=$("#company_sign").val();
       $('#signature-settings-form').each(function(i, obj){
            var attach_sign="";
            var chk_fname=$( "div.form-item" ).find( "#chk_first_name_sign" ).is(':checked');
            var chk_sname=$( "div.form-item" ).find( "#chk_surname_sign" ).is(':checked');
            var chk_company=$( "div.form-item" ).find( "#chk_company_sign" ).is(':checked');

            if(chk_fname){attach_sign=first_name_sign;}
            if(chk_sname){attach_sign += ' '+surname_sign;}
            if(chk_company){attach_sign += ','+company_sign;}
            $('#sample_signature').attr('value', attach_sign);
       });
    });

    $('#company_sign').on('keyup', function (e){
       var first_name_sign = $("#first_name_sign").val();
       var surname_sign=$("#surname_sign").val();
       var company_sign=$("#company_sign").val();
       $('#signature-settings-form').each(function(i, obj){
            var attach_sign="";
            var chk_fname=$( "div.form-item" ).find( "#chk_first_name_sign" ).is(':checked');
            var chk_sname=$( "div.form-item" ).find( "#chk_surname_sign" ).is(':checked');
            var chk_company=$( "div.form-item" ).find( "#chk_company_sign" ).is(':checked');

            if(chk_fname){attach_sign=first_name_sign;}
            if(chk_sname){attach_sign += ' '+surname_sign;}
            if(chk_company){attach_sign += ','+company_sign;}
            $('#sample_signature').attr('value', attach_sign);
       });
    });

    //Show delete button when quantity is changed to zero or one-----
    $(".cart_item_buttons").bind("keyup", function() {
		var value = $(this).find('#quantity').val();
		$(".cart_item_buttons").removeClass("test");
		$(this).addClass("test");
		if($.isNumeric(value) != true){
			$("#error_from_input_length").css("display","none")
            $("#error_from_input_text").css("display","block")
		}
		if (value==0 || value==1) {
			$(this).find('#delete').show();
			$(this).find('#update').show();
		}else if(value>0){
			$(this).find('#delete').hide();
			$(this).find('#update').show();
		}
    });

	// Product cart page --> Update product quantity validation
  	$('.test_update').on('click', function () {
		var quantity = $(this).parent().find("#quantity").val();
		var minimumQty = $(this).siblings('.product.amount.product_minimum_amount').val();
		minimumQty = (minimumQty == '') ? '0' : minimumQty
        if($.isNumeric(quantity)==true){
            if(quantity >= parseInt(minimumQty)){
                $("#error_from_input_length").css("display","none")
            }
            else{
                $("#error_from_input_text").css("display","none")
                $("#error_from_input_length").css("display","block")
                $("#minimum_product_amount").html(minimumQty);
                return false
            }
        }else{
            $("#error_from_input_length").css("display","none")
            $("#error_from_input_text").css("display","block")
            return false
        }
		var sale_order_line =  $(this).parent().find("#line_name").val();
		var button_ref = $(this)
		openerp.jsonRpc("/page/test/quantity", 'call', {
		    'quantity':quantity,
		    'sale_order_line' : sale_order_line,
	    }).then(function (data) {
		    var quantity_input = $(button_ref).siblings('#quantity');
		    var update_btn = $(button_ref);
		    $(update_btn).css("display","none")
		    $(quantity_input).val(data)
		    $("#error_from_input_length").css("display","none")
            $("#error_from_input_text").css("display","none")
		});
	});

  	//To get all current values of product where delete button is appeared------
    $('#delete_action').on( 'click', '.cart_item_buttons #delete', function() {
        var product_id = $(this).parent(".cart_item_buttons").find('#product_id').val();
        var line_id = $(this).parent(".cart_item_buttons").find('#line_id').val();
        var qty = $(this).parent(".cart_item_buttons").find('#quantity').val();
        $('#product_id').val(product_id)
        $('#line_id').val(line_id)
        $('#qty').val(qty)
    });

	//Prevent cart to proceed if there is any error
    $('#move_to_delivery_address').on('click', function(){
    	if($('#error_from_input_length').css('display') == 'block' || $('#error_from_input_text').css('display') == 'block'){
    		event.preventDefault()
    	}
	});

    //Enable Confirm the Quotation button
    $('.checkboxes input[type=checkbox]').bind("change",function () {
    	checkedTotal = 0;
    	$('.checkboxes input[type=checkbox]').each(function () {
    		if(this.checked){
    			checkedTotal = checkedTotal + 1;
    		}
    	});
    	if(checkedTotal == 3){
    		$('form.margintop60.avoid-this.commercial-page-form input[type=submit]').removeAttr("disabled");
    	}else{
    		$('form.margintop60.avoid-this.commercial-page-form input[type=submit]').attr("disabled","disabled")
    	}
    });

    $('#annual_total').on('keyup', function (e){
       var annual_total = $("#annual_total").val();
       $('.confirm_total_count_gray.annual_class').attr('value', annual_total);
    });

    // On entering string in amount field gives error
    $(".block_filtr").find('#check_int').on('keyup', function() {
        var quantity = $("#check_int").val();
        var minimumQuantity = parseInt($('.amount.product_minimum_amount').html());
        if($.isNumeric(quantity) != true){
            $(this).parent('div').addClass('error');
            $("#error_from_input_text").css("display","block")
            $("#error_from_input_length").css("display","none")
            $('#check_int').parent('div').addClass('error');
            $('#product_cart_error_message').removeClass('hidden');
            return;
            event.preventDefault();
        }else{
			if(quantity >= minimumQuantity){
				$("#error_from_input_length").css("display","none")
				$('#check_int').parent('div').removeClass('error');
				prod_id = $("input[id='product_id']").attr('value');
			}else{
				$("#error_from_input_length").css("display","block")
				$("#error_from_input_text").css("display","none")
				$('#check_int').parent('div').addClass('error');
				$('#product_cart_error_message').removeClass('hidden');
				return;
			}
			$("#error_from_input_text").css("display","none")
		}
        if(quantity == ''){
        	$(this).parent('div').removeClass('error');
            $("#error_from_input_text").css("display","none")
        }
    });


    // On adding product into cart perform validation
    $('#add_cart_pro button[type=button]').on('click', function (){
    	var buttonName = $(this).attr('name');
    	if(buttonName == 'redirect_catalog_button'){
    		$('#redirect_catalog').val('True');
    	}
    	processProductData();
    });
	function processProductData(){
		var src_img = $(".image_pro.displayed img").attr("src");
		var res= src_img.split("/")
		image_selected = res[res.length-1]
		$('#p_id').val(image_selected);
		var quantity = $("#check_int").val();
		var minimumQuantity = parseInt($('.amount.product_minimum_amount').html());
		if ($.isNumeric(quantity)==true){
			if(quantity >= minimumQuantity){
				$("#error_from_input_length").css("display","none")
				$('#check_int').parent('div').removeClass('error');
				prod_id = $("input[id='product_id']").attr('value');
			}else{
				$("#error_from_input_length").css("display","block")
				$("#error_from_input_text").css("display","none")
				$('#check_int').parent('div').addClass('error');
				$('#product_cart_error_message').removeClass('hidden');
				return;
			}
			$("#error_from_input_text").css("display","none")
			prod_id = $("input[id='product_id']").attr('value');
		}else{
            $('#check_int').parent('div').addClass('error');
            $("#error_from_input_text").css("display", "block")
            $("#error_from_input_length").css("display", "none")
            $('#product_cart_error_message').removeClass('hidden');
            return;
        }

        product_name = $("#product_name").val();
		prod_new_id = $("#prod_new_id").val();
		attribute_id = $("#attribute_id").val();
		attribute_line_id = $("#attribute_line_id").val();
		custom_var = $("#custom_var").val();
		var numCondition = /^[0-9.,]+$/;
		if (custom_var){
			if (numCondition.test(custom_var)==true){
				$('#custom_var').parent('div').removeClass('error');
				$("#length_not_permitted").css('display', 'none');
				$("#error_from_input_text_length").css("display","none");
			}else{
				$('#custom_var').parent('div').addClass('error');
				$("#length_not_permitted").css('display', 'none');
				$("#error_from_lenth_already_exist").css('display', 'none');
				$("#error_from_input_text_length").css("display","block");
				$('#product_cart_error_message').removeClass('hidden');
				return;
			}
		}
		var product_data = [];
		$(".product_data").each(function(){
			product_data.push($(this).val());
		});
		if($('#custom_var').length > 0 && $('#custom_var').val()){
			length_value = parseFloat($('#custom_var').val())
			min_value = parseFloat($('.min_length.length').html());
			max_value = parseFloat($('.max_length.length').html());
			var listItems = $('.list-unstyled.js_add_cart_variants.nav-stacked > li');
			var isDuplicate = false;
			$(listItems).each(function(index) {
			    if($(this).hasClass('form-group js_attribute_value')){
			    	var length = parseFloat($(this).find('span.length_value').html())
			    	if(length_value == length){
			    		$('#custom_var').parent('div').addClass('error');
						$("#error_from_lenth_already_exist").css('display', 'block');
						$('#product_cart_error_message').removeClass('hidden');
						isDuplicate = true;
						return false;
			    	}
			    }
			});
			if(isDuplicate){
				$("#error_from_lenth_is_big").css('display', 'none');
				$("#error_from_lenth_is_small").css('display', 'none');
				return;
			}
			if(length_value < min_value){
				$('#custom_var').parent('div').addClass('error');
				$("#error_from_lenth_is_big").css('display', 'none');
				$("#error_from_lenth_is_small").css('display', 'block');
				$('#product_cart_error_message').removeClass('hidden');
				$("#error_from_lenth_already_exist").css('display', 'none');
				return
			}else if(length_value > max_value){
				$('#custom_var').parent('div').addClass('error');
				$("#error_from_lenth_is_small").css('display', 'none');
				$("#error_from_lenth_is_big").css('display', 'block');
				$('#product_cart_error_message').removeClass('hidden');
				$("#error_from_lenth_already_exist").css('display', 'none');
				return
			}

			openerp.jsonRpc("/shop/cart/update_custom_profile_length", 'call', {
				'product_data':product_data,
			}).then(function (data, product_form) {
				if('error' in data){
					if(data['error'] == 'length_not_permitted'){
						$('#custom_var').parent('div').addClass('error');
						$("#length_not_permitted").css('display', 'block');
						$('#product_cart_error_message').removeClass('hidden');
						event.preventDefault();
					}else{
						$('#add_cart_pro').submit();
					}
				}else{
					$('#add_cart_pro').submit();
				}
			});
		}else{
			$('#add_cart_pro').submit();
		}
	}
});

//included when create login and registration page
$(document).ready(function () {
  var error_data = window.location.search.substring(1).split("=");
  var error_dict = {};
  error_dict[error_data[0]] = error_data[1];
  if(error_dict[error_data[0]] == "valid")
  {
      document.getElementById("note_invoice_msg").style.display = "block";
      document.getElementById("not_valid_msg").style.display = "none";
  }
  else if(error_dict[error_data[0]] == "invalid"){
    document.getElementById("not_valid_msg").style.display = "block";
  }
  else if(error_dict[error_data[0]] == "saved"){
      document.getElementById("profile_save_msg").style.display = "block";
  }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profile_image').attr('src', e.target.result);
           /* var data = $this.parent('.profile-picture').find('#profile_image').val();
            alert(data)*/
            //alert(e.target.result)
            $('#profile_image').attr('value', e.target.result);
         //   $('#fileInput').attr('value', e.target.result);
            $('#hidn_fld').attr('value', e.target.result);
          /*  $('#profile_image').val(e.target.result);*/
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#fileInput").change(function(){
    readURL(this);
});

function chooseFileContactcall() {
  $("#fileInput_contact_call").click();
  event.preventDefault()
}
function readURLContactcall(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#hidn_call_fld').attr('value', e.target.result);
            var filename = $('input[type=file]').val().split('\\').pop();
            $('#hidn_file_name').val(filename);
             $(".file_name_box").css('display', 'block');
             $(".uploaded_file_close").css('display', 'block');
             $(".file_name_box").html(filename);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#fileInput_contact_call").change(function(){
    readURLContactcall(this);
});

function chooseFileUploadyourscheme() {
  $("#fileInput_uploadyourscheme_call").click();
  event.preventDefault()
}
function readURLUploadSchemecall(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#hidn_scheme_fld').attr('value', e.target.result);
            var filename = $('input[type=file]').val().split('\\').pop();
            file_type = filename.split('.');
            file_extension = file_type.pop();
            if(file_extension != "dxf" && file_extension != "dwg")
              {
              $("#popup_bg").css('display', 'block');
              $("#contacts2_popup_container_upload_scheme").css('display', 'block');
              }
            if(file_extension === undefined)
              {
              $("#popup_bg").css('display', 'block');
              $("#contacts2_popup_container_upload_scheme").css('display', 'block');
              }
             $('#hidn_file_name').val(filename);
             $(".file_name_box").css('display', 'block');
             $(".uploaded_file_close").css('display', 'block');
             $(".file_name_box").html(filename);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#fileInput_uploadyourscheme_call").change(function(){
    readURLUploadSchemecall(this);
});
$('.upload_scheme_ok').click(function(){
    $("#popup_bg").css('display', 'none');
    $("#contacts2_popup_container_upload_scheme").css('display', 'none');
    $("#error_from_input_file_uploadyourscheme").css('display', 'none');
    $("#error_for_access_level2").css('display', 'none');
    location.reload();
});
$('.second_upload_scheme_upload_button').click(function(){
    var quantity = $(this).parent().parent().find("#fileInput_uploadyourscheme_call").val();
    if (quantity == "") {
        $("#popup_bg").css('display', 'block');
        $("#error_from_input_file_uploadyourscheme").css('display', 'block');
    } else if (quantity != ""){
        $('.upload_preload_data_start').css('display', 'block');
    }
});

$('.upload_scheme_upload_button').click(function(){
    var quantity = $(this).parent().parent().find("#fileInput_uploadyourscheme_call").val();
    if (quantity == "") {
        $("#popup_bg").css('display', 'block');
        $("#error_from_input_file_uploadyourscheme").css('display', 'block');
    } else if (quantity != ""){
        $('.upload_preload_data_start').css('display', 'block');
    }
});

$('.red-btn-link.uploadscheme.public').click(function(){
	$("#popup_bg").css('display', 'block');
	$("#error_for_access_level2").css('display', 'block');
});
//profile image is not edited then retain original image value
$(document).ready(function () {
var width_current_value;
var height_current_value;
var article_id;
  if(!$('#hidn_fld').attr('value')){
    var image_value = $('#profile_image').attr('value');
    $("#hidn_fld").attr('value',image_value);
  }
//code for show current url in FAQ page popup
    $('.get_link').click(function(){
            current_url = window.location.href;
            var pre_url = current_url.slice("#");
            var new_url = pre_url.split('#')[0];
            document.getElementById("link_question_dialogue").innerHTML = new_url + '#' + $(this).parents(".faq_item").attr("id");
            /*$("#link_question_dialogue").dialog({
                show : "box",
                title: "Title",
                hide : "slide",
                closeText: "Close",
                modal: true,
                buttons :  {
                Close: function() {
                    $(this).dialog('close');
                }
            }
        });*/
    });

    if(window.location.href.indexOf("faq_question_") > -1){
        url = window.location.href;
        var index = url.indexOf('#faq');
        var question_string = url.substring(index);
        que_obj = $(question_string).find('.reply');
        $(que_obj).removeAttr("style");
        $(que_obj).css("display", "inline-block");
    //    $(que_obj).css("overflow", "inherit");
    }
//code for matrix on product page

    $(".matrix-label").click(function (){
           var tableBody = $(this).parent().parent().parent();
           var currentTr = $(this).parent();
           var current_id = $(currentTr).find("input").attr("id");

           var split_current_id = current_id.split("*");
           var current_width = split_current_id[0];
           var current_height_id = split_current_id[1];
           var current_height = $(".custom-name-"+current_height_id).attr("value");
           $("#length").val(current_height);
           $("#width").val(current_width);
           $(tableBody).find("label.matrix-label-checked").removeClass("matrix-label-checked");
           $(this).addClass("matrix-label-checked");

    });


    $(".matrix-label-pe").click(function (){
           var tableBody = $(this).parent().parent().parent();
           var currentTr = $(this).parent();
           var current_id = $(currentTr).find("input").attr("id");

           var split_current_id = current_id.split("*");
           var current_width = split_current_id[0];
           var current_height_id = split_current_id[1];
           var current_height = $(".custom-name-pe-"+current_height_id).attr("value");
           $("#length").val(current_height);
           $("#width").val(current_width);
           $(tableBody).find("label.matrix-label-checked").removeClass("matrix-label-checked");
           $(this).addClass("matrix-label-checked");

    });

    $(".matrix-label-pur").click(function (){
           var tableBody = $(this).parent().parent().parent();
           var currentTr = $(this).parent();
           var current_id = $(currentTr).find("input").attr("id");

           var split_current_id = current_id.split("*");
           var current_width = split_current_id[0];
           var current_height_id = split_current_id[1];
           var current_height = $(".custom-name-pur-"+current_height_id).attr("value");
           $("#length").val(current_height);
           $("#width").val(current_width);
           $(tableBody).find("label.matrix-label-checked").removeClass("matrix-label-checked");
           $(this).addClass("matrix-label-checked");

    });


    $("#lange_12").click(function (){
            $("#product_matrix_id").slideDown();
     });
     $("#lange_13").click(function (){
            $("#product_matrix_id").slideUp();
            var length_matrix = $('#length').val();
            var width_matrix = $('#width').val();
            $('#length').removeAttr('value');
            $('#width').removeAttr('value');
        });

    $("div.demo-1").each(function (){
       var width_id_show = $(this)
       var current_id = $(width_id_show).find("input").attr("id");
       width_current_value = current_id.split(".");
       width_current_value = width_current_value[0]
    });
    $("div.demo-2").each(function (){
       var height_id_show = $(this)
       var current_id = $(height_id_show).find("input").attr("id");
       height_current_value = current_id.split(".");
       height_current_value = height_current_value[0]
    });
    $("div.demo-3").each(function (){
       var article_id_show = $(this)
       article_id = $(article_id_show).find("input").attr("id");
       var article_id_split = article_id.toString().split('');
       article_id = article_id_split[0]+article_id_split[1]+article_id_split[2]
    });
    $("#lange_4").click(function (){
            openerp.jsonRpc("/web/session/get_session_info", 'call',{
            }).then(function(data){
                if(data.uid == null){
                }else{
                    $("#display_other_length").slideDown();
                }
            });
     });

    $("input.js_variant_change").click(function (){
        $('#custom_var').parent('div').removeClass('error');
        $("#error_from_input_text_length").css('display', 'none');
        $("#length_not_permitted").css('display', 'none');
        $("#error_from_lenth_is_big").css('display', 'none');
        $("#error_from_lenth_is_small").css('display', 'none');
        $("#error_from_lenth_already_exist").css('display', 'none');
        $('#custom_var').removeAttr('value');
        $("#display_other_length").slideUp();
    });

    $("input.js_variant_change").each(function (){
       var input_product_length_show = $(this)
       input_product_length_name_show = $(input_product_length_show).attr("name");
       $("#lange_4").attr('name',input_product_length_name_show);

    });

    //code for read/unread message on message page.
    $("input.check_unread_mail").click(function (){
        var input_message_id_show = $(this)
        input_message_mail_id__show = $(input_message_id_show).attr("id");
        if($(this).parent().parent().hasClass('not-read'))
        {
        openerp.jsonRpc("/page/messages/read_message", 'call', {
            'mail_id' :input_message_mail_id__show,

            }).then(function (data) {
            	manageMessageCounts(data);
            });
            $(this).closest('div.message-row').removeClass('not-read')
        }
        else
        {
        openerp.jsonRpc("/page/messages/unread_message", 'call', {
            'mail_id' :input_message_mail_id__show,

            }).then(function (data) {
            	manageMessageCounts(data);
            });
        $(this).closest('div.message-row').addClass('not-read')
        }
    });
    $("input.check_read_mail").click(function (){
        var input_message_id_show = $(this)
        input_message_mail_id__show = $(input_message_id_show).attr("id");
        if($(this).parent().parent().hasClass('not-read'))
        {
        openerp.jsonRpc("/page/messages/read_message", 'call', {
            'mail_id' :input_message_mail_id__show,
            }).then(function (data) {
            	manageMessageCounts(data);
            });
        $(this).closest('div.message-row').removeClass('not-read')
        }
        else
        {
        openerp.jsonRpc("/page/messages/unread_message", 'call', {
            'mail_id' :input_message_mail_id__show,
            }).then(function (data) {
            	manageMessageCounts(data);
            });
            $(this).closest('div.message-row').addClass('not-read')
        }
    });

// code for display pdf & dwg buttons on paroduct page.
$('#adhsiv1').click(function() {
$(".image_pro").removeClass("displayed");
   $('#have_glue').val('True');
   if($('#lange_12').is(':checked')) {
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_foam_adhesive").show();
            $("#pdf_foam_adhesive").show();

            $('#with_adhesive_with_form').show();
            $("#with_adhesive_with_form .image_pro").addClass("displayed");
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();

    }else if($('#lange_13').is(':checked')) {
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_adhesive").show();
            $("#pdf_adhesive").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').show();
            $("#no_form_with_adhesive .image_pro").addClass("displayed");

    }/*else{
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_adhesive").show();
            $("#pdf_adhesive").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').show();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();
    }*/
});
$('#adhsiv2').click(function() {
    $(".image_pro").removeClass("displayed");
	$('#have_glue').val('');
   if($('#lange_12').is(':checked')) {
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").show();
            $("#pdf_foam").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').show();
            $("#no_adhesive_with_form .image_pro").addClass("displayed");
            $('#no_form_with_adhesive').hide();

    }else if($('#lange_13').is(':checked')) {
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_product").show();
            $("#pdf_product").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').show();
            $("#no_adhesive_no_form .image_pro").addClass("displayed");
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();

    }/*else{
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_product").show();
            $("#pdf_product").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').show();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();
    }*/
});
$('#lange_12').click(function() {
$(".image_pro").removeClass("displayed")
   if($('#adhsiv1').is(':checked')) {
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_foam_adhesive").show();
            $("#pdf_foam_adhesive").show();

            $('#with_adhesive_with_form').show();
            $("#with_adhesive_with_form .image_pro").addClass("displayed");
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();

    }else if($('#adhsiv2').is(':checked')) {
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_foam").show();
            $("#pdf_foam").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').show();
            $("#no_adhesive_with_form .image_pro").addClass("displayed");
            $('#no_form_with_adhesive').hide();

}
});
$('#lange_13').click(function() {
$(".image_pro").removeClass("displayed")
   if($('#adhsiv1').is(':checked')) {
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_product").hide();
            $("#pdf_product").hide();
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_adhesive").show();
            $("#pdf_adhesive").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').hide();
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').show();
            $("#no_form_with_adhesive .image_pro").addClass("displayed");

    }else if($('#adhsiv2').is(':checked')) {
            $("#dwg_foam_adhesive").hide();
            $("#pdf_foam_adhesive").hide();
            $("#dwg_foam").hide();
            $("#pdf_foam").hide();
            $("#dwg_adhesive").hide();
            $("#pdf_adhesive").hide();
            $("#dwg_product").show();
            $("#pdf_product").show();

            $('#with_adhesive_with_form').hide();
            $('#no_adhesive_no_form').show();
            $("#no_adhesive_no_form .image_pro").addClass("displayed");
            $('#no_adhesive_with_form').hide();
            $('#no_form_with_adhesive').hide();
}
});

var article_width_height_value = article_id+width_current_value+height_current_value ;

    //code for password check on user profile setting page
    var password1       = $('#new_password'); //id of first password field
    var password2       = $('#confirm_pwd'); //id of second password field
    var passwordsInfo   = $('#valid_password'); //id of indicator element
    var passwordscheckInfo   = $('#check_password'); //id of indicator element
    var passwordsInfoError = $('#pass-info'); //id of indicator element

    passwordStrengthCheck(password1,password2,passwordsInfo,passwordscheckInfo); //call password check function


$( "#change-password-form" ).submit(function( event ) {

  if(passwordsInfo.hasClass('error'))
            {
                passwordsInfoError.html("Enter Valid password");
                return false;
            }
});


 $("#send_email_commercial_page").click(function (){
            var sale_order = $("#sale_order").val();

           openerp.jsonRpc("/page/commercial_proposal/invoice_send", 'call', {
                    'sale_order':sale_order
            }).then(function (data) {
            $("#send_email_commercial_page_success_msg").css('display', 'block');
            });
     });

 $("#send_email_billing_information_page").click(function (){
            var sale_order = $("#sale_order").val();
           openerp.jsonRpc("/page/billing_information/send_mail", 'call', {
            'sale_order':sale_order
            }).then(function (data) {
            $("#send_email_billing_information_page_success_msg").css('display', 'block');
            });
     });
 $("#send_email_order_confirmation_page").click(function (){
            var sale_order = $("#sale_order").val();
           openerp.jsonRpc("/page/order_confirmation/invoice_send", 'call', {
            'sale_order':sale_order
            }).then(function (data) {
            $("#send_email_order_confirmation_page_success_msg").css('display', 'block');
            });
     });

     //code for save data form cotact questions page
     $("#save_contact_questions_data").click(function(){
                var first_name = $("#text1").val();
                var last_name = $("#text2").val();
                var company = $("#text3").val();
                var phone = $("#text4").val();
                var email= $("#email1").val();
                var subject = $("#select1").val();
                var messege = $("#textarea1").val();
                var filter = /^[0-9-+]+$/;

                if(first_name == '')
                {
                     $("#contact_question_first_name_id").addClass('error');
                     return false
                }
                if(first_name)
                {
                     $("#contact_question_first_name_id").removeClass('error');
                }
                if(last_name == '')
                {
                     $("#contact_question_last_name_id").addClass('error');
                     return false
                }
                if(last_name)
                {
                     $("#contact_question_last_name_id").removeClass('error');
                }
                if(company == '')
                {
                     $("#contact_question_company_id").addClass('error');
                     return false
                }
                if(company)
                {
                     $("#contact_question_company_id").removeClass('error');
                }
                if(phone == '')
                {
                     $("#contact_question_phone").addClass('error');
                     return false
                }
                if(phone)
                {
                    if (filter.test(phone)) {
                    $("#contact_question_phone").removeClass('error');
                    }
                    else {
                    $("#contact_question_phone").addClass('error');
                        return false;
                    }
                }
                if(email == '')
                {
                     $("#contact_question_email").addClass('error');
                     return false
                }
                if(email)
                {
                isValidEmailAddress(email)
                if (isValidEmailAddress(email) == true)
                {
                }
                if (isValidEmailAddress(email) == false)
                {
                     $("#contact_question_email").addClass('error');
                     return false
                }
                }
                if(messege == '')
                {
                     $("#contact_question_message").addClass('error');
                     return false
                }
                if(messege)
                {
                     $("#contact_question_message").removeClass('error');
                }

            openerp.jsonRpc("/page/contact_question/save_data", 'call', {
                    'first_name' :first_name,
                    'last_name' :last_name,
                    'company' :company,
                    'phone' :phone,
                    'email' :email,
                    'subject' :subject,
                    'messege' :messege,
            }).then(function (data) {
                     $('#submit_question_client')[0].reset();
                     $('#submit_question_client').find('div').removeClass('error');
                     $("#contacts2_popup_popup_container").css('display', 'block');
                     $("#popup_bg").css('display', 'block');
            });
    });

//code for save data form cotact call page
     $("#save_contact_call_data").click(function(){
                var first_name = $("#text1").val();
                var file_name = $("#hidn_call_fld").val();
                var file_name_name = $("#hidn_file_name").val();
                var last_name = $("#text2").val();
                var company = $("#text3").val();
                var phone = $("#text4").val();
                var email= $("#email1").val();
                var subject = $("#select1").val();
                var messege = $("#textarea1").val();
                var filter = /^[0-9-+]+$/;
                if(first_name == '')
                {
                     $("#contact_question_first_name_id").addClass('error');
                     return false
                }
                if(first_name)
                {
                     $("#contact_question_first_name_id").removeClass('error');
                }
                if(company == '')
                {
                     $("#contact_question_company_id").addClass('error');
                     return false
                }
                if(company)
                {
                     $("#contact_question_company_id").removeClass('error');
                }
                if(phone)
                {
                    if (filter.test(phone)) {
                    $("#contact_question_phone").removeClass('error');
                    }
                    else {
                    $("#contact_question_phone").addClass('error');
                        return false;
                    }
                }
                if(email == '')
                {
                     $("#contact_question_email").addClass('error');
                     return false
                }
                if(email)
                {
                isValidEmailAddress(email)
                if (isValidEmailAddress(email) == true)
                {
                }
                if (isValidEmailAddress(email) == false)
                {
                     $("#contact_question_email").addClass('error');
                     return false
                }
                }

            openerp.jsonRpc("/page/contact_question/save_data", 'call', {
                    'first_name' :first_name,
                    'file_name' : file_name,
                    'file_name_name' : file_name_name,
                    'last_name' :last_name,
                    'company' :company,
                    'phone' :phone,
                    'email' :email,
                    'subject' :subject,
                    'messege' :messege,
            }).then(function (data) {
                     $('#submit_call_client')[0].reset();
                     $('#submit_call_client').find('div').removeClass('error');
                     $("#contacts2_popup_popup_container").css('display', 'block');
                     $(".file_name_box").css('display', 'none');
                     $(".uploaded_file_close").css('display', 'none');
                     $("#popup_bg").css('display', 'block');
            });

    });
    $('.popup-red-btn').on('click', function() {

        if($("#alert_chechbox").is(':checked')){
            $("#create-article-checkbox-1").attr("checked", "checked")
        }
		 $('.popup-container').fadeOut();
		 $('.popup-bg').fadeOut();
    });

 });
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
//    alert(pattern.test(emailAddress))
    return pattern.test(emailAddress);

};

function passwordStrengthCheck(password1, password2, passwordsInfo,passwordscheckInfo)
{
    //Must contain at least one upper case letter, one lower case letter and one digit.
    var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{8,}$/;

    $(password1).on('keyup', function(e) {
        if(VryStrongPass.test(password1.val()))
        {
            passwordsInfo.removeClass('error').addClass('no-error');
        }
        else
        {
            passwordsInfo.addClass('error');

        }
    });
    $(password2).on('keyup', function(e) {
        if(password1.val() !== password2.val())
        {
            passwordscheckInfo.addClass('error');
        }else{
            passwordscheckInfo.removeClass('error').addClass('no-error');
        }
    });
}
$(document).ready(function () {
	$('#registration-form').find('#password').keypress(function() {
		$(this).parent('div').removeClass('error');
	});
	$('#registration-form').find('#login').change(function() {
		if($(this).val()){
			openerp.jsonRpc("/page/registration/checkemail", 'call', {
				'email' :$(this).val(),
			}).then(function (data) {
				if(!data){
					$('#registration-form').find('#login').parent('div').addClass('error');
				}else{
					$('#registration-form').find('#login').parent('div').removeClass('error');
				}
			});
		}
	});
	$('#registration-form').submit(function(){
		var pwdField = $(this).find('#password');
		if($(pwdField).val() == ''){
			$(pwdField).parent('div').addClass('error');
			$(pwdField).parent('div').removeClass('pwd-error');
			return false;
		}else if(!checkStrongPassword($(pwdField).val())){
			$(pwdField).parent('div').addClass('pwd-error');
			return false;
		}else{
			$(pwdField).parent('div').removeClass('error');
			$(pwdField).parent('div').removeClass('pwd-error');
		}
		if($('#registration-form').find('#login').parent('div').hasClass('error')){
			return false;
		};
		return true;
	});
	$('#login-form').submit(function(){
		var pwdField = $(this).find('#re_password');
		var confrmPwdField = $(this).find('#confirm_password');
		if($(pwdField).length > 0){
			if($(pwdField).val() == ''){
				$(pwdField).parent('div').addClass('error');
				$(pwdField).parent('div').removeClass('pwd-error');
				return false;
			}else if(!checkStrongPassword($(pwdField).val())){
				$(pwdField).parent('div').addClass('pwd-error');
				return false;
			}else if($(pwdField).val() != $(confrmPwdField).val()){
				$(pwdField).parent('div').removeClass('error');
				$(pwdField).parent('div').removeClass('pwd-error');
				$(pwdField).parent('div').addClass('errorbox');
				$(confrmPwdField).parent('div').addClass('error');
				return false;
			}
		}
		return true;
	});
});
function checkStrongPassword(password){
	var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{8,}$/;
	return VryStrongPass.test(password)
}

function get_main_category(id_product,id_cat,cat_info,list_length,prodcut_info,info_product)
{
    var result2=[];
    var cat_for_products = info_product[id_product];
    for(var cat=0; cat < cat_for_products.length; cat++)
    {
        var public_category_id = cat_for_products[cat];
        public_categories = cat_info[public_category_id];
        if(public_categories['main_category'])
        {
            var temp=result2.push(public_categories['name']);
        }
    }
    return result2;
}

function get_image(prodcut_info,id_product,list_length)
{
   for(var j=0; j < list_length; j++)
    {
         if(prodcut_info[j].id==id_product)
         {
            return prodcut_info[j].image;
         }
    }

}

function get_category_info(id_product,id_cat,cat_info,list_length,prodcut_info,info_product)
{
    var result1=[];
    var cat_for_products = info_product[id_product];
    for(var cat=0; cat < cat_for_products.length; cat++)
    {
        var public_category_id = cat_for_products[cat];
        public_categories = cat_info[public_category_id];
        if(public_categories['width'] || public_categories['geometrie'])
        {
            var temp=result1.push(public_categories['name']);
        }
    }
    return result1;
}

function get_fullname(prodcut_info,id_product,list_length)
{
    for(var j=0; j < list_length; j++)
    {
        var p_info=prodcut_info[id_product];
        var dis_name=p_info['name'];
        var low_dis_name = dis_name.toLowerCase();
        var split_name=low_dis_name.split(" ");
        var first_half=split_name.join("-");
        var second_half='-'+p_info['id'];
        var full_name=first_half+second_half;
        return full_name;
    }
}

function get_website_published(prodcut_info,id_product,list_length)
{
    for(var j=0; j < list_length; j++)
    {
        var p_info=prodcut_info[id_product];
        return p_info['website_published'];
    }
}

function get_product_name(prodcut_info,id_product,list_length)
{
    for(var j=0; j < list_length; j++)
    {
         var p_info=prodcut_info[id_product];
         return ","+p_info['name'];
    }
}
function get_article_name(prodcut_info,id_product,list_length)
{
    for(var j=0; j < list_length; j++)
    {
         var p_info=prodcut_info[id_product];
         if(p_info['article'])
            {
                return p_info['article'];
            }
            else
            {
                return "";
            }
    }
}
function manageMessageCounts(data){
	if(data != 0){
		$("#unread_msg_header").html(data)
		var data = '+'+data
		$("#count_unread").addClass("count");
		$("#count_unread").html(data)
		$("#unread_msg_header").removeClass("hidden");
	}
	else{
		$("#count_unread").removeClass("count");
		$("#unread_msg_header").html("")
		$("#count_unread").html("")
		$("#unread_msg_header").addClass("hidden");
	}
}
function chooseFileContactcallpublic(){
    $("#contacts2_popup_popup_container_public").css('display', 'block');
    $("#popup_bg_public").css('display', 'block');
}

//To scroll up content at particular question on click of hide button
// $(".query_wrap .reply .hide-btn").click(function(){
//     $('html, body').animate({
//         scrollTop: $(this).parent().parent().find(".query").offset().top
//     });
//     $(this).parent().parent().find(".reply").slideToggle("slow", function(){
//         $("this").parent().removeClass("active");
//     });
//     return false;
// });
