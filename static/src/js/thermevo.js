$(document).ready(function () {
    $("#commercial_print_page").find('button').on('click', function () {
        //Print ele4 with custom options
        $("#commercial_print_page").print({

            //Use Global styles
            globalStyles: true,
            //Add link with attrbute media=print
            mediaPrint: true,
            //Custom stylesheet
            stylesheet: "https://localhost:8070/thermevo_website_management/static/src/css/thermevo_website.css",
            //Print in a hidden iframe
            iframe: false,
            //Don't print this
            noPrintSelector: ".avoid-this",
            //Add this at top
            //  prepend : "Hello World!!!<br/>",
            //Add this on bottom
            //  append : "<br/>Buh Bye!"
        });
    });

    var delivary_addr_form = $('#delivery_address_form');
    if ($(delivary_addr_form).length > 0) {
        country = $(delivary_addr_form).find("#country_id option:selected").text();
        checkCountryForVat(country);
        country_id = $(delivary_addr_form).find("#country_id option:selected").val();
    }
    $('#delivery_address_form').submit(function () {
        if ($('#vat_verify').val() == 'false') {
            $('#vat').val('');
        }
    });

    $(".select-wrap").find('#shipping_id').on('change', function () {
        //Print ele4 with custom options
        if ((document.getElementById('shipping_id').value) == 0) {
            document.getElementById("shipping_name").required = false;
            document.getElementById("shipping_street").required = false;
            document.getElementById("shipping_city").required = false;
            document.getElementById("shipping_country_id").required = false;
            document.getElementById("shipping_phone").required = false;
            document.getElementById("shipping_zip").required = false;
            document.getElementById("shipping_last_name").required = false;
        }
    });

    $(".select-wrap").find('#country_id').on('change', function () {
        country = $('#country_id option:selected').text()
        checkCountryForVat(country);
    });

    $('.oe_website_sale').each(function () {
        var oe_website_sale = this;
        $(oe_website_sale).on("change", ".oe_cart input.js_quantity", function (event) {
            var $input = $(this);
            var value = parseInt($input.val(), 10);
            var $dom = $(event.target).closest('tr');
            var default_price = parseFloat($dom.find('.text-danger > span.oe_currency_value').text());
            var $dom_optional = $dom.nextUntil(':not(.optional_product.info)');
            var line_id = parseInt($input.data('line-id'), 10);
            var product_id = parseInt($input.data('product-id'), 10);
            var product_ids = [product_id];
            $dom_optional.each(function () {
                product_ids.push($(this).find('span[data-oe-model="product.product"]').data('oe-id'));
            });
            if (isNaN(value)) value = 0;
            openerp.jsonRpc("/shop/get_unit_price", 'call', {
                'product_ids': product_ids,
                'add_qty': value,
                'line_id': line_id
            })
                .then(function (res) {
                    //basic case
                    $dom.find('span.oe_currency_value').last().text(res[product_id].toFixed(2));
                    $dom.find('.text-danger').toggle(res[product_id] < default_price && (default_price - res[product_id] > default_price / 100));
                    //optional case
                    $dom_optional.each(function () {
                        var id = $(this).find('span[data-oe-model="product.product"]').data('oe-id');
                        var price = parseFloat($(this).find(".text-danger > span.oe_currency_value").text());
                        $(this).find("span.oe_currency_value").last().text(res[id].toFixed(2));
                        $(this).find('.text-danger').toggle(res[id] < price && (price - res[id] > price / 100));
                    });
                    openerp.jsonRpc("/shop/cart/update_json", 'call', {
                        'line_id': line_id,
                        'product_id': parseInt($input.data('product-id'), 10),
                        'set_qty': value
                    })
                        .then(function (data) {
                            if (!data.quantity) {
                                location.reload();
                                return;
                            }
                            var $q = $(".my_cart_quantity");
                            $q.parent().parent().removeClass("hidden", !data.quantity);
                            $q.html(data.cart_quantity).hide().fadeIn(600);

                            $input.val(data.quantity);
                            $('.js_quantity[data-line-id=' + line_id + ']').val(data.quantity).html(data.quantity);
                            $("#cart_total").replaceWith(data['website_sale.total']);
                        });
                });
        });

    });
});

function checkCountryForVat(country) {
    euro_country_list = ["France", "Spain", "Sweden", "Sweden", "Germany", "Finland",
        "Poland", "United Kingdom", "Romania", "Kazakhstan", "Greece", "Bulgaria", "Hungary", "Portugal",
        "Serbia", "Iceland", "Ireland", "Austria", "Czech Republic", "Georgia", "Lithuania", "Latvia", "Croatia", "Bosnia-Herzegovina"
        , "Slovakia", "Estonia", "Denmark", "Netherlands", "Switzerland", "Moldavia", "Belgium", "Albania", "Macedonia, the former Yugoslav Republic of",
        "Turkey", "Slovenia", "Montenegro", "Cyprus", "Azerbaijan", "Luxembourg", "Andorra, Principality of", "Malta",
        "Liechtenstein", "San Marino", "Monaco"]

    non_euro_country_list = ["Afghanistan, Islamic State of", "Belarus", "India", "Kuwait", "Russian Federation", "South Sudan", "Ukraine", "United Arab Emirates"]

    if ($.inArray(country, euro_country_list) > -1) {
        $('#vat').prop('required', false);
        $("#vat").prop('disabled', false);
        $('#asterisk_vat').css('display', 'inline-block');
        $('#vat_verify').val('true');
    } else if ($.inArray(country, non_euro_country_list) > -1) {
        $('#vat').prop('required', false);
        $("#vat").prop('disabled', true);
        $('#asterisk_vat').css('display', 'none');
        $('#vat_verify').val('false');
        $("#vat").val('');
    } else {
        $('#vat').prop('required', false);
        $("#vat").prop('disabled', false);
        $('#asterisk_vat').css('display', 'none');
        $('#vat_verify').val('false');
    }
}