$(window).load(function () {
    // init Isotope
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            gutter: 10,
            columnWidth: '.grid-item'
        }
    });
    $('.portfolio_filter li').on('click', function () {
        var filterValue = $(this).attr('rel');
        $grid.isotope({filter: filterValue});
    });
});
$(document).ready(function () {

    // init Isotope
    /*var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            gutter: 10,
            columnWidth: '.grid-item'
        }
    });*/
    // filter items on button click
    /* $('.filter-button-group').on( 'click', 'button', function() {
         var filterValue = $(this).attr('data-filter');
         $grid.isotope({ filter: filterValue });
     });
     var i = 0;
     var filter = '';
     $('.portfolio_filter li').on('click', function(){
         $(this).toggleClass('active');
         filter = '';
         i = 0;
         if ($('.portfolio_filter li.active').length > 0) {
             $('.portfolio_items .item').addClass('hidden');
             $('.portfolio_filter li.active').each(function(){
                 $('.portfolio_items .item.' + $(this).attr('rel')).removeClass('hidden');
                 if (i == 0) {
                     filter += $(this).attr('rel');
                 } else {
                     filter += ',' +$(this).attr('rel');
                 }
                 i++;
             });
             $grid.isotope({ filter: filter });
         } else {
             $('.portfolio_items .item').removeClass('hidden');
             $grid.isotope({ filter: '*' });
         }
     });*/
});