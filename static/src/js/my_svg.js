var default_width = '';
var default_height = '';
var default_viewbox = '';
var groups = [];
var materials = [];
var current_group = 0;
var scrollpane ="";
var scrollpane_u ="";
var scrollpane_emission ="";
var polygonsArr = [];
var polygonsArrFill = [];
var polygonsemission = [];
var temperature = [];
var current_temperature = '';
var find_emission = '';
var replace_emission = '';
var red_temperature = '';
var blue_temperature = '';
var PanZoom = '';
var window_count = 0;
var list_material= 'none';
var list_material_array= [];
var all_materials = [];
var materials_emission = [];
var try_to_fill = 'false';
var remove_temperature = 'false';
var total_errors = 0;
var current_errors = 1;
var w_h_global = [];
var current_paint_material = '';
var current_paint_group = '';
var last_painted_group = '';
var last_painted_material = '';
var intervalID = '';


$.expr[':'].icontains = function(a, i, m) {
    return $(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

function rebuild_jscroll () {
    if ($(window).width() < 960) {
        $('.material_list,.material_list_u').height(108);
    } else {
        $('.material_list_subwrap').each(function(){
            if ($(this).find('.material_list_u').length > 0) {
                $('.material_list_u').height($('.svg_wrap #svg').height()-$('.buttons_top').outerHeight()-$(this).find('.material_list_top').outerHeight()-$(this).find('.open_close').outerHeight()-38-44);
            }
            if ($(this).find('.material_list').length > 0) {
                $('.material_list').height($('.svg_wrap #svg').height()-$('.buttons_top').outerHeight()-$(this).find('.material_list_top').outerHeight()-$(this).find('.open_close').outerHeight()-38-44);
            }
        });
        //$('.material_list,.material_list_u').height($('.svg_wrap #svg').height()-$('.buttons_top').outerHeight()-$('.material_list_top').outerHeight()-38);
    }
}

function rebuild_temperature(svg_opt,svg_data) {
    $('#show_material').append('<div class="temperature_button"><input name="temp_button" type="radio" value="cel" id="cel"/><label for="cel">&deg;C</label><input name="temp_button" type="radio" value="kel" id="kel"/><label for="kel">&deg;K</label></div>');
    $('#show_material').append('<div class="temperature_desc"></div>');
    $('#show_material').append('<div class="temperature_select"><input name="temp_button_select" type="radio" value="all_temp" id="all_temp"/><label for="all_temp">Show all isoterms</label><input name="temp_button_select" type="radio" value="selected_temp" id="selected_temp"/><label for="selected_temp">Show selected</label></div>');
    var colorss = [];
    var color = '';
    var keys = [];
    var i = 0;
    var current = 0;
    var br = 05;
    var bg = 33;
    var bb = 80;
    var rr = 75;
    var rg = 5;
    var rb = 33;

    $(svg_opt).find('isotherm').each(function(){
        if ($(this).attr('temp') == 283.15) {
            colorss[$(this).attr('temp')] = '#000000';
        } else if ($(this).attr('temp') < 283.15) {
            current = Math.abs(($(this).attr('temp') - 283.15))*15;
            colorss[$(this).attr('temp')] = 'rgb(' + br + ',' + bg + ',' + (bb + current) + ')';
        } else if ($(this).attr('temp') > 283.15) {
            current = Math.abs(($(this).attr('temp') - 283.15))*15;
            colorss[$(this).attr('temp')] = 'rgb(' + (rr + current) + ',' + rg + ',' + rb + ')';
        }
    });

    keys = Object.keys(colorss);

    keys.forEach(function(key){
        color = colorss[key];
        $(svg_opt).find('.temperature_desc').append('<div class="temp_item" rel=' + key + '><input type="checkbox" value="0" name="' + key + '"/><div class="temp_color" style="background:' + color + '"></div><div class="temp_title">   ' + key + '</div></div>');
    });

    $(svg_opt).find('isotherm').each(function(){
        color = colorss[$(this).attr('temp')];
        $(this).closest('polyline').css('stroke',color);
    });

    $(svg_opt).find('#cel').trigger('click');
    $(svg_opt).find('#all_temp').trigger('click');

    $(svg_opt).find('.temperature_desc .temp_item .temp_title').each(function(){
        $(this).text(parseFloat($(this).text()) - 273.15);
    });

    $.each(svg_data.isoterm, function( index, value ) {
        $(svg_opt).find('.temperature_desc .temp_item').each(function(){
            if ($(this).attr('rel') == index && value == true) {
                $(this).addClass('selected');
            }
        });
    });

    $('.temperature_button input[type=radio]').change(function(){

        if (!$(this).hasClass('active')) {
            $('.temperature_button input[type=radio]').removeClass('active');
            $(this).addClass('active');
            if ($(this).attr('id') == 'cel') {
                $('.temperature_desc .temp_item .temp_title').each(function(){
                    $(this).text(parseFloat($(this).text()) - 273.15);
                });
            } else {
                $('.temperature_desc .temp_item .temp_title').each(function(){
                    $(this).text(parseFloat($(this).text()) + 273.15);
                });
            }
        }
    });

    $('.temperature_select input[type=radio]').change(function(){
        if (!$(this).hasClass('active')) {
            $('.temperature_select input[type=radio]').removeClass('active');
            $(this).addClass('active');
            if ($(this).attr('id') == 'all_temp') {
                $('.temperature_desc .temp_item').removeClass('unchecked');
                $('.temperature_desc .temp_item').removeClass('iso_click');
                $(svg_opt).find('isotherm').each(function(){
                   $(this).closest('polyline').css('display','block');
                });
            } else {
                $('.temperature_desc .temp_item').each(function(){
                    $(this).addClass('iso_click');
                    if (!$(this).hasClass('selected')) {
                        $(this).addClass('unchecked');

                            var iso_temp = $(this).attr('rel');

                            $(svg_opt).find('isotherm').each(function(){
                                if ($(this).attr('temp') == iso_temp) {
                                    $(this).closest('polyline').css('display','none');
                                }
                            });
                    }
                });
            }
        }
    });
    $('body').on('click','.svg_wrap_result .temp_item.iso_click .temp_color',function(){
        var iso_temp = $(this).closest('.temp_item').attr('rel');
        if ($(this).closest('.temp_item').hasClass('unchecked')) {
            $(this).closest('.temp_item').removeClass('unchecked');
            $(this).closest('.temp_item').addClass('selected');
            $(svg_opt).find('isotherm').each(function(){
                if ($(this).attr('temp') == iso_temp) {
                    $(this).closest('polyline').css('display','block');
                }
            });
        } else {
            $(this).closest('.temp_item').addClass('unchecked');
            $(this).closest('.temp_item').removeClass('selected');
            $(svg_opt).find('isotherm').each(function(){
                if ($(this).attr('temp') == iso_temp) {
                    $(this).closest('polyline').css('display','none');
                }
            });
        }
        var iso_temp = $(this).closest('.svg_wrap_result');
        var iso_array = [];
        $(this).closest('.svg_wrap_result').find('.temp_item').each(function(){
            if ($(this).hasClass('selected')) {
                iso_array.push($(this).attr('rel'));
            }
        });
        openerp.jsonRpc('/flixo/post_isoterm', 'call', {
            isoterm:iso_array,
            token:$.trim($(iso_temp).find('.token').text()),
        }).then(function(data){});
    });

}

function array_intersect (arr1) {

    var retArr = {},
        argl = arguments.length,
        arglm1 = argl - 1,
        k1 = '',
        arr = {},
        i = 0,
        k = ''

    arr1keys: for (k1 in arr1) {
        arrs: for (i = 1; i < argl; i++) {
            arr = arguments[i]
            for (k in arr) {
                if (arr[k] === arr1[k1]) {
                    if (i === arglm1) {
                        retArr[k1] = arr1[k1]
                    }
                    // If the innermost loop always leads at least once to an equal value, continue the loop until done
                    continue arrs
                }
            }
            // If it reaches here, it wasn't found in at least one array, so try next value
            continue arr1keys
        }
    }

    return retArr
}

function initPanZoom() {
    var eventsHandler;

            eventsHandler = {
              haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel']
            , init: function(options) {
                var instance = options.instance
                  , initialScale = 1
                  , pannedX = 0
                  , pannedY = 0

                // Init Hammer
                // Listen only for pointer and touch events
                this.hammer = Hammer(options.svgElement, {
                  inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
                })

                // Enable pinch
                this.hammer.get('pinch').set({enable: true})

                // Handle double tap
                this.hammer.on('doubletap', function(ev){
                  instance.zoomIn()
                })

                // Handle pan
                this.hammer.on('panstart panmove', function(ev){
                  // On pan start reset panned variables
                  if (ev.type === 'panstart') {
                    pannedX = 0
                    pannedY = 0
                  }

                  // Pan only the difference
                  instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY})
                  pannedX = ev.deltaX
                  pannedY = ev.deltaY
                })

                // Handle pinch
                this.hammer.on('pinchstart pinchmove', function(ev){
                  // On pinch start remember initial zoom
                  if (ev.type === 'pinchstart') {
                    initialScale = instance.getZoom()
                    instance.zoom(initialScale * ev.scale)
                  }

                  instance.zoom(initialScale * ev.scale)

                })

                // Prevent moving the page on some devices when panning over SVG
                options.svgElement.addEventListener('touchmove', function(e){ e.preventDefault(); });
              }

            , destroy: function(){
                this.hammer.destroy()
              }
            }
            ///////////////////////////////////////////////////////
            var beforePan

            beforePan = function(oldPan, newPan){
              var stopHorizontal = false
                , stopVertical = false
                , sizes = this.getSizes()
                , zoom = this.getZoom()
                , gutterWidth = sizes.viewBox.width * 0.2 * sizes.realZoom / zoom
                , gutterHeight = sizes.viewBox.height * 0.2 * sizes.realZoom / zoom
                  // Computed variables
                , leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth
                , rightLimit = sizes.width - gutterWidth - (sizes.viewBox.x * sizes.realZoom)
                , topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight
                , bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom)

              customPan = {}
              customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x))
              customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y))
              return customPan
            }

            // Expose to window namespase for testing purposes
            PanZoom = svgPanZoom('#SvgjsSvg1000', {
              zoomEnabled: true,
              controlIconsEnabled: true,
              fit: true,
              center: true,
              customEventsHandler: eventsHandler,
              beforePan: beforePan,
              controlIconsEnabled: false,
              maxZoom: 40,
              dblClickZoomEnabled: false
              // viewportSelector: document.getElementById('demo-tiger').querySelector('#g4') // this option will make library to misbehave. Viewport should have no transform attribute
            });
            /*if ($(event.target).hasClass("fa-arrow-up"))
                    example.panUp()
                if ($(event.target).hasClass("fa-arrow-down"))
                    example.panDown()
                if ($(event.target).hasClass("fa-arrow-left"))
                    example.panLeft()
                if ($(event.target).hasClass("fa-arrow-right"))
                    example.panRight()
                if ($(event.target).hasClass("fa-plus")) {
                    example.zoomIn();
                }
                if ($(event.target).hasClass("fa-minus")) {
                    example.zoomOut();
                }
                if ($(event.target).hasClass("fa-refresh")) {
                    example.reset();
                }*/
            $('#svg_controls .fa-arrow-left,.svg_controls .fa-arrow-left').click(function(){
                var sizes = PanZoom.getSizes()
                x = sizes.viewBox.x - (sizes.viewBox.width/10);
                PanZoom.panBy({'x': (sizes.viewBox.width/10)*sizes.realZoom, 'y': 0});
                return false;
            });
            $('#svg_controls .fa-arrow-right,.svg_controls .fa-arrow-right').click(function(){
                var sizes = PanZoom.getSizes()
                x = sizes.viewBox.x - (sizes.viewBox.width/10);
                PanZoom.panBy({'x': -(sizes.viewBox.width/10)*sizes.realZoom, 'y': 0});
                return false;
            });
            $('#svg_controls .fa-arrow-down,.svg_controls .fa-arrow-down').click(function(){
                var sizes = PanZoom.getSizes()
                x = sizes.viewBox.x - (sizes.viewBox.width/10);
                PanZoom.panBy({'x': 0, 'y': -(sizes.viewBox.height/10)*sizes.realZoom});
                return false;
            });
            $('#svg_controls .fa-arrow-up,.svg_controls .fa-arrow-up').click(function(){
                var sizes = PanZoom.getSizes()
                x = sizes.viewBox.x - (sizes.viewBox.width/10);
                PanZoom.panBy({'x': 0, 'y': (sizes.viewBox.height/10)*sizes.realZoom});
                return false;
            });
            $('#svg_controls .fa-plus,.svg_controls .fa-plus').click(function(){
                PanZoom.zoomIn();
                return false;
            });
            $('#svg_controls .fa-minus,.svg_controls .fa-minus').click(function(){
                PanZoom.zoomOut();
                return false;
            });
            $('#svg_controls .fa-refresh,.svg_controls .fa-refresh').click(function(){
                PanZoom.resetZoom();
                PanZoom.center();
                return false;
            });
}

function append_created(name_user,color,lambda,eps,dencity,material_id) {

    var name = '';
    var output = '';

    if (lambda !== '') {
        name += 'λ = ' + lambda;
    }

    if (eps !== '') {
        if (name !== '') {
            name += ', ';
        }
        name  += 'ε = ' + eps;
    }
    if (dencity !== '') {
        if (name !== '') {
            name += ', ';
        }
        name  += 'd = ' + dencity;
    }
    name += ' <br> ' + name_user;

    output += '<div class="material_item material' + material_id + '" material="' + material_id +'">';
    output += '<div class="color_name"  onclick="fill_path_by_id(\'' + material_id +'\');">';
    output += '<span class="color_block" style="background:' + color +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
    output += '<span class="name">'+ name + '</span>';
    output += '</div>';
    output += '<span class="option_list"><span></span><span></span><span></span></span>';
    output += '<span class="option_button delete_option">Delete</span>';
    output += '</div>';

    materials[material_id]=[];
    materials[material_id][0]=material_id;
    materials[material_id][1]=name_user;
    materials[material_id][2]=dencity;
    materials[material_id][3]=lambda;
    materials[material_id][4]=eps;
    materials[material_id][5]=color;

    return output;
}

function emission_scrollpane(materials_emission) {
    var output = "";

    for (var index in materials_emission) {
        var name = "";
        tarr = materials[materials_emission[index]];

        if (tarr[3] !== '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] !== '') {
            if (name != '') {
                name += ', ';
            }
            name  += 'ε = ' + tarr[4];
        }
        if (tarr[2] !== '') {
            if (name != '') {
                name += ', ';
            }
            name  += 'd = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];

        output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] +'">';
        output += '<div class="color_name">';
        output += '<span class="color_block" style="background:' + tarr[5] +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
        output += '<span class="name">'+ name + '</span>';
        output += '</div>';
        output += '</div>';

    }

    var api_emission = scrollpane_emission.data('jsp');
    api_emission.getContentPane().html(output);
    api_emission.reinitialise();
}

function processData(allText) {
    var output = "";
    var tarr = allText['O'][0];
    $('.item_group_block_change .button_air').html('<div class="change_to_air" onclick="fill_path_by_id(\'' + tarr[0] +'\');">' + tarr[1] + '</div>').attr('material',tarr[0]);
    for (var i=0; i<allText['O'].length; i++) {
        var name = "";
        tarr = allText['O'][i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] !== '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] !== '') {
            if (name !== '') {
                name += ', ';
            }
            name  += 'ε = ' + tarr[4];
        }
        if (tarr[2] !== '') {
            if (name !== '') {
                name += ', ';
            }
            name  += 'd = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] +'" >';
            output += '<div class="color_name" onclick="fill_path_by_id(\'' + tarr[0] +'\');">';
            output += '<span class="color_block" style="background:' + tarr[5] +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">'+ name + '</span>';
            output += '</div>';
            output += '<span class="option_list"><span></span><span></span><span></span></span>';
            output += '<span class="option_button add_option">Add to my materials</span>';
            output += '</div>';
        }
    }


    $('.material_list').append(output);
    rebuild_jscroll();
    scrollpane = $('.material_list');
    scrollpane.jScrollPane();

    /*******************************User scrollpane*************************************/
    var output = "";
    var tarr = allText['U'][0];

    for (var i=0; i<allText['U'].length; i++) {
        var name = "";
        tarr = allText['U'][i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] !== '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] !== '') {
            if (name !== '') {
                name += ', ';
            }
            name  += 'ε = ' + tarr[4];
        }
        if (tarr[2] !== '') {
            if (name !== '') {
                name += ', ';
            }
            name  += 'd = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] +'">';
            output += '<div class="color_name" onclick="fill_path_by_id(\'' + tarr[0] +'\');">';
            output += '<span class="color_block" style="background:' + tarr[5] +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">'+ name + '</span>';
            output += '</div>';
            output += '<span class="option_list"><span></span><span></span><span></span></span>';
            output += '<span class="option_button delete_option">Delete</span>';
            output += '</div>';
        }
    }


    $('.material_list_u').append(output);
    rebuild_jscroll();
    scrollpane_u = $('.material_list_u');
    scrollpane_u.jScrollPane();

}

function processData_update(allText) {
    var output = "";
    var tarr = allText["O"][0];

    $('.item_group_block_change .button_air').html('<div class="change_to_air" onclick="fill_path_by_id(\'' + tarr[0] +'\');">' + tarr[1] + '</div>').attr('material',tarr[0]);
    for (var i=0; i<allText['O'].length; i++) {
        var name = "";
        tarr = allText['O'][i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] !== '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] !== '') {
            name  += ', ε = ' + tarr[4];
        }
        if (tarr[2] !== '') {
            name  += ', d = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] + '" >';
            output += '<div class="color_name" onclick="fill_path_by_id(\'' + tarr[0] +'\');">';
            output += '<span class="color_block" style="background:' + tarr[5] + '"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">' + name + '</span>';
            output += '</div>';
            output += '<span class="option_list"><span></span><span></span><span></span></span>';
            output += '<span class="option_button add_option">Add to my materials</span>';
            output += '</div>';
        }
    }
    var api = scrollpane.data('jsp');
    api.getContentPane().html(output);
    api.reinitialise();
    /*******************************User scrollpane*************************************/
    var output = "";
    var tarr = allText["U"][0];
    for (var i=0; i<allText['U'].length; i++) {
        var name = "";
        tarr = allText['U'][i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] !== '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] !== '') {
            name  += ', ε = ' + tarr[4];
        }
        if (tarr[2] !== '') {
            name  += ', d = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] + '" >';
            output += '<div class="color_name" onclick="fill_path_by_id(\'' + tarr[0] + '\');">';
            output += '<span class="color_block" style="background:' + tarr[5] + '"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">' + name + '</span>';
            output += '</div>';
            output += '<span class="option_list"><span></span><span></span><span></span></span>';
            output += '<span class="option_button delete_option">Delete</span>';
            output += '</div>';
        }
    }
    var api_u = scrollpane_u.data('jsp');
    api_u.getContentPane().html(output);
    api_u.reinitialise();
}

function show_notification(n,wrap) {
	n  = $(n);
	wrap  = $(wrap);

    n.css('top',(wrap.outerHeight()-n.outerHeight())/2);

    /*if (n.hasClass('notice_ready') && $(window).width() >= 960) {
        n.css('left',(wrap.outerWidth()-n.outerWidth()+$('.material_list_wrap').outerWidth())/2);
    } else {
        n.css('left',(wrap.outerWidth()-n.outerWidth())/2);
    }*/
    n.css('left',(wrap.outerWidth()-n.outerWidth())/2);
    if (n.hasClass('notice_change_error')) {
        n.css('top',0);
        n.css('left',0);
    }
	n.css('display','block');
	wrap.find('.svg_notice_bg').css('display','block');

    function close_popup_congratulation() {
        if ($('.notice_congratulation').css('display') != 'none') {
            $('.notice_congratulation .close_notice').trigger('click');
        }
    }
    function close_popup_add_material() {
        $('.notice_add_material .close_notice').trigger('click');
    }
    function close_popup_int(){
        if ($('.notice_slider').css('display') == 'none') {
            setTimeout(close_popup_congratulation, 2000);
            clearInterval(intervalID);
        }
    }
    if (n.hasClass('notice_congratulation')) {
        var intervalID = setInterval(close_popup_int, 1000) // использовать функцию
    }
    if (n.hasClass('notice_add_material')) {
        setTimeout(close_popup_add_material, 2000);
    }
}

var hexDigits = new Array
("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

//Function to convert hex format to a rgb color
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function close_notification(n,wrap) {
	$(n).css('display','none');
	$(wrap).find('.svg_notice_bg').css('display','none');
}

function get_x(y,x1,y1,x2,y2) {
    if ( x1 == x2 ) {
        return x1;
    }
    var a = (y2-y1)/(x2-x1);
    var b = (x2*y1-x1*y2)/(x2-x1);
    var x = (y-b)/a;

    return x;
}

var svg_html = '';

function svg_success() {
    $.get($('.svg_data .url').text(), function (data) {
        var error = 0;
        $('.svg_wrap').append('<div id="svg_temp"></div>');
        $('#svg_temp').append(new XMLSerializer().serializeToString(data.documentElement));
        default_width = $('#svg_temp svg').attr('width');
        default_height = $('#svg_temp svg').attr('height');
        svg_html=$('#svg_temp svg').attr('id','temp_svg');
        var svg = document.getElementById('temp_svg');
        var box = svg.getAttribute('viewBox');
        default_viewbox = box;
        box = box.split(' ');

        var x = box[0];
        var y = box[1];
        var width = box[2];
        var height = box[3];

        draw.viewbox(x, y, width, height);

        svg_html = svg_html[0].childNodes;

        $(svg_html).each(function(){
            $('#svg_temp').html(this);
            draw.svg($('#svg_temp').html());
        });

        /***********************************************************************************/

        var convert_polygon = [];

        var i = 0;
        $('.svg_wrap #svg path').each(function () {
         $(this).attr('id', 'path' + i);
            i++;
        });

        $('.svg_wrap #svg path').each(function () {
                 var path = SVG.get($(this).attr('id'));
                 var i = 0;
                 var d='';
                 $.each(path.array().value, function(index, value) {
                     if (i == 0) {
                     	d += 'M'+(parseFloat(value[1]) * 100).toFixed(6)+','+(parseFloat(value[2]) * 100).toFixed(6);
                     } else {
                     	d += ' '+value[0]+(parseFloat(value[1]) * 100).toFixed(6)+','+(parseFloat(value[2]) * 100).toFixed(6);
                     }
                     i = i + 1;
                 });
                 $(this).append('<realpath d="' + $(this).attr('d') + '"></realpath>');
                 path.plot(d);
                 $(this).css('stroke-width',parseFloat($(this).css('stroke-width'))*100);
         });
        var box = svg.getAttribute('viewBox');
        default_viewbox = box;
        box = box.split(' ');

        var x = box[0];
        var y = box[1];
        var width = box[2];
        var height = box[3];
        draw.viewbox(x*100, y*100, width*100, height*100);

        i=0;

        initPanZoom();

        $('.svg_wrap #svg desc').each(function () {

            var text = $(this).text();

            if (text == 'Lonely Vertices' || text == 'Shady Edges' || text == 'Unused Edges') {
                error = error + 1;
            }
        });

            $('.svg_wrap #svg path').css('fill','none');
            //TODO change ajax
            openerp.jsonRpc('/flixo/get_material_json', 'call', {token:$('.svg_wrap .svg_data .token').text()}).then(function(data) {
                processData(data);
            });
            /*$.ajax({
                type: "GET",
                url: $('.svg_data .csv').text(),
                dataType: "json",
                success: function(data) {
                    processData(data);
                }
            });*/

            //TODO change ajax
            openerp.jsonRpc('/flixo/get_product_information_json', 'call', { token:$('.svg_wrap .svg_data .token').text()}).then(function(data) {

            /*$.ajax({
                type: "GET",
                url: $('.svg_data .material_list_group').text(),
                dataType: "json",
                success: function(data) {*/
                    var i = 0;
                    $('.svg_wrap #svg path').each(function () {
                     $(this).attr('id', 'path' + i);
                        i++;
                    });
                    $.each( data, function( key, value ) {
                        if (value['material_id'] != '') {
                            $('.svg_wrap #svg #path' + value['id']).find('matprop').attr('material',value['material_id']);
                            $('.svg_wrap #svg #path' + value['id']).attr('rel',value['id']);
                        }
                    });
                     i = 0;
                     var el_arr = [];
                     var el_arr_mat = [];
                     $('.svg_wrap #svg path').each(function () {
                         $(this).attr('rel', i);
                         //if ($(this).find('area').attr('value') > 0.000001) {
                         if ($(this).find('matprop').attr('id') != '{00000000-0000-0000-0000-000000000000}') {
                             $(this).attr('id', 'path' + i).css('fill','#B0E0E6');
                             var path = SVG.get('path' + i);
                             var tempPoligon = [];
                             var tempPoligon2 = [];

                             $.each(path.array().value, function(index, value) {

                                 if (value[0] == "M") {
                                     if (tempPoligon.length > 0) {
                                         polygonsArr.push(tempPoligon);
                                         tempPoligon.push({'path': 'path' + i});
                                         polygonsArrFill.push(tempPoligon);
                                         tempPoligon = [];
                                     }
                                 }
                                 tempPoligon.push({'x': value[1], 'y': value[2]});
                                 tempPoligon2.push(value[1] + ',' + value[2]);
                             });
                             if (tempPoligon.length > 0) {
                                polygonsArr.push(tempPoligon);
                                 tempPoligon.push({'path': 'path' + i});
                                 polygonsArrFill.push(tempPoligon);
                             }
                             polygonsemission[i]=tempPoligon2;



                             //if ($(this).find('matprop').attr('material') === undefined || $(this).find('matprop').attr('material') == '{00000000-0000-0000-0000-000000000000}' ) {
                             if ($(this).find('matprop').attr('material') != 'O-1053' && $(this).find('matprop').attr('material') != 'O-1036' && $(this).find('matprop').attr('material') != 'O-2000' && $(this).find('matprop').attr('material') != 'O-2054' ) {
                                el_arr_mat.push('#'+$(this).attr('id'));
                             } else {
                                 var material = $(this).find('matprop').attr('material');

                                 if (el_arr[material] === undefined) {
                                     el_arr[material] = [];
                                 }

                                 el_arr[material]['material'] = material;
                                 if (el_arr[material]['ids'] === undefined) {
                                    el_arr[material]['ids'] = [];
                                 }
                                 el_arr[material]['ids'].push('#'+$(this).attr('id'));
                             }
                             if (path.width() > 19 || path.height() > 19) {
                                window_count = window_count + 1;
                             }
                         } else {
                             var path = SVG.get('path' + i);
                             var tempPoligon = [];
                             $.each(path.array().value, function(index, value) {
                                 if (value[0] == "M") {
                                     if (tempPoligon.length > 0) {
                                         polygonsArr.push(tempPoligon);
                                         tempPoligon.push({'path': 'path' + i});
                                         polygonsArrFill.push(tempPoligon);
                                         tempPoligon = [];
                                     }
                                 }
                                 tempPoligon.push({'x': value[1], 'y': value[2]});
                             });
                             if (tempPoligon.length > 0) {
                                 polygonsArr.push(tempPoligon);
                                 tempPoligon.push({'path': 'path' + i});
                                 polygonsArrFill.push(tempPoligon);
                             }
                         }
                         i = i + 1;
                     });

                     $('.svg_wrap #svg g.svg-pan-zoom_viewport > g').attr('id','group_d');

                     $('.svg_wrap #svg g.svg-pan-zoom_viewport > g').append('<g id="temperature"></g>');
                     //var group_d = SVG.get('group_d').addTo(draw);
                     var el_arr_main = [];
                     $.each( el_arr_mat, function( key, value ) {
                         var temp_arr = [];
                         temp_arr['material'] = 'none';
                         temp_arr['ids'] = [];
                         temp_arr['ids'].push(value);
                         el_arr_main.push(temp_arr);
                     });

                    for (var key in el_arr) {
                        el_arr[key]['filled'] = 'FALSE';
                        groups.push(el_arr[key]);
                    }
                    for (var key in el_arr_main) {
                        el_arr_main[key]['filled'] = 'FALSE';
                        groups.push(el_arr_main[key]);
                    }
                     temp_arr = undefined;
                     el_arr = undefined;
                     el_arr_mat = undefined;

                    if (error > 0) {
                        total_errors=1;
                        $('.svg_wrap .upload_preload_data').css('display','none');
                        show_notification('.notice_error', '.svg_wrap');
                        $('.svg_wrap #svg circle,.svg_wrap #svg line').each(function () {
                            $(this).attr('id', 'error' + total_errors);
                            total_errors++;
                        });
                        //TODO
                        openerp.jsonRpc('/flixo/figure_error', 'call', {
                         token:$('.svg_wrap .svg_data .token').text()
                         }).then(function(data) {});

                        $('.svg_wrap #svg g circle').each(function(){
                            $(this).attr('cx',$(this).attr('cx')*100);
                            $(this).attr('cy',$(this).attr('cy')*100);
                        });
                        $('.svg_wrap #svg g line').each(function(){
                            $(this).attr('x1',$(this).attr('x1')*100);
                            $(this).attr('y1',$(this).attr('y1')*100);
                            $(this).attr('x2',$(this).attr('x2')*100);
                            $(this).attr('y2',$(this).attr('y2')*100);
                        });

                        $('.svg_notice.notice_error .close_notice').click(function () {
                            close_notification('.notice_error', '.svg_wrap');
                            show_notification('.notice_change_error', '.svg_wrap');
                            return false;
                        });
                        $('.svg_wrap #svg g').each(function(){
                            var stroke_width = parseFloat($(this).css('stroke-width'));
                            if (stroke_width < 1) {
                                //stroke_width = stroke_width*3*100;
                                stroke_width = stroke_width*100;
                                $(this).css('stroke-width',stroke_width);
                            }
                        });
                        svg_center_error();
                        $('.svg_wrap #svg circle').each(function(){
                            var radius = $(this).attr('r');
                            radius = radius*100;
                            $(this).attr('r',radius);
                        });
                        $('.svg_notice.notice_change_error .skip_notice').on( "click",function () {
                            $('.svg_wrap #svg svg line,.svg_wrap #svg svg circle').css('opacity',"1");
                            $('.svg_wrap #svg svg path').css('opacity',"1");

                            var stroke_width = parseFloat($('#error'+current_errors).css('stroke-width'));

                            stroke_width = stroke_width/3;
                            $('#error'+current_errors).css('stroke-width',stroke_width);
                            current_errors++;
                            if (total_errors == current_errors) {
                                PanZoom.reset();
                                close_notification('.notice_change_error', '.svg_wrap');
                                show_notification('.notice_congratulation', '.svg_wrap');
                            } else {
                                svg_center_error();
                            }
                            return false;
                        });
                        $('.svg_notice.notice_change_error .close_notice').on( "click",function () {
                            $('.svg_wrap #svg svg line,.svg_wrap #svg svg circle').css('opacity',"1");
                            $('.svg_wrap #svg svg path').css('opacity',"1");

                            var stroke_width = parseFloat($('#error'+current_errors).css('stroke-width'));

                            stroke_width = stroke_width/3;
                            $('#error'+current_errors).css('stroke-width',stroke_width);
                            $('#error'+current_errors).remove();
                            current_errors++;
                            if (total_errors == current_errors) {
                                PanZoom.reset();
                                close_notification('.notice_change_error', '.svg_wrap');
                                show_notification('.notice_congratulation', '.svg_wrap');
                            } else {
                                svg_center_error();
                            }
                            return false;
                        });


                    }

                    $('.svg_notice.notice_congratulation .close_notice').on( "click",function () {
                        close_notification('.svg_notice', '.svg_wrap');
                        if ($(this).closest('.svg_notice').hasClass('notice_congratulation')) {
                            $('.svg_wrap .item_group_block').css('display','block');
                            next_fill();
                        }
                        return false;
                    });
                    $('.button_yes, .button_next').click(function(){
                        next_element();
                    });
                    $('.button_prev').click(function(){
                        prev_element();
                    });
                    $('.button_no').click(function(){
                        $('.item_group_block').css('display', 'none');
                        $('.item_group_block_change').css('display', 'block');
                        rebuild_jscroll();
                        var api = scrollpane.data('jsp');
                        api.reinitialise();
                        var api_u = scrollpane_u.data('jsp');
                        api_u.reinitialise();
                        svg_center();
                    });
                    $('.button_menu').click(function(){
                        $('.item_group_block').css('display', 'block');
                        $('.item_group_block_change').css('display', 'none');
                        svg_center();
                        stop_painting();
                    });

                    $('.material_list_top input').on('keyup',function(){
                        var el = $(this).closest('.material_list_subwrap');
                        if($(this).val().length >= 3) {
                            el.find('.material_item').css('display','block');
                            el.find('.material_item .name').not(".material_item .name:icontains('" + $(this).val() +"')").closest('.material_item').css('display','none');
                            var api = scrollpane.data('jsp');
                            api.reinitialise();
                            var api_u = scrollpane_u.data('jsp');
                            api_u.reinitialise();
                        } else {
                            el.find('.material_item').css('display','block');
                            var api = scrollpane.data('jsp');
                            api.reinitialise();
                            var api_u = scrollpane_u.data('jsp');
                            api_u.reinitialise();
                        }
                    });
                    $('.svg_wrap .button_ungroup').click(function(){
                        if ($('.paint_button').hasClass('active')) {
                            if (current_paint_group !== '') {
                                ungroup_groups_paint(current_paint_group);
                            }
                        } else {
                            ungroup_groups();
                        }
                    });
                    $('.svg_wrap .button_ready').click(function(){
                        $('.item_group_block_change').css('display', 'none');
                        $('.item_group_block').css('display', 'none');
                        show_notification('.notice_ready', '.svg_wrap');
                        stop_painting();
                        PanZoom.reset();
                        ready_buttons(true);
                        list_material_array = [];

                        var jsonArg2 = new Object();
                        $('.svg_wrap #svg svg path').each(function(){
                            var jsonArg2 = new Object();
                            jsonArg2.id = jQuery(this).attr('rel');
                            jsonArg2.material = jQuery(this).find('matprop').attr('material');
                            list_material_array.push(jsonArg2);
                            var path = '';
                            path = SVG.get('path' + jQuery(this).attr('rel'))
                            w_h_global.push({'w': path.width().toFixed(6), 'h':path.height().toFixed(6)});
                        });
                    });
                    $('.svg_wrap .button_create').click(function(){
                        $('.item_group_block_change').css('display', 'none');
                        $('.create_material').css('display', 'block');
                        var newColor = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
                        $(".create_material .color").css('background',newColor);
                    });
                    $('.svg_wrap .close_create').click(function(){
                        $('.item_group_block_change').css('display', 'block');
                        $('.create_material').css('display', 'none');
                    });
                    $('.svg_wrap .create_ready').click(function(){
                        var error = false;
                        $('.svg_wrap .create_material .inputs input,.svg_wrap .create_material .inputs textarea').removeClass('error');

                        if ($('.svg_wrap  .create_material .lambda').val() == '') {
                            error = true;
                            $('.svg_wrap  .create_material .lambda').addClass('error');
                        }

                        if ($('.svg_wrap  .create_material .name').val() == '') {
                            error = true;
                            $('.svg_wrap  .create_material .name').addClass('error');
                        }

                        if (error == true) {
                            return false;
                        }
                        //TODO change ajax

                        openerp.jsonRpc('/flixo/create_material_json', 'call', {
                         name:$('.svg_wrap .create_material .name').val(),
                         color:rgb2hex($('.svg_wrap .create_material .color').css('background-color')),
                         lambda:$('.svg_wrap .create_material .lambda').val(),
                         eps:$('.svg_wrap .create_material .eps').val(),
                         dencity:$('.svg_wrap .create_material .dencity').val(),
                         }).then(function(data){

                        /*$.post("/ajax/create_material.php",
                            {
                                name:$('.svg_wrap .create_material .name').html(),
                                color:rgb2hex($('.svg_wrap .create_material .color').css('background-color')),
                                lambda:$('.svg_wrap .create_material .lambda').val(),
                                eps:$('.svg_wrap .create_material .eps').val(),
                                density:$('.svg_wrap .create_material .dencity').val(),
                                insulation_strips:$('.svg_wrap .create_material #insulation_strips').prop('checked'),
                            },
                            function(data){*/
                                var new_material = data;
                                var output=append_created($('.svg_wrap .create_material .name').val(),$('.svg_wrap .create_material .color').css('background-color'),$('.svg_wrap .create_material .lambda').val(),$('.svg_wrap .create_material .eps').val(),$('.svg_wrap .create_material .dencity').val(),new_material);
                                var api_u = scrollpane_u.data('jsp');
                                api_u.getContentPane().prepend(output);
                                fill_path_by_id(new_material);
                                $('.item_group_block_change').css('display', 'block');
                                $('.create_material').css('display', 'none');
                                rebuild_jscroll();
                                api_u.reinitialise();
                                api_u.scrollTo(0, parseInt($(".material"+new_material).offset().top-$(".material_list").offset().top));
                            },"json");
                    });
                    $('.svg_wrap .temperature_ready').click(function(){
                        show_notification('.notice_temperature', '.svg_wrap');
                    });
                    $('.svg_wrap .button_cheak').click(function(){
                        close_notification('.notice_temperature', '.svg_wrap');
                    });
                    $('.svg_wrap .chose_temperature .button_interior,#cabinet-container.flixo.svg .flixo_step2').click(function(){
                        if ($(this).hasClass('flixo_step2') && $(this).hasClass('active')) {
                            return false;
                        }
                        if ($('.svg_wrap radsurface').length > 0) {
                            show_notification('.notice_set_emmition_remove', '.svg_wrap');
                        } else {
                            $('.svg_wrap .chose_temperature').css('display', 'none');
                            $('.item_group_block').css('display', 'block');
                            $('.flixo_step2').removeClass('selectd_prev').addClass('active');
                            $('.flixo_step3').removeClass('active');
                            $('#chose_temperature_point, #svg').attr('class', '');
                            $('.svg_wrap svg image').css('display', 'none');
                            //current_group = groups.length - 1 ;
                            var material_id = groups[current_group]['material'];
                            fill_path(current_group, material_id);
                            ready_buttons(false);
                            svg_center();
                            svg_center();
                        }
                        return false;
                    });
                    $('.svg_wrap .notice_ready .ready_no').click(function(){
                        $('.item_group_block').css('display','block');
                        close_notification('.notice_ready', '.svg_wrap');
                        svg_center();
                        return false;
                    });
                    $('.svg_wrap .notice_error_flixo .close_notice').click(function(){
                        close_notification('.notice_error_flixo', '.svg_wrap');
                        $('.svg_wrap .chose_temperature').css('display','block');
                        return false;
                    });

                    $('.svg_wrap .notice_ready .ready_button').click(function(){
                        close_notification('.notice_ready', '.svg_wrap');
                        show_notification('.notice_set_emmition', '.svg_wrap');
                        $('.item_group_block').css('display', 'none');
                        $('.item_group_block_change').css('display', 'none');
                        $('.svg_wrap #svg svg path').css('fill-opacity',"1");
                        PanZoom.resetZoom();
                        PanZoom.center();
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition .ready_no').click(function(){
                        close_notification('.notice_set_emmition', '.svg_wrap');
                        $('.svg_wrap .chose_temperature').css('display','block');
                        $('#chose_temperature_point span').each(function(){
                            if (!$(this).hasClass('hidden')) {
                                $('#chose_temperature_point').addClass('moved').addClass($(this).attr('class'));
                                $(this).addClass('active');
                                current_temperature = $(this).attr('rel');
                                return false;
                            }
                        });
                        $('.flixo_step2').addClass('selectd_prev').removeClass('active');
                        $('.flixo_step3').addClass('active');
                        $('.svg_wrap svg image').css('display','block');
                        $('.svg_wrap #svg svg path').css('fill-opacity',"1");
                        PanZoom.resetZoom();
                        PanZoom.center();
                        return false;
                    });
                    $('.svg_wrap .button_emission_ready').click(function(){
                        show_notification('.notice_set_emmition_ready', '.svg_wrap');
                        PanZoom.reset();
                        $('.svg_wrap .emission_block').css('display','none');
                        find_emission = '';
                        replace_emission = '';
                        $('.material_list_emmision_mainwrap').css('display','none');
                        $('.old_surface').remove();
                        $('.temp_surface').removeClass('temp_surface');

                        $('svg path').each(function(){
                            var element = SVG.get($(this).attr('id'));
                            if(element.hasClass('temp_path')) {
                                element.removeClass('temp_path');
                            }
                        });
                        $('.button_emission_reset_last').removeClass('active');
                        $('.button_emission').removeClass('emission');
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition_ready .ready_no').click(function(){
                        close_notification('.notice_set_emmition_ready', '.svg_wrap');
                        $('.svg_wrap .emission_block').css('display','block');
                        return false;
                    });
                    $('.svg_wrap .emission_block .button_material').click(function(){
                        if ($('.svg_wrap radsurface').length > 0) {
                            show_notification('.notice_set_emmition_remove', '.svg_wrap');
                        } else {
                            $('.svg_wrap radsurface').remove();
                            find_emission = '';
                            replace_emission = '';
                            $('svg path').each(function(){
                                var element = SVG.get($(this).attr('id'));
                                if(element.hasClass('custom_path')) {
                                    element.remove();
                                }
                            });
                            $('.button_emission').removeClass('emission');
                            $('.button_emission_reset_last').removeClass('active');
                            $('.emission_block').css('display','none');
                            $('.item_group_block').css('display','block');
                            var material_id = groups[current_group]['material'];
                            fill_path(current_group, material_id);
                            ready_buttons(false);
                            svg_center();
                        }
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition_remove .ready_no').click(function(){
                        close_notification('.notice_set_emmition_remove', '.svg_wrap');
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition_remove .ready_button').click(function(){
                        close_notification('.notice_set_emmition_remove', '.svg_wrap');
                        $('.svg_wrap radsurface').remove();
                        find_emission = '';
                        replace_emission = '';
                        $('svg path').each(function(){
                            var element = SVG.get($(this).attr('id'));
                            if(element.hasClass('custom_path')) {
                                element.remove();
                            }
                        });
                        $('.svg_wrap .chose_temperature').css('display', 'none');
                        $('.flixo_step2').removeClass('selectd_prev').addClass('active');
                        $('.flixo_step3').removeClass('active');
                        $('#chose_temperature_point, #svg').attr('class', '');
                        $('.svg_wrap svg image').css('display', 'none');
                        $('.button_emission').removeClass('emission');
                        $('.button_emission_reset_last').removeClass('active');
                        $('.emission_block').css('display','none');
                        $('.item_group_block').css('display','block');
                        var material_id = groups[current_group]['material'];
                        fill_path(current_group, material_id);
                        ready_buttons(false);
                        svg_center();
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition_ready .ready_button').click(function(){
                        close_notification('.notice_set_emmition_ready', '.svg_wrap');
                        $('.svg_wrap .chose_temperature').css('display','block');
                        $('#chose_temperature_point span').each(function(){
                            if (!$(this).hasClass('hidden')) {
                                $('#chose_temperature_point').addClass('moved').addClass($(this).attr('class'));
                                $(this).addClass('active');
                                current_temperature = $(this).attr('rel');
                                return false;
                            }
                        });
                        $('.flixo_step2').addClass('selectd_prev').removeClass('active');
                        $('.flixo_step3').addClass('active');
                        $('.svg_wrap svg image').css('display','block');
                        $('.svg_wrap #svg svg path').css('fill-opacity',"1");
                        return false;
                    });
                    $('.button_emission').click(function(){
                        if ($(this).hasClass('emission')) {
                            find_emission = '';
                            replace_emission = '';
                            $('.material_list_emmision_mainwrap').css('display','none');
                            $('.old_surface').remove();
                            $('.temp_surface').removeClass('temp_surface');

                            $('svg path').each(function(){
                                var element = SVG.get($(this).attr('id'));
                                if(element.hasClass('temp_path')) {
                                    element.removeClass('temp_path');
                                }
                            });
                            $('.button_emission_reset_last').removeClass('active');
                        }
                        $(this).toggleClass('emission');
                    });
                    $('.notice_set_emmition_not_air .ready_ok').click(function(){
                        close_notification('.notice_set_emmition_not_air', '.svg_wrap');
                        return false;
                    });
                    $('.svg_wrap .notice_set_emmition .ready_button').click(function(){
                        close_notification('.notice_set_emmition', '.svg_wrap');
                        show_notification('.notice_emmition_info', '.svg_wrap');
                        $('.emission_block').css('display','block');
                        return false;
                    });
                    $('.svg_wrap .notice_emmition_info .ready_button').click(function(){
                        close_notification('.notice_emmition_info', '.svg_wrap');
                        return false;
                    });
                    $('.svg_wrap .button_to_emmision').click(function(){
                        $('.svg_wrap .chose_temperature').css('display', 'none');
                        $('.flixo_step2').removeClass('selectd_prev').addClass('active');
                        $('.flixo_step3').removeClass('active');
                        $('#chose_temperature_point, #svg').attr('class', '');
                        $('.svg_wrap svg image').css('display', 'none');
                        $('.emission_block').css('display','block');
                    });
                    choose_temperature();
                    choose_emission();
                    $('.svg_wrap .button_result').click(function(){
                        $('.upload_preload_data_start').css('display','block');
                        var mass_push=[];

                        $('.svg_wrap').append('<div class="svg_send" style="display:none">' +$('.svg_wrap #svg').html() + '</div>');
                        $('.svg_wrap .svg_send svg').find('.svg-pan-zoom_viewport').attr('id','haha');

                        $('.svg_wrap .svg_send path').removeAttr('rel');
                        $('.svg_wrap .svg_send path.custom_path').remove();
                        $('.svg_wrap .svg_send path').removeAttr('id');
                        $('.svg_wrap .svg_send path matprop').removeAttr('material');
                        $('.svg_wrap .svg_send path bcprop').removeAttr('rel_id');
                        $('.svg_wrap .svg_send path bcprop').removeAttr('rel');
                        $('.svg_wrap .svg_send path bcprop').removeAttr('rel_img');
                        $('.svg_wrap .svg_send .image_temperature').remove();
                        if (window_count > 1) {
                            mass_push['frame'] = 'internal';
                        } else {
                            mass_push['frame'] = 'external';
                        }
                        $('.svg_wrap .svg_send svg').attr('viewbox',default_viewbox);
                        $('.svg_wrap .svg_send svg').attr('width',default_width);
                        $('.svg_wrap .svg_send svg').attr('height',default_height);
                        $('.svg_wrap .svg_send svg').removeAttr('xmlns');
                        $('.svg_wrap .svg_send svg').removeAttr('xmlns:xlink');
                        $('.svg_wrap .svg_send svg').removeAttr('xmlns:svgjs');
                        $('.svg_wrap .svg_send svg').removeAttr('id');
                        $('.svg_wrap .svg_send svg').removeAttr('style');
                        $('.svg_wrap .svg_send svg').removeAttr('xmlns:ev');
                        $('.svg_wrap .svg_send svg').removeAttr('preserveAspectRatio');
                        $('.svg_wrap .svg_send svg').removeAttr('preserveaspectratio');
                        $('.svg_wrap .svg_send svg path').each(function(){
                            $(this).attr('d',$(this).find('realpath').attr('d'));
                            $(this).css('stroke-width',parseFloat($(this).css('stroke-width'))/100);
                            $(this).find('realpath').remove();
                        });
                        //<radsurface rel_air="41" class="" startx="-22.116125" starty="-23.666552" endx="-22.116125" endy="-23.666552" eps="1">
                        $('.svg_wrap .svg_send svg radsurface').each(function(){
                            $(this).removeAttr('rel_air').removeAttr('class');
                            $(this).attr('startx',($(this).attr('startx')/100).toFixed(8));
                            $(this).attr('starty',($(this).attr('starty')/100).toFixed(8));
                            $(this).attr('endx',($(this).attr('endx')/100).toFixed(8));
                            $(this).attr('endy',($(this).attr('endy')/100).toFixed(8));
                        });

                        var group_d =$('.svg_wrap .svg_send svg #group_d');
                        var desc = $('.svg_wrap .svg_send svg desc');

                        $('.svg_wrap .svg_send svg').html('');
                        $('.svg_wrap .svg_send svg').append(desc);
                        $('.svg_wrap .svg_send svg').append(group_d);
                        $('.svg_wrap .svg_send svg #haha').remove();


                        mass_push['svg']='<?xml version="1.0" encoding="UTF-8"?>' + $('.svg_wrap .svg_send').html();
                        mass_push['material_list']=JSON.stringify(list_material_array);

                        $('.svg_wrap .svg_send').remove();
                        //TODO change ajax

                        openerp.jsonRpc('/flixo/post_svg_file_json', 'call', {
                         frame:mass_push['frame'],
                         svg:mass_push['svg'],
                         material_list:mass_push['material_list'],
                         svg_w_h:w_h_global,
                         token:$('.svg_wrap .svg_data .token').text(),
                         }).then(function(data){
                                if (data['error'] == 'True') {
                                    close_notification('.notice_temperature', '.svg_wrap');
                                    $('.notice_error_flixo .text').html(data['error_message']);
                                    show_notification('.notice_error_flixo', '.svg_wrap');
                                    $('.upload_preload_data_start').css('display','none');
                                }
                                if (data['error'] == 'False') {
                                    if (data['link'] != '') {
                                        window.location = data['link'];
                                        // clearInterval(intervalID);
                                    }
                                }
                            },"json");
                    });


                    $('.svg_wrap .upload_preload_data').css('display','none');
                    if (error == 0) {
                        show_notification('.notice_congratulation', '.svg_wrap');
                    }
                    //show_notification('.notice_ready', '.svg_wrap');
                    //show_notification('.notice_set_emmition', '.svg_wrap');
                    gets_path();
                    /* TODO openerp need delete next}*/
                //}
            });



    });
}

function line_width(x1,x2,y1,y2) {
    return Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
}

function choose_temperature() {

    $('#chose_temperature_point span').each(function(key){
        temperature.push({'class' : $(this).attr('class'), 'url' : $(this).css('background-image').slice(4, -1).replace(/"/g, ""), 'position' : '' })
        $(this).attr('id','temperature' + key).attr('rel',key);
    });

    $('#chose_temperature_point span').click(function(){

        current_temperature = $(this).attr('rel');
        if (!$('#chose_temperature_point #temperature'  + current_temperature).hasClass('active')) {
            $('#chose_temperature_point, #svg').attr('class','');
            $('#chose_temperature_point, #svg').addClass(temperature[current_temperature]['class']);
            $('#chose_temperature_point').addClass('moved');
            $('#chose_temperature_point span').removeClass('active');
            $('#chose_temperature_point #temperature'  + current_temperature).addClass('active');
        } else {
            $('#chose_temperature_point, #svg').attr('class','');
            $('#chose_temperature_point, #svg').removeClass(temperature[current_temperature]['class']);
            $('#chose_temperature_point').removeClass('moved');
            $('#chose_temperature_point span').removeClass('active');
        }
    });

   /* var mySvg = $("#svg svg")[0];
    var hammersvg = new Hammer(mySvg);
    hammersvg.on('tap',"image", function(evt) {
    });*/

    $('#svg svg').hammer({domEvents:true}).on("tap", 'image',function() {
        remove_temperature = 'true';
       /*
    });*/

    //$("#svg svg").on('click', 'image', function(evt) {
        $('#chose_temperature_point span').not('#chose_temperature_point span.hidden').removeClass('active');
        var img_id= $(this).attr('id');
        $('#chose_temperature_point #temperature'  + $('svg [rel_img="' + img_id +'"]').attr('rel_id')).removeClass('hidden').removeClass('active');
        var id_bcprop = $('svg [rel_img="' + img_id +'"]').attr('id');
        var i=0;

        $(this).remove();
        $('svg [rel_img="' + img_id +'"]').remove();
        if (id_bcprop == 'External') {
            $('svg bcprop').each(function(){
                if($(this).attr('id') == 'External') {
                    i = i + 1;
                }
            });
            if (i == 0) {
                blue_temperature = "";
            } else {
                blue_temperature = $('svg [id="External"]').attr('rel');
            }
        }
        if (id_bcprop == 'Interior') {
            $('svg bcprop').each(function(){
                if($(this).attr('id') == 'Interior') {
                    i = i + 1;
                }
            });
            if (i == 0) {
                red_temperature = "";
            } else {
                red_temperature = $('svg [id="Interior"]').attr('rel');
            }
        }
        $('#chose_temperature_point span').each(function(){
            if (!$(this).hasClass('hidden')) {
                $('#chose_temperature_point').addClass('moved').addClass($(this).attr('class'));
                $(this).addClass('active');
                current_temperature = $(this).attr('rel');
                return false;
            }
        });
        //remove_temperature = 'false';

    });
    var mySvg = $("#svg svg")[0];
    var hammersvg = new Hammer(mySvg);
    hammersvg.on('tap', function(evt) {
        //console.log(evt);
        /*alert(ev);
    });


         $("#svg").on('click', 'svg', function(evt) {*/
        if (remove_temperature == 'true') {
            remove_temperature = 'false';
            return false;
        }

        if (!$('#chose_temperature_point').hasClass('moved')) {
            return false;
        }

        $("#svg svg").find('#SvgjsDefs1001').remove();
        var mySvg = $("#svg svg")[0];

        var pt = mySvg.createSVGPoint();

        pt.x = evt.center.x;
        pt.y = evt.center.y;

        var loc = pt.matrixTransform( mySvg.firstElementChild.getScreenCTM().inverse()  );

        loc.x = loc.x;
        loc.y = loc.y;

        var my_polinome = polygonsArr[0];
        var array_points = [];
        npol = polygonsArr[0].length;
        j = 0;

        for (k = 0; k < polygonsArr.length; k++){
            npol = polygonsArr[k].length;
            var my_polinome = polygonsArr[k];
            var j = 0;
            for (i = 1; i < npol; i++){
                if ((loc.y >= my_polinome[j].y && loc.y <= my_polinome[i].y) || (loc.y <= my_polinome[j].y && loc.y >= my_polinome[i].y)) {
                    var points = [];
                    points['x'] = my_polinome[j].x;
                    points['y'] = my_polinome[j].y;
                    points['x2'] = my_polinome[i].x;
                    points['y2'] = my_polinome[i].y;
                    array_points.push(points);
                }
                j = i;
            }
            if ((loc.y >= my_polinome[0].y && loc.y <= my_polinome[npol-1].y) || (loc.y <= my_polinome[0].y && loc.y >= my_polinome[npol-1].y)) {
                var points = [];
                points['x'] = my_polinome[0].x;
                points['y'] = my_polinome[0].y;
                points['x2'] = my_polinome[npol-1].x;
                points['y2'] = my_polinome[npol-1].y;
                array_points.push(points);
            }
        }

        var min_x = '';
        var max_x = '';
        var min_points = [];
        var max_points = [];
        for (i = 0; i < array_points.length; i++){
            var temp_x = get_x(loc.y,array_points[i]['x'],array_points[i]['y'],array_points[i]['x2'],array_points[i]['y2']);

            if (min_x === '') {
                min_x = temp_x;
                max_x = temp_x;
                min_points = array_points[i];
                max_points = array_points[i];
            }

            if (min_x > temp_x) {
                min_x = temp_x;
                min_points = array_points[i];
            }
            if (max_x < temp_x) {
                max_x = temp_x;
                max_points = array_points[i];
            }
        }
        var imageW = 0.5;
        var imageH = 0.5;
        var group_d = SVG.get('group_d');
        var current_x = '';
        var current_y = '';

        if (array_points.length > 0) {
            /****************Вычисление точек*********************/

            if ($('#chose_temperature_point #temperature'+current_temperature).hasClass('blue_point')) {
                if (blue_temperature == ''){
                    if  (line_width(min_x,loc.x,loc.y,loc.y) < line_width(max_x,loc.x,loc.y,loc.y)) {
                        blue_temperature="min";
                        if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                            current_x = min_points['x'];
                            current_y = min_points['y'];
                        } else {
                            current_x = min_points['x2'];
                            current_y = min_points['y2'];
                        }
                    } else {
                        blue_temperature="max";
                        if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                            current_x = max_points['x'];
                            current_y = max_points['y'];
                        } else {
                            current_x = max_points['x2'];
                            current_y = max_points['y2'];
                        }
                    }
                } else if (blue_temperature == 'min') {
                    blue_temperature="max";
                    if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                        current_x = max_points['x'];
                        current_y = max_points['y'];
                    } else {
                        current_x = max_points['x2'];
                        current_y = max_points['y2'];
                    }
                } else {
                    blue_temperature="min";
                    if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                        current_x = min_points['x'];
                        current_y = min_points['y'];
                    } else {
                        current_x = min_points['x2'];
                        current_y = min_points['y2'];
                    }
                }
            }
            if ($('#chose_temperature_point #temperature'+current_temperature).hasClass('red_point')) {
                if (red_temperature == ''){
                    if  (line_width(min_x,loc.x,loc.y,loc.y) < line_width(max_x,loc.x,loc.y,loc.y)) {
                        red_temperature="min";
                        if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                            current_x = min_points['x'];
                            current_y = min_points['y'];
                        } else {
                            current_x = min_points['x2'];
                            current_y = min_points['y2'];
                        }
                    } else {
                        red_temperature="max";
                        if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                            current_x = max_points['x'];
                            current_y = max_points['y'];
                        } else {
                            current_x = max_points['x2'];
                            current_y = max_points['y2'];
                        }
                    }
                } else if (red_temperature == 'min') {
                    red_temperature="max";
                    if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                        current_x = max_points['x'];
                        current_y = max_points['y'];
                    } else {
                        current_x = max_points['x2'];
                        current_y = max_points['y2'];
                    }
                } else {
                    red_temperature="min";
                    if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                        current_x = min_points['x'];
                        current_y = min_points['y'];
                    } else {
                        current_x = min_points['x2'];
                        current_y = min_points['y2'];
                    }
                }
            }

            var image = group_d.image(temperature[current_temperature]['url'], imageW, imageH);

            sizes = PanZoom.getSizes();
            PanZoom.resetPan();
            PanZoom.center();
            zoom = PanZoom.getZoom();

            PanZoom.panBy({'x': (-current_x+sizes.viewBox.x+(sizes.viewBox.width)/2)*sizes.realZoom, 'y': (-current_y+sizes.viewBox.y+(sizes.viewBox.height/2))*sizes.realZoom})

            image.move(current_x - imageW / 2, current_y - imageH / 2);

            image.front().attr('class', 'image_temperature');
            $('#temperature' + current_temperature).addClass('hidden');
            if (temperature[current_temperature]['class'] == 'blue_point') {
                $('#svg svg #temperature').append("<bcProp id='External' x='" + (current_x/100).toFixed(8) + "' y='" + (current_y /100).toFixed(8) + "' temp='273.15' rs='0.04' rel_img='" + image.attr('id') + "' rel_id='" +current_temperature+ "' rel='" + blue_temperature + "'/>");
            } else{
                $('#svg svg #temperature').append("<bcProp id='Interior' x='" + (current_x/100).toFixed(8) + "' y='" + (current_y /100).toFixed(8) + "' temp='293.15' rs='0.13' rel_img='" + image.attr('id') + "' rel_id='" +current_temperature+ "' rel='" + red_temperature + "'/>");
            }
            if($('#chose_temperature_point span').not('#chose_temperature_point span.hidden').length == 0) {
                $('.chose_temperature .temperature_ready').removeClass('hidden');
            }


            $('#chose_temperature_point, #svg').attr('class','');
            $('.chose_temperature .button_reset_temperature').click(function(){
                $('#svg svg .image_temperature,#svg svg bcProp').remove();
                $('#chose_temperature_point span').removeClass('hidden');
                $('#chose_temperature_point span').removeClass('active');
                blue_temperature = '';
                red_temperature = '';
            });

            $('#chose_temperature_point span').each(function(){
                if (!$(this).hasClass('hidden')) {
                    $('#chose_temperature_point').addClass('moved').addClass($(this).attr('class'));
                    $(this).addClass('active');
                    current_temperature = $(this).attr('rel');
                    return false;
                }
            });
        }

    });
}

 function choose_emission () {
     $('.material_list_emission').on('click','.material_item',function() {
         if ($(this).hasClass('active')) {
             $('.material_list_emmision_mainwrap').css('display','none');
             $('.create_material_emmision').css('display','block');
             var newColor = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
             $(".create_material_emmision .color").css('background',newColor);

             var tarr = materials[find_emission];

             //if (tarr[3] != '') {
                 $(".create_material_emmision .lambda").val(tarr[3]);
             //}

             //if (tarr[4] != '') {
                 $(".create_material_emmision .eps").val(tarr[4]);
             //}
             //if (tarr[2] != '') {
                 $(".create_material_emmision .dencity").val(tarr[2]);
             //}
             $(".create_material_emmision .name").val(tarr[1]);
             $('.material_list_emmision_mainwrap .emission_ready').removeClass('proceed');
         } else {
             $('.material_list_emission .material_item').removeClass('active');
             $(this).addClass('active');
             find_emission = $(this).attr('material');
             $('.material_list_emmision_mainwrap .emission_ready').addClass('proceed');
         }
         return false;
     });

     $('.material_list_emmision_mainwrap .emission_ready').on('click',function(){
         if ($('.material_list_emmision_mainwrap .emission_ready').hasClass('proceed')) {
             $('.material_list_emmision_mainwrap .emission_ready').removeClass('proceed');
            $('.material_list_emmision_mainwrap').css('display','none');
            $('.create_material_emmision').css('display','block');
             var newColor = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
             $(".create_material_emmision .color").css('background',newColor);

             var tarr = materials[find_emission];

             if (tarr[3] !== '') {
                 $(".create_material_emmision .lambda").val(tarr[3]);
             }

             if (tarr[4] !== '') {
                 $(".create_material_emmision .eps").val(tarr[4]);
             }
             if (tarr[2] !== '') {
                 $(".create_material_emmision .dencity").val(tarr[2]);
             }
             $(".create_material_emmision .name").val(tarr[1]);
         }
         return false;
     });
     $('.button_emission_reset_last').click(function(){
         $('.temp_surface').remove();
         $('.old_surface').removeClass('old_surface');

         $('svg path').each(function(){
             var element = SVG.get($(this).attr('id'));
             if(element.hasClass('temp_path')) {
                 element.remove();
             }
         });
         $('.button_emission_reset_last').removeClass('active');
     });
     $('.create_material_emmision .close_create_emmision').on('click',function(){

         $('.material_list_emmision_mainwrap').css('display','block');
         $('.create_material_emmision').css('display','none');

         return false;
     });
     $('.close_find_emmision').click(function(){
         $('.material_list_emmision_mainwrap').css('display','none');
         return false;
     });
     $('.notice_set_emmition_wrong_eps .ready_button a').click(function(){
         close_notification('.notice_set_emmition_wrong_eps', '.svg_wrap');
         return false;
     });
     $('.create_material_emmision .create_ready_emmision').on('click',function(){

         //TODO change ajax

         var create_epsilon = $('.create_material_emmision .eps').val();

         if (parseFloat(create_epsilon) < 0 || parseFloat(create_epsilon) > 1 || create_epsilon === undefined || create_epsilon == '') {
             show_notification('.notice_set_emmition_wrong_eps', '.svg_wrap');
            return false;
         }

        openerp.jsonRpc('/flixo/create_material_json', 'call', {
          name:$('.svg_wrap .create_material_emmision .name').val(),
          color:rgb2hex($('.svg_wrap .create_material_emmision .color').css('background-color')),
          lambda:$('.svg_wrap .create_material_emmision .lambda').val(),
          eps:$('.svg_wrap .create_material_emmision .eps').val(),
          dencity:$('.svg_wrap .create_material_emmision .dencity').val(),
          }).then(function(data){

         /*$.post("/ajax/create_material.php",
             {
                 name:$('.svg_wrap .create_material_emmision .name').html(),
                 color:rgb2hex($('.svg_wrap .create_material_emmision .color').css('background-color')),
                 lambda:$('.svg_wrap .create_material_emmision .lambda').val(),
                 eps:$('.svg_wrap .create_material_emmision .eps').val(),
                 density:$('.svg_wrap .create_material_emmision .dencity').val(),
                 insulation_strips:$('.svg_wrap .create_material #insulation_strips_emmision').prop('checked'),
             },
             function(data){*/
                 replace_emission = data;
                 materials[replace_emission]=[];
                 materials[replace_emission][0]=replace_emission;
                 materials[replace_emission][1]=$('.svg_wrap .create_material_emmision .name').html();
                 materials[replace_emission][2]=$('.svg_wrap .create_material_emmision .dencity').val();
                 materials[replace_emission][3]=$('.svg_wrap .create_material_emmision .lambda').val();
                 materials[replace_emission][4]=$('.svg_wrap .create_material_emmision .eps').val();
                 materials[replace_emission][5]=rgb2hex($('.svg_wrap .create_material_emmision .color').css('background-color'));
                 $('.create_material_emmision').css('display','none');
             },"json");

         return false;
     });
     $('#svg svg').hammer({domEvents:true}).on("tap", 'path',function(evt) {

        if (!$('.button_emission').hasClass('emission')) {
            return false;
        }
         if ($('.create_material_emmision').css('display') == 'block') {
            return false;
         }

         var current_pos = $(this).attr('rel');

         if ($('#path'+current_pos).find('matprop').attr('material') != 'O-2000') {
             show_notification('.notice_set_emmition_not_air', '.svg_wrap');
             return false;
         }

         var polygonsArr2 = polygonsemission[current_pos];

         var poligons_array = [];
         materials_emission = [];

         if (replace_emission === '') {
             $.each(polygonsemission, function( index, value ) {
                 var emissity_value = value;
                 if (current_pos != index) {
                     $result = array_intersect(polygonsArr2, value);
                     var mat_id = $('#path'+index).find('matprop').attr('material');
                     if (Object.keys($result).length > 1 && mat_id != 'O-2000') {
                         materials_emission[mat_id]=mat_id;
                     }
                 }
             });

             $('.material_list_emmision_mainwrap').css('display','block');

             emission_scrollpane(materials_emission);
         } else {
             $('.old_surface').remove();
             $('.temp_surface').removeClass('temp_surface');

             $('svg path').each(function(){
                 var element = SVG.get($(this).attr('id'));
                 if(element.hasClass('temp_path')) {
                     element.removeClass('temp_path');
                 }
             });
             $.each(polygonsemission, function( index, value ) {
                  var emissity_value = value;
                  if (current_pos != index) {
                      $result = array_intersect(polygonsArr2, value);
                      var mat_id = $('#path'+index).find('matprop').attr('material');
                      if (Object.keys($result).length > 1 && mat_id != 'O-2000') {



                          materials_emission[mat_id]=mat_id;

                          if (poligons_array[mat_id] === undefined) {
                            poligons_array[mat_id] = [];
                          }
                          poligons_array[mat_id].push(index) ;

                          if (mat_id == find_emission) {
                              var path_d = '';
                              var i = 0;

                              var element_wrong_emission ='';
                              var elements_emission_start = [];
                              var elements_emission = [];
                              var elements_emission_sup = [];
                              $.each($result, function( index2, value2 ) {
                                  elements_emission_start[index2] = value2;
                              });



                              $.each(elements_emission_start, function( index2, value2 ) {
                                  if ( value2 !== undefined) {
                                      if (element_wrong_emission === '') {
                                          element_wrong_emission = index2;
                                          elements_emission_sup.push(value2)
                                      } else {
                                          if (parseInt(element_wrong_emission) + 1 != parseInt(index2)) {
                                              elements_emission.push(elements_emission_sup);
                                              elements_emission_sup = [];
                                              elements_emission_sup.push(value2)
                                          } else {
                                              elements_emission_sup.push(value2)
                                          }
                                          element_wrong_emission = index2;
                                      }
                                  }
                              });
                              elements_emission.push(elements_emission_sup)

                              var emission_color = materials[replace_emission][5];
                              var emission_epsilon = materials[replace_emission][4];

                              $('#path'+index +' radSurface').each(function(){
                                  if ($(this).attr('rel_air') == current_pos) {
                                      $(this).addClass('old_surface');
                                  }
                              });

                              $.each(elements_emission,function( index3, value3 ){
                                  path_d = '';
                                  i=0;
                                  if (value3.length > 1) {
                                      $.each(value3,function( index2, value2 ){
                                          if (i == 0) {
                                              path_d+='M '+value2;
                                          } else if (i == 1) {
                                              path_d+=' L '+value2;
                                          } else {
                                              path_d+=' L '+value2;
                                          }
                                          i++;
                                      });
                                      //console.log(path_d)

                                      var element = SVG.get('#path'+current_pos)
                                      var clone = element.clone();
                                      clone.front().addClass('custom_path').addClass('temp_path').attr('d', path_d).style('stroke',emission_color).style('stroke-width','0.05').style('fill','none');
                                      //$('#path'+index).find('radSurface').addClass('old_surface');


                                      var end = value3[0].split(',');
                                      var start = value3[value3.length - 1].split(',');
                                      $('#path'+index).append('<radSurface rel_air="' + current_pos + '" class="temp_surface" startX="' + start[0] + '" startY="' + start[1] + '" endX="' + end[0] + '" endY="' + end[1] + '" eps="' + emission_epsilon + '" />');
                                      $('.button_emission_reset_last').addClass('active');
                                  }
                              });
                              /*path_d = '';
                              i = 0;
                              var splice = 'none';
                              $.each(elements_emission, function( index2, value2 ) {
                                  if (value2.length < 2) {
                                      splice = index2;
                                  }
                              });

                              if (splice != 'none') {
                                  elements_emission.splice(splice, 1);
                              }

                              var elements_emission = $.map(elements_emission, function(value, index) {
                                return [value];
                              });

                              var tmp_arr = [];

                              if (elements_emission.length == 1 ){
                                  elements_emission=elements_emission[0];
                              } else {
                                  tmp_arr=elements_emission[1];
                                  for ( var i = 0, l = elements_emission[0].length; i < l; i++ ) {
                                      tmp_arr.push(elements_emission[0][i]);
                                  }
                                  elements_emission = tmp_arr;
                              }

                              i = 0;

                              $.each(elements_emission,function( index2, value2 ){
                                  if (i == 0) {
                                    path_d+='M '+value2;
                                  } else if (i == 1) {
                                    path_d+=' L '+value2;
                                  } else {
                                    path_d+=' L '+value2;
                                  }
                                  i++;
                              });*/



                          }

                      }
                  }
              });

              emission_scrollpane(materials_emission);

             //console.log(poligons_array);
         }



     });
 }
$(document).ready(function(){
    $('.svg_wrap .upload_preload_data').css('display','block');
});
$(window).load(function(){
    if (!SVG.supported) {
        alert('SVG not supported')
    } else if ($('.svg_wrap').length >= 1){
        scrollpane_emission = $('.material_list_emission');
        scrollpane_emission.jScrollPane();

        draw = SVG('svg').size('100%', '100%');

        if ($('.svg_wrap .url').length > 0) {
            show_notification('.notice_slider', '.svg_wrap');
            var swiper = new Swiper('.swiper-container',{
                pagination: '.swiper-pagination',
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
            });
            $('.notice_slider .slider_close').click(function(){
                close_notification('.notice_slider', '.svg_wrap');
                return false;
            });
            svg_success();
        }
    } else if ($('.svg_wrap_result').length >= 1) {



        $('.svg_wrap .upload_preload_data').css('display','none');
        var token = $.trim($('.svg_wrap_result .svg_data .token').text());
        if(token.substring(token.length - 4) == ',opt') {
            $('.flixo_step5').addClass('active');
            $('.flixo_step4').removeClass('active').addClass('selectd_prev');
        }

        $('.result_formula_bottom_button .result_calculate').click(function(){
            openerp.jsonRpc('/flixo/added_insulation_strips', 'call', {
                svg_w_h:w_h_global,
                token:token,
            }).then(function(data){
                window.location.href = data;
            });
            return false;
        });

        draw_material = SVG('show_material').size('100%', '100%');
        //draw_isoterm = SVG('show_isoterm').size('100%', '100%');
        $.get($('.show_material .svg_data .url').text(), function (data) {
            $('.svg_wrap_result .show_material').append('<div id="svg_temp"></div>');
            $('#svg_temp').html(new XMLSerializer().serializeToString(data.documentElement));
            $('#svg_temp svg').attr('id','temp_svg');
            var svg_html=$('#svg_temp svg').attr('id','temp_svg');
            var svg = document.getElementById('temp_svg');
            var box = svg.getAttribute('viewBox');
            box = box.split(' ');

            var x = box[0];
            var y = box[1];
            var width = box[2];
            var height = box[3];

            draw_material.viewbox(x, y, width, height);

            svg_html = svg_html[0].childNodes;

            $(svg_html).each(function(){
                $('#svg_temp').html(this);
                draw_material.svg($('#svg_temp').html());
            });
            $('#svg_temp').remove();

            /***********************************************************************************/

            var convert_polygon = [];

            var i = 0;
            $('.svg_wrap_result path,.svg_wrap_result polyline').each(function () {
                $(this).attr('id', 'path' + i);
                i++;
            });

            $('.svg_wrap_result path').each(function () {
                var path = SVG.get($(this).attr('id'));
                var i = 0;
                var d='';
                $.each(path.array().value, function(index, value) {
                    if (i == 0) {
                        d += 'M'+(parseFloat(value[1]) * 100).toFixed(6)+','+(parseFloat(value[2]) * 100).toFixed(6);
                    } else {
                        d += ' '+value[0]+(parseFloat(value[1]) * 100).toFixed(6)+','+(parseFloat(value[2]) * 100).toFixed(6);
                    }
                    i = i + 1;
                });
                $(this).append('<realpath d="' + $(this).attr('d') + '"></realpath>');
                path.plot(d);
                $(this).css('stroke-width',parseFloat($(this).css('stroke-width'))*100);
            });
            $('.svg_wrap_result polyline').each(function () {
                var path = SVG.get($(this).attr('id'));
                var i = 0;
                var d=[];
                var d_sup=[];
                $.each(path.array().value, function(index, value) {
                    d_sup=[];
                    d_sup.push((parseFloat(value[0]) * 100));
                    d_sup.push((parseFloat(value[1]) * 100));

                    d.push(d_sup);
                });
                $(this).append('<realpath d="' + $(this).attr('d') + '"></realpath>');
                path.plot(d);
                $(this).css('stroke-width',parseFloat($(this).css('stroke-width'))*100);
            });
            var box = svg.getAttribute('viewBox');
            default_viewbox = box;
            box = box.split(' ');

            var x = box[0];
            var y = box[1];
            var width = box[2];
            var height = box[3];
            draw_material.viewbox(x*100, y*100, width*100, height*100);

            i=0;
/*
            svgPanZoom = $(".svg_wrap_result .show_material svg").svgPanZoom({
                events: {
                    mouseWheel: true,
                    doubleClick: false,
                    drag: true,
                    dragCursor: "move"
                }, maxZoom: 5, panFactor: 0.05
            });
            $(".svg_wrap_result .show_material .svg.show_material").swipe( {
                pinchStatus:function(event, phase, direction, distance , duration , fingerCount, pinchZoom) {
                    svgPanZoom.events.drag= false;
                },
                pinchIn:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomIn(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                pinchOut:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomOut(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                fingers:2,
            });

            var callback = function (example) {
                return function (event) {
                    if ($(event.target).hasClass("fa-arrow-up"))
                        example.panUp()
                    if ($(event.target).hasClass("fa-arrow-down"))
                        example.panDown()
                    if ($(event.target).hasClass("fa-arrow-left"))
                        example.panLeft()
                    if ($(event.target).hasClass("fa-arrow-right"))
                        example.panRight()
                    if ($(event.target).hasClass("fa-plus"))
                        example.zoomIn()
                    if ($(event.target).hasClass("fa-minus"))
                        example.zoomOut()
                    if ($(event.target).hasClass("fa-refresh"))
                        example.reset()
                }
            };

            $("div.show_material_control.svg_controls i").click(callback(svgPanZoom));*/
            initPanZoom();
            var svg_img=$('.svg_wrap_result');
            openerp.jsonRpc('/flixo/svg_calculate_json', 'call', {token:$('.svg_wrap_result .svg_data .token').text()}).then(function(data) {
            /*$.getJSON('/ajax/calculate_json.php', { }, function(data) {*/
                if (typeof data === 'object') {
                    rebuild_temperature(svg_img,data);
                }
            });


        });

        $('.show_materials').click(function(){
            $(this).css('display','none');
            $('.result_calculate').css('display','inline-block');
            $('.show_material,.show_isoterm,.show_calculate').css('display' , 'none');
            $('.show_material').css('display' , 'block');
            return false;
        });
        $('.result_calculate').not('.result_formula_bottom_button .result_calculate').click(function(){
            $('.show_material,.show_isoterm,.show_calculate').css('display' , 'none');
            $('.show_calculate').css('display' , 'block');
            $('.show_materials').css('display','inline-block');
            $('.result_calculate').css('display','none');
            return false;
        });
    }
});

$(window).resize(function(){
    rebuild_jscroll();
});

function fill_path_by_id(material_id) {
    if ($('body').hasClass('painting')) {
        current_paint_material = material_id;
        if (material_id == 'O-2000') {
            $('.button_air').find('.change_to_air').removeClass('not_air');
        } else {
            $('.button_air').find('.change_to_air').addClass('not_air');
        }
        $('.svg_wrap .material_item').removeClass('active');
        $('.svg_wrap .material_item.material'+material_id).addClass('active')
        return false;
    }
    if ($('.svg_wrap .material_item.material'+material_id).hasClass('active')) {
        next_element();
    } else {
        fill_path(current_group, material_id);
        groups[current_group]['material']=material_id;
    }
}

function ungroup_groups() {

    var ids = groups[current_group]['ids'];
    groups[current_group]['ids']=[];
    groups[current_group]['ids'].push(ids[0]);

    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap #svg svg').find(ids[0]).css('fill-opacity', '1');
    jQuery('.svg_wrap .button_ungroup').addClass('hidden');

    var temp_arr = [];

    for (var j = 1; j < ids.length; j++) {
        temp_arr = [];
        temp_arr['material'] = groups[current_group]['material'];
        temp_arr['ids'] = [];
        temp_arr['ids'].push(ids[j]);

        groups.splice(current_group + j,0,temp_arr);
    }
}

function svg_center(){
    var center_x = 0;
    var center_y = 0;
    var center_x2 = 0;
    var center_y2 = 0;

    for ( var i = 0, l = groups[current_group]['ids'].length; i < l; i++) {
        var element = SVG.get(groups[current_group]['ids'][i]);
        var x = element.x();
        var y = element.y();
        var x2 = element.x() + element.width();
        var y2 = element.y() + element.height();
        if (center_x == 0) {
            center_x = x;
        } else if (center_x > x){
            center_x = x;
        }
        if (center_y == 0) {
            center_y = y;
        } else if (center_y > y){
            center_y = y;
        }
        if (center_x2 == 0) {
            center_x2 = x2;
        } else if (center_x2 < x2){
            center_x2 = x2;
        }
        if (center_y2 == 0) {
            center_y2 = y2;
        } else if (center_y2 < y2){
            center_y2 = y2;
        }
    }

    var my_x = (center_x2+center_x)/2;
    var my_y = (center_y2+center_y)/2;

   /* if ($(window).width() >= 960) {
        if ($('.svg_wrap .item_group_block_change').css('display') == 'block') {
            my_x = my_x*1.05;
        } else {
            my_x = my_x*1.04;
        }
    } else {
        my_x = my_x*1.05;
    }*/


    PanZoom.reset();
    /*PanZoom.resetPan();*/
    //PanZoom.zoom(1);
    //PanZoom.center();
    sizes = PanZoom.getSizes();


    var area = $('.svg_wrap svg ' + groups[current_group]['ids'][0]).find('area').attr('value');

    var zoom_count = 1;
    var zoom_countx10 = 1;

    while (area*zoom_countx10 < 1) {
        zoom_countx10 = zoom_countx10 *10;
        zoom_count = zoom_count + 1;
        if (zoom_count >20) {
            return false;
        }
    }

    if (area >= 0.0001 || groups[current_group]['ids'].length > 1) {
        PanZoom.panBy({'x': (-my_x+sizes.viewBox.x+(sizes.viewBox.width)/2)*sizes.realZoom, 'y': (-my_y+sizes.viewBox.y+(sizes.viewBox.height)/2)*sizes.realZoom});
    } else if (area < 0.0001 && area > 0.00001) {
        PanZoom.zoom(2);
        PanZoom.center();
        zoom = PanZoom.getZoom();
        PanZoom.panBy({'x': (-my_x+sizes.viewBox.x+(sizes.viewBox.width/2))*sizes.realZoom*zoom, 'y': (-my_y+sizes.viewBox.y+(sizes.viewBox.height/2))*sizes.realZoom*zoom})
    } else {
        PanZoom.zoom(zoom_count*2.5);
        PanZoom.center();
        zoom = PanZoom.getZoom();
        PanZoom.panBy({'x': (-my_x+sizes.viewBox.x+(sizes.viewBox.width)/2)*sizes.realZoom*zoom, 'y': (-my_y+sizes.viewBox.y+(sizes.viewBox.height)/2)*sizes.realZoom*zoom});
    }

}

function svg_center_error(){

    var my_x = 0;
    var my_y = 0;

    $('.svg_wrap #svg svg line,.svg_wrap #svg svg circle').css('opacity',".3");
    $('.svg_wrap #svg svg path').css('opacity',"0.3");
    $('.svg_wrap #svg svg #error'+current_errors).css('opacity', '1');

    var stroke_width = parseFloat($('#error'+current_errors).css('stroke-width'));

    stroke_width = stroke_width*3;
    $('#error'+current_errors).css('stroke-width',stroke_width);



    var element = SVG.get('#error'+current_errors);

    if (element.type == 'circle') {
        my_x = element.cx();
        my_y = element.cy();
    }
    if (element.type == 'line') {
        var data = element.bbox();
        my_x = (data.x2+data.x)/2;
        my_y = (data.y2+data.y)/2;
    }

     my_x = my_x;


    PanZoom.reset();
    sizes = PanZoom.getSizes();
     PanZoom.zoom(5);
     PanZoom.center();
     zoom = PanZoom.getZoom();
     PanZoom.panBy({'x': (-my_x+sizes.viewBox.x+(sizes.viewBox.width/2))*sizes.realZoom*zoom, 'y': (-my_y+sizes.viewBox.y+(sizes.viewBox.height/2))*sizes.realZoom*zoom});

}

function fill_path(cg, material_id) {

    if ($('body').hasClass('painting')) {
        last_painted_group = cg;
        last_painted_material = groups[cg]['material'];
    }

    var color = materials[material_id][5];
    var lambda = materials[material_id][3];
    var eps = materials[material_id][4];
    var dencity = materials[material_id][2];
    var name_change = materials[material_id][1];
    var type = materials[material_id][7];
    var paths = groups[cg]['ids'].join(',');

    groups[cg]['filled']='TRUE';

    $( ".svg_wrap .material_item" ).removeClass( "active" );

    $('.svg_wrap #svg svg path').css('fill-opacity',"0.3");
    $('.svg_wrap .item_group_block .color_block').css('background-color',color);
    $('.svg_wrap #svg svg').find(paths).css('fill', color);
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('material', material_id);
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('type', type);
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('id', material_id);
    if (lambda !== '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('lambda', lambda);
        name_change  += ' λ = ' + lambda;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('lambda');
    }
    if (eps !== '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('eps', eps);
        name_change  += ' ε = ' + eps;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('eps');
    }
    if (dencity !== '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('density', dencity);
        name_change  += ' d = ' + dencity;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('density');
    }
    $('.svg_wrap .item_group_block .name_change').html(name_change);
    $( ".svg_wrap .material_item[material='" + material_id + "']" ).addClass( "active" );
    if (!$('body').hasClass('painting')) {
        if (material_id != 'O-2000') {
            $('.change_to_air').addClass('not_air');
        } else {
            $('.change_to_air').removeClass('not_air');
        }
    }
}

function next_fill_fun(material_id) {
    if(groups[current_group]['ids'].length > 1) {
        jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
    } else {
        jQuery('.svg_wrap .button_ungroup').addClass('hidden');
        /*var box = draw.viewbox()
        var zoom = box.zoom;
        var area = $('.svg_wrap svg ' + groups[current_group]['ids'][0]).find('area').attr('value');
        if (area < 0.0001) {
            PanZoom.zoomIn(2);
        }*/
    }

    $( ".svg_wrap .material_item" ).removeClass( "active" );
    svg_center();
    if (material_id != 'none') {
        $('.item_group_block_change .button_menu').css('display','inline-block');
        fill_path(current_group, material_id);
    } else {
        fill_path_by_id($('.item_group_block_change .button_air').attr('material'));
        $('.item_group_block_change .button_menu').css('display','none');
        $('.item_group_block').css('display', 'none');
        $('.item_group_block_change').css('display', 'block');
        rebuild_jscroll();
        var api = scrollpane.data('jsp');
        api.reinitialise();
        var api_u = scrollpane_u.data('jsp');
        api_u.reinitialise();
        //side_move = 'empty_right';
        $('.svg_wrap #svg svg path').css('fill-opacity',".3");
        var paths = groups[current_group]['ids'].join(',');
        $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
        //$('.svg_wrap #svg svg').find(paths).css('fill', '#ffffff');
    }
}

function next_fill() {
    try_to_fill = 'true';
    var material_id = groups[current_group]['material'];

    if (list_material != material_id) {
        list_material = material_id;
        //TODO change ajax

        openerp.jsonRpc('/flixo/get_material_json', 'call', {
         material_id:material_id,
         token:$('.svg_wrap .svg_data .token').text()
         }).then(function(data) {
         processData_update(data);
         next_fill_fun(material_id);
         $('.svg_wrap .material_list_top input').val('');
         $('.svg_wrap .material_item').css('display','block');
         try_to_fill = 'false';
         });


        /*$.ajax({
            type: "GET",
            url: $('.svg_data .csv_second').text(),
            dataType: "json",
            success: function(data) {
                processData_update(data);
                next_fill_fun(material_id);
                $('.svg_wrap .material_list_top input').val('');
                $('.svg_wrap .material_item').css('display','block');
                try_to_fill = 'false';
            }
        });*/
    } else {
        next_fill_fun(material_id);
        try_to_fill = 'false';
    }

}

function next_element() {
    if (try_to_fill == 'true') {
        return false;
    }
    var material_id = groups[current_group]['material'];
    if (material_id != 'none') {
        current_group = current_group +1;
        if (current_group == groups.length) {
            ready_buttons(true);
            current_group = current_group - 1;
        } else {
            next_fill();
        }
    }
}

function ready_buttons(ready) {
    if (ready == true) {
        jQuery('.button_ready').addClass('visible');
        jQuery('.button_yes, .button_next').addClass('hidden');
    } else {
        jQuery('.button_ready').removeClass('visible');
        jQuery('.button_yes, .button_next').removeClass('hidden');
    }
}

function prev_element_fun(material_id) {
    if(groups[current_group]['ids'].length > 1) {
        jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
    } else {
        jQuery('.svg_wrap .button_ungroup').addClass('hidden');
    }
    $('.item_group_block_change .button_menu').css('display','inline-block');
    svg_center();
    fill_path(current_group, material_id);
    var paths = groups[current_group]['ids'].join(',');
    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
    ready_buttons(false);
}
function prev_element() {
    if (current_group == 0) {
        return false;
    }

    current_group = current_group - 1;
    var material_id = groups[current_group]['material'];
    if (list_material != material_id) {
        list_material = material_id;
        //TODO change ajax

         openerp.jsonRpc('/flixo/get_material_json', 'call', {
            material_id:material_id,
            token:$('.svg_wrap .svg_data .token').text()
            }).then(function(data) {
                    processData_update(data);
                    prev_element_fun(material_id);
                     $('.svg_wrap .material_list_top input').val('');
                     $('.svg_wrap .material_item').css('display','block');

             });
        /*$.ajax({
            type: "GET",
            url: $('.svg_data .csv_second').text(),
            dataType: "json",
            success: function(data) {
                processData_update(data);
                prev_element_fun(material_id);
                $('.svg_wrap .material_list_top input').val('');
                $('.svg_wrap .material_item').css('display','block');
            }
        });*/
    } else {
        prev_element_fun(material_id);
    }
}


jQuery(document).ready(function(){
    if ($('.svg_wrap .svg_notice').lenght > 0) {
        $('.svg_wrap .svg_notice').draggable();
    }

    $('body').on('click','.open_close',function(){
        var el = $(this);
        if (el.closest('.material_list_subwrap').hasClass('opened')) {
            $('.material_list_subwrap.opened .material_list_slide').slideUp(300);
            $('.material_list_subwrap').removeClass('opened');
        } else {
            if ($('.material_list_wrap').find('.material_list_subwrap.opened').length > 0) {
                $('.material_list_subwrap.opened .material_list_slide').slideUp(300,function(){
                    el.closest('.material_list_subwrap').find('.material_list_slide').slideDown(300,function(){
                        $('.material_list_subwrap').removeClass('opened');
                        el.closest('.material_list_subwrap').addClass('opened');
                        rebuild_jscroll();
                        var api = scrollpane.data('jsp');
                        api.reinitialise();
                        var api_u = scrollpane_u.data('jsp');
                        api_u.reinitialise();
                    });

                });
            } else {
                el.closest('.material_list_subwrap').find('.material_list_slide').slideDown(300,function(){
                    el.closest('.material_list_subwrap').addClass('opened');
                    rebuild_jscroll();
                    var api = scrollpane.data('jsp');
                    api.reinitialise();
                    var api_u = scrollpane_u.data('jsp');
                    api_u.reinitialise();
                });
            }
        }
    });

    $('body').on('click','.option_list',function(event){
        var el = $(this).closest('.material_item').find('.option_button');
        if (el.css('display') == 'none') {
            $('.option_button').css('display','none');
            el.css('display','flex');
        } else {
            el.css('display','none');
        }
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    $('.material_list_wrap').on('click','.delete_option',function(){
        var el = $(this).closest('.material_item').attr('material');
        $(this).closest('.material_item').remove();
        openerp.jsonRpc('/flixo/delete_material', 'call', {
            material_id:el,
        }).then(function(data) {});
    });
    $('.material_list_wrap').on('click','.add_option',function(){
        var el = $(this).closest('.material_item').attr('material');
        var el_obj = $(this);

        var add_material = materials[el];
        openerp.jsonRpc('/flixo/create_material_json', 'call', {
         name:add_material[1],
         color:add_material[5],
         lambda:add_material[3],
         eps:add_material[4],
         dencity:add_material[2],
         }).then(function(data){
        /*$.post("/ajax/create_material.php",
        {
            name:add_material[1],
            color:add_material[5],
            lambda:add_material[3],
            eps:add_material[4],
            density:add_material[2],
        },
        function(data){*/
            var new_material = data;
            var output=append_created(add_material[1],add_material[5],add_material[3],add_material[4],add_material[2],new_material);
            var api_u = scrollpane_u.data('jsp');
            api_u.getContentPane().prepend(output);
            //fill_path_by_id(new_material);
            /*$('.item_group_block_change').css('display', 'block');
            $('.create_material').css('display', 'none');*/

            show_notification('.notice_add_material', '.svg_wrap');
            el_obj.css('display','none');
            rebuild_jscroll();
            api_u.reinitialise();
            api_u.scrollTo(0, parseInt($(".material"+new_material).offset().top-$(".material_list").offset().top));
        },"json");
    });
    $('.notice_add_material .close_notice').click(function(){
        close_notification('.notice_add_material', '.svg_wrap');
        return false;
    });
    $('#svg,.buttons_top,.open_close,.material_list_top').click(function(){
        $('.option_button').css('display','none');
    });
});

function isPointInPoly(poly, pt){
    for(var c = false, i = -1, l = poly.length-3, j = l - 1; ++i < l; j = i)
        ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y))
        && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
        && (c = !c);
    return c;
}

function ungroup_groups_paint(cg) {
    $('.paint_button_redo').addClass('hidden');
    var paths_remove = groups[cg]['ids'].join(',');
    $('.svg_wrap #svg svg').find(paths_remove).css('stroke', 'black');
    $('.svg_wrap #svg svg').find(paths_remove).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths_remove).css('stroke-width')) / 5);
    current_paint_group = '';

    var ids = groups[cg]['ids'];
    groups[cg]['ids']=[];
    groups[cg]['ids'].push(ids[0]);

    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    jQuery('.svg_wrap .button_ungroup').addClass('hidden');

    var temp_arr = [];

    for (var j = 1; j < ids.length; j++) {
        temp_arr = [];
        temp_arr['material'] = groups[cg]['material'];
        temp_arr['ids'] = [];
        temp_arr['ids'].push(ids[j]);

        groups.splice(cg + j,0,temp_arr);
    }

}

function stop_painting(){
    $('.svg_wrap #svg svg path').css('fill-opacity',"0.3");
    $('.paint_button_redo').addClass('hidden');
    $('body').removeClass('painting');
    $('.paint_button').removeClass('active');
    $('.button_air').find('.change_to_air').addClass('not_air');
    $('.buttons_top .button_prev,.buttons_top .button_next').removeClass('hidden');
    groups.sort(function(x, y) {
        return (x['filled'] === y['filled'])? 0 : (x['filled'] === 'TRUE')? -1 : 1;
    });
    var count_filled = 0;
    $.each(groups,function(key, value){
        if (value['filled'] === 'TRUE') {
            count_filled++;
        } else {
            current_group = key;
            return false;
        }
    });
    if ((groups.length - 1) == count_filled ) {
        current_group = count_filled;
    }
    current_group = current_group - 1;
    if (current_paint_group !== '' ) {
        var paths_remove = groups[current_paint_group]['ids'].join(',');
        $('.svg_wrap #svg svg').find(paths_remove).css('stroke', 'black');
        $('.svg_wrap #svg svg').find(paths_remove).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths_remove).css('stroke-width')) / 5);
        current_paint_group = '';
    }

    var paths = groups[current_group]['ids'].join(',');
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity',"1");
    next_element();
}

function gets_path() {

    $('.paint_button_redo').click(function(){
        if (last_painted_material == 'none') {
            var paths = groups[last_painted_group]['ids'].join(',');
            $('.svg_wrap #svg svg path').css('fill-opacity',"0.3");

            $('.svg_wrap #svg svg').find(paths).css('fill', 'rgb(176, 224, 230)');
            $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('material');
            $('.svg_wrap #svg svg').find(paths).find('matprop').attr('type', 'const');
            $('.svg_wrap #svg svg').find(paths).find('matprop').attr('id', '12345');

            groups[last_painted_group]['material']='none';
            groups[last_painted_group]['filled']='FALSE';
            $('.paint_button_redo').addClass('hidden');
        } else {
            fill_path(last_painted_group, last_painted_material);
            groups[last_painted_group]['material']=last_painted_material;
            $('.paint_button_redo').addClass('hidden');
        }
        $('.svg_wrap #svg svg path').css('fill-opacity',"1");
        if (current_paint_group !== '' ) {
            var paths_remove = groups[current_paint_group]['ids'].join(',');
            $('.svg_wrap #svg svg').find(paths_remove).css('stroke', 'black');
            $('.svg_wrap #svg svg').find(paths_remove).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths_remove).css('stroke-width')) / 5);
        }

    });

    $('.paint_button').click(function(){
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('body').addClass('painting');
            current_paint_material = $('.button_air').attr('material');
            $('.svg_wrap .material_item').removeClass('active');
            $('.button_air').find('.change_to_air').removeClass('not_air');
            $('.buttons_top .button_prev,.buttons_top .button_next').addClass('hidden');
            $.each(groups,function(key, value){
                if (value['material'] !== 'none') {
                    fill_path(key, value['material']);
                }
            });
            $('.svg_wrap #svg svg path').css('fill-opacity',"1");
            PanZoom.resetZoom();
            PanZoom.center();
        } else {
            stop_painting();
        }
    });

    $('#svg svg').on("tap", 'path',function(evt) {

        if (!$('body').hasClass('painting')) {
            return false;
        }

        var current_pos = '#path' + $(this).attr('rel');
        var current_to_paint = '';
        $.each( groups, function( key, value ) {
            if (value['ids'].indexOf(current_pos) != -1) {
                current_to_paint = key;
            }

        });

        if(groups[current_to_paint]['ids'].length > 1) {
            jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
        } else {
            jQuery('.svg_wrap .button_ungroup').addClass('hidden');
        }

        if (current_paint_group !== current_to_paint ) {
            if (current_paint_group !== '' ) {
                var paths_remove = groups[current_paint_group]['ids'].join(',');
                $('.svg_wrap #svg svg').find(paths_remove).css('stroke', 'black');
                $('.svg_wrap #svg svg').find(paths_remove).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths_remove).css('stroke-width')) / 5);
            }
            var paths = groups[current_to_paint]['ids'].join(',');
            $('.svg_wrap #svg svg').find(paths).css('stroke', 'red');
            $('.svg_wrap #svg svg').find(paths).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths).css('stroke-width')) * 5);
            $('.svg_wrap #svg svg path').css('fill-opacity',"0.3");
            $('.svg_wrap #svg svg').find(paths).css('fill-opacity',"1");
            current_paint_group = current_to_paint;
        } else {
            var paths_remove = groups[current_paint_group]['ids'].join(',');
            $('.svg_wrap #svg svg').find(paths_remove).css('stroke', 'black');
            $('.svg_wrap #svg svg').find(paths_remove).css('stroke-width', parseFloat($('.svg_wrap #svg svg').find(paths_remove).css('stroke-width')) / 5);
            fill_path(current_paint_group, current_paint_material);
            groups[current_paint_group]['material']=current_paint_material;
            $('.paint_button_redo').removeClass('hidden');
            jQuery('.svg_wrap .button_ungroup').addClass('hidden');
            current_paint_group = '';

            var count_filled = 0;
            $.each(groups,function(key, value){
                if (value['filled'] === 'TRUE') {
                    count_filled++;
                } else {
                    current_group = key;
                    return false;
                }
            });
            if ((groups.length - 1) == count_filled ) {
                ready_buttons(true);
            }
            $('.svg_wrap #svg svg path').css('fill-opacity',"1");
        }



    });
}