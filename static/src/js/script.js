(function ($) {
    function getInternetExplorerVersion() {
        var rv = -1;
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
                rv = parseFloat(RegExp.$1);
        }
        else if (navigator.appName == 'Netscape') {
            var ua = navigator.userAgent;
            var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
                rv = parseFloat(RegExp.$1);
        }
        return rv;
    }

    $(window).load(function () {
        var swiper2 = new Swiper('#we-block .swiper-container', {
            slidesPerView: 5,
            loop: true,
            nextButton: '#we-block .swiper-button-next',
            prevButton: '#we-block .swiper-button-prev',
        });
    });

    $(document).ready(function () {

        $('#iqub-container.catalog3 .section .wrap-search .form-catalog .form-top .advanced-search-btn').click(function () {
            $('#iqub-container.catalog3 .section .wrap-search .form-catalog .advanced-search-container').slideToggle('slow');
            return false;
        });
        $('#iqub-container.catalog3 .section .wrap-search .form-catalog .advanced-search-container .advanced-close').click(function () {
            $('#iqub-container.catalog3 .section .wrap-search .form-catalog .advanced-search-container').slideToggle('slow');
            return false;
        });

        var form_slide_id = 1;
        var form_slide = "";

        $(".add-item-container .add-item").click(function () {

            if (form_slide_id < 5) {
                if ($("input#slide" + form_slide_id).val() != "") {
                    $("input#slide" + form_slide_id).parent().parent().parent().css("border", "0");
                    form_slide_id++;
                    form_slide = '<div class="create-article-add-slider-item">';
                    form_slide += '<div class="image-preview">';
                    form_slide += '<canvas id="images-preview-canvas-' + form_slide_id + '" class="images-preview-canvas"></canvas>';
                    form_slide += '</div>';
                    form_slide += '<div class="right-container">';
                    form_slide += '<div class="item">';
                    form_slide += '<div class="slide-number">Slide ' + form_slide_id + '</div>';
                    form_slide += '<input name="slide[]" type="file" id="slide' + form_slide_id + '">';
                    form_slide += '<label for="slide' + form_slide_id + '" class="label-file">Choose the picture</label>';
                    form_slide += '<div class="description">Min image Width:- 200 px, Max Size ofthe image - 5 MB</div>';
                    form_slide += '</div>';
                    form_slide += '<div class="item">';
                    form_slide += '<label for="caption-picture' + form_slide_id + '">The caption of the picture</label>';
                    form_slide += '<input type="text" id="caption-picture1">';
                    form_slide += '<div class="description">You can write: 200 characters</div>';
                    form_slide += '</div>';
                    form_slide += '</div>';
                    form_slide += '</div>';
                    $(".wrap_from_slide").append(form_slide);
                    if (form_slide_id == 5) {
                        $(".add-item-container").remove();
                    }
                } else {
                    $("input#slide" + form_slide_id).parent().parent().parent().css("border", "1px solid #e62e4c");
                }
            }
        });

        $('.scroll-pane').jScrollPane({
            showArrows: true,
            arrowScrollOnHover: true,
            wheelSpeed: 120,
            autoReinitialise: true
        });
        $(".show-filtr-catalog").click(function () {
            $(".filtr_radio_checkbox").slideToggle();
            return false;
        });
        if (getInternetExplorerVersion() !== -1) {
            //alert("this ie");
            $("#video-bg > div > video").css("height", "auto").parent().css("background", "#000");
        } else {
            //alert("this not ie");

        }

        var showText = function (target, message, index, interval) {
            if (index < message.length) {
                $(target).append(message[index++]);
                setTimeout(function () {
                    showText(target, message, index, interval);
                }, interval);
            }
        }
        //showText("#video-bg .section .title span", "Schon", 0, 500);

        //$("#video-bg .section .title span").text().length.each(function( index, value ){
        //  alert(index);
        //  alert(value);
        //  //setTimeout(function () { showText(target, message, index, interval); }, 100);
        //});
        //var select_text = function (uw_it) { $("#video-bg .section .title span").find(".chart_"+uw_it).addClass("active"); };
        //
        //var uw_i = $("#video-bg .section .title > span").text().length;
        //
        //while (uw_i > 0) {
        //  var i = uw_i;
        //  setTimeout(function () {
        //    $("#video-bg .section .title span").find(".chart_"+uw_i).addClass("active");
        //  }, 1000*uw_i);
        //  uw_i--;
        //}
        setTimeout(function () {
            $("#video-bg .section .title span").find(".chart_5").addClass("active");
        }, 600);
        setTimeout(function () {
            $("#video-bg .section .title span").find(".chart_4").addClass("active");
        }, 700);
        setTimeout(function () {
            $("#video-bg .section .title span").find(".chart_3").addClass("active");
        }, 800);
        setTimeout(function () {
            $("#video-bg .section .title span").find(".chart_2").addClass("active");
        }, 900);
        setTimeout(function () {
            $("#video-bg .section .title span").find(".chart_1").addClass("active");
        }, 1000);
        setTimeout(function () {
            $("#video-bg .section .title span").html("");
            showText("#video-bg .section .title span", "Jetzt", 0, 250);
        }, 2000);


        /*show query */
        $(".query_wrap").not(".active").find(".reply").css("display", "none");
        $(".query_wrap").not(".active").addClass("active");
        $(".query_wrap .query").click(function () {
            //$(this).parent().toggleClass("active");
            $(this).parent().find(".reply").slideToggle("slow", function () {
            });
            return false;
        });
        $(".query_wrap .reply .hide-btn").click(function () {
            //$(this).parent().parent().removeClass("active");
            $(this).parent().parent().find(".reply").slideToggle("slow", function () {
                $(this).parent().removeClass("active");
                $(this).parent().parent().find(".reply").css("display", "none");
            });
            return false;
        });

        /*show comment*/
        $(".comment-reply").not(".active").find(".comment-reply-content").css("display", "none");
        $(".comment-reply .comment-reply-link").click(function () {
            $(this).parent().addClass("active");
            $(this).parent().find(".comment-reply-content").slideToggle("slow", function () {
            });
            return false;
        });

        $(".add_new_comment_link").click(function () {
            $(this).slideToggle("slow", function () {
                $(this).parent().find(".comment-reply-content").slideToggle("slow", function () {
                });
            });
            return false;
        });

        /*show filtr product*/
        $(".red-btn-link.show-filtr-product").click(function () {
            //$("#iqub-container .section .page-product .product-info .right").show();
            //return false;
            $("#iqub-container .section .page-product .product-info .right").slideToggle();
            return false;
        });

        var swiper1 = new Swiper('#clients-slider .swiper-container', {
            loop: true,
            nextButton: '#clients-slider .swiper-button-next',
            prevButton: '#clients-slider .swiper-button-prev',
        });

        var languageImg = $('.language-btn .language-menu a.active').css('background-image');
        $('.language-btn').css('background-image', languageImg);
        $('.language-btn').on('click', function () {
            $(this).find('.language-menu').toggle();
        });
        $('.language-btn .language-menu a').on('click', function () {
            $('.language-btn .language-menu a').removeClass('active');
            $(this).addClass('active');
            var languageImg = $(this).css('background-image');
            $('.language-btn').css('background-image', languageImg);
        });
        $('.search-toggle').on('click', function () {
            $(this).closest('.header-top').toggleClass('active');
            if ($('#header .header-top').hasClass('active')) {
                var sectionW = $('#header .section').width();
                var inputW = sectionW - 70;
                $('#header .header-top.active .search-form input[type="text"]').width(inputW);
            } else {
                $('#header .search-form input[type="text"]').width(0);
            }
        });
        $('.menu-btn-mobile').on('click', function () {
            $('.header-menus-container').slideToggle();
            $('#wrapwrap').toggleClass('mobile_opened');
        });
        $('.message-error .close').on('click', function () {
            $(this).closest('.message-error').fadeOut();
        });
        $('.faq .toggle-link').on('click', function () {
            $(this).parent().find('.faq-container').slideToggle();
            return false;
        });
        if ($('#TagsContainer').length > 0) {
            if (!$('#TagsCanvas').tagcanvas({
                    textColour: '#044487',
                    outlineThickness: 1,
                    maxSpeed: 0.1,
                    depth: 0.5,
                    shape: 'vcylinder',
                    stretchY: 2
                })) {
                // TagCanvas failed to load
                $('#TagsContainer').hide();
            }
        }
        $(".swiper-container").each(function () {
            Swiper($(this), {
                pagination: $(this).find('.swiper-pagination'),
                paginationClickable: true
            });
        });

        jQuery(".faq_menu a").click(function () {
            if (jQuery(this).hasClass('active')) {
                jQuery(this).removeClass('active');
                jQuery(jQuery(this).attr('href')).removeClass('active');
            } else {
                jQuery(".faq_menu a").removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.faq_tab').removeClass('active');
                jQuery(jQuery(this).attr('href')).addClass('active');
            }
            return false;
        });

        $('.list-adresses-links a').on('click', function () {
            var postcode = $(this).attr('data-postcode');
            var country = $(this).attr('data-country');
            var city = $(this).attr('data-city');
            var adress = $(this).attr('data-adress');
            var title = $(this).text();
            $('.adress-form #postcode').val(postcode);
            $('.adress-form #country').val(country);
            $('.adress-form #city').val(city);
            $('.adress-form #adress').val(adress);
            $('.list-adresses-links a').removeClass('active');
            $('.adress-form h2').text(title);
            $(this).addClass('active');
            return false;
        });

        $('.add-adress').on('click', function () {
            var title = $(this).text();
            $('.list-adresses-links a').removeClass('active');
            $('.adress-form #postcode').val('');
            $('.adress-form #country').val('');
            $('.adress-form #city').val('');
            $('.adress-form #adress').val('');
            $('.adress-form h2').text(title);
            return false;
        });

        $(".wrap_from_slide").on('change', '.create-article-add-slider-item input[type="file"]', function (e) {
            var canvas = $(this).closest('.create-article-add-slider-item').find('.images-preview-canvas')[0];
            handleImage(canvas, e);
            return false;
        });

        function handleImage(canvas, e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                var ctx = canvas.getContext('2d');
                var img = new Image();
                var w = $(canvas).width();
                var h = $(canvas).height();
                $(canvas).closest('.image-preview').css('background-image', 'none');
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                img.onload = function () {
                    drawImageProp(ctx, img, 0, 0, w, h);
                    //ctx.drawImage(img,0,0,img.width,img.height,0,0,w,h);
                }
                img.src = event.target.result;
            }
            reader.readAsDataURL(e.target.files[0]);
        }

        function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

            if (arguments.length === 2) {
                x = y = 0;
                w = ctx.canvas.width;
                h = ctx.canvas.height;
            }

            /// default offset is center
            offsetX = offsetX ? offsetX : 0.5;
            offsetY = offsetY ? offsetY : 0.5;

            /// keep bounds [0.0, 1.0]
            if (offsetX < 0) offsetX = 0;
            if (offsetY < 0) offsetY = 0;
            if (offsetX > 1) offsetX = 1;
            if (offsetY > 1) offsetY = 1;

            var iw = img.width,
                ih = img.height,
                r = Math.min(w / iw, h / ih),
                nw = iw * r,   /// new prop. width
                nh = ih * r,   /// new prop. height
                cx, cy, cw, ch, ar = 1;

            /// decide which gap to fill
            if (nw < w) ar = w / nw;
            if (nh < h) ar = h / nh;
            nw *= ar;
            nh *= ar;

            /// calc source rectangle
            cw = iw / (nw / w);
            ch = ih / (nh / h);

            cx = (iw - cw) * offsetX;
            cy = (ih - ch) * offsetY;

            /// make sure source rectangle is valid
            if (cx < 0) cx = 0;
            if (cy < 0) cy = 0;
            if (cw > iw) cw = iw;
            if (ch > ih) ch = ih;

            /// fill image in dest. rectangle
            ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
        }

        $('.popup-red-btn').on('click', function () {
            $('.popup-container').fadeOut();
            $('.popup-bg').fadeOut();
        });

        $('.cabinet-post-cart .chose_file').on('click', function () {
            $('.cabinet-post-cart .attached_files input[type=file]').not('.cabinet-post-cart .attached_files.uploaded input[type=file]').click();
            return false;
        });
        $(document).on('click', '.cabinet-post-cart .uploaded_file_close', function () {
            $(this).closest('.attached_files').remove();
            return false;
        });
        $(document).on('change', '.cabinet-post-cart .attached_files input', function () {
            $(this).closest('.attached_files').addClass('uploaded');
            $(this).closest('.attached_files').find('.uploaded_file').html($(this).val());
            $('.cabinet-post-cart .attached_files_wrap').append('<div class="attached_files"><div class="uploaded_file"></div><div class="uploaded_file_close"></div><input type="file" name="uploaded_file[]"></div>');
        });
        $('.new_message').click(function () {
            $('.new_message').css('display', 'none');
            $('.upload_files_form').css('display', 'block');
            return false;
        });
        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        if ($("#company_legal_name").length > 0) {
            $("#company_legal_name").autocomplete({
                source: availableTags
            });
        }
        var frontSwiper = new Swiper('.front-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            loop: true,
            nextButton: '.front-container .swiper-button-next',
            prevButton: '.front-container .swiper-button-prev',
        });

        /***Front map***/
        /*
         var neighborhoods = [
         {lat: 52.511, lng: 13.447},
         {lat: 52.549, lng: 13.422},
         {lat: 52.497, lng: 13.396},
         {lat: 52.517, lng: 13.394}
         ];

         var markers = [];
         var map;

         function initMap() {
         map = new google.maps.Map(document.getElementById('front_map'), {
         zoom: 12,
         center: {lat: 52.520, lng: 13.410}
         });
         }

         function drop() {
         clearMarkers();
         for (var i = 0; i < neighborhoods.length; i++) {
         addMarkerWithTimeout(neighborhoods[i], i * 200);
         }
         }

         function addMarkerWithTimeout(position, timeout) {
         window.setTimeout(function() {
         markers.push(new google.maps.Marker({
         position: position,
         map: map,
         animation: google.maps.Animation.DROP
         }));
         }, timeout);
         }

         function clearMarkers() {
         for (var i = 0; i < markers.length; i++) {
         markers[i].setMap(null);
         }
         markers = [];
         }*/

        $('.seats_dimentions').on('click', '.close', function () {
            $(this).closest('.seats_dimentions_item').remove();
        });
        $('.seats_dimentions_add .append_dimention').on('click', function () {
            if ($(this).closest('.seats_dimentions_add').find('input').val() != '') {
                var append_text = "";
                append_text += "<div class='seats_dimentions_item'>";
                append_text += "<input type='text' name='dimension[]'/>";
                append_text += "<span class='value'>" + $(this).closest('.seats_dimentions_add').find('input').val() + "</span>";
                append_text += "<span class='close'>x</span>";
                append_text += "</div>"
                $(this).closest('.seats_dimentions_add').find('input').val('');
                $('.seats_dimentions_items').append(append_text);
            }
        });

        $('.catalog .list-products .group_product .product_add_to_cart').on('click', function () {
            $(this).css('display', 'none');
            var link = $(this).attr('href');
            var product_id = $(this).closest('.product').find('.product_id').val();
            var material_name = $(this).closest('.product').find('.material_name').val();
            $('.upload_preload_data_start').css('display', 'block');
            openerp.jsonRpc(link, 'call', {
                'product_id': product_id,
                'material_name': material_name,
            }).then(function (data) {
                $('.upload_preload_data_start').css('display', 'none');
                $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
                //$('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
                //$('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
                $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
                $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
                if ($('.cart-link').text() > 0) {
                    $('.cart-link').addClass('active');
                } else {
                    $('.cart-link').removeClass('active');
                }
                $('.hidden_ajax').remove();
            });
            return false;
        });

        $('.cart-link').hover(function () {
            if ($(window).width() > 1023) {
                $('.header_cart').slideDown();
            }
        });
        $('body').on('click', '.header_cart_close', function () {
            $('.header_cart').slideUp();
        });


    });

    $(window).resize(function () {
        var sectionW = $('#header .section').width();
        var leftW = $('#header .left').width();
        var inputW = sectionW - 70;
        $('#header .header-top.active .search-form input[type="text"]').width(inputW);
    });
    $(window).load(function () {
        function initMap() {
            if (typeof google !== 'undefined' && $('#contacts-map').length > 0) {
                var map = new google.maps.Map(document.getElementById('contacts-map'), {
                    zoom: 6,
                    center: {lat: 48.42731, lng: 9.044026}
                });

                var image = {
                    url: 'images/map_marker.png',
                    size: new google.maps.Size(60, 70),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(35, 70)
                };
                var marker = new google.maps.Marker({
                    position: {lat: 48.42731, lng: 9.044026},
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(map, 'zoom_changed', function () {
                    map.setCenter(marker.getPosition());
                });
                google.maps.event.addDomListener(window, 'resize', function () {
                    map.setCenter(marker.getPosition());
                });
            }
        }

        initMap();


        /**************************Front map*****************************/
        if (typeof google !== 'undefined' && $('#front_map').length > 0) {
            var map_front = new google.maps.Map(document.getElementById('front_map'), {
                zoom: 4,
                center: {lat: 48, lng: 9},
                disableDefaultUI: true,
                zoomControl: true,
                scrollwheel: false,
                styles: [
                    {
                        "featureType": "all",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#77afcf"
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "gamma": 0.01
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "saturation": -31
                            },
                            {
                                "lightness": -33
                            },
                            {
                                "weight": 2
                            },
                            {
                                "gamma": 0.8
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "hue": "#00c6ff"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "hue": "#00b0ff"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "hue": "#ff0000"
                            },
                            {
                                "visibility": "simplified"
                            },
                            {
                                "gamma": "2.02"
                            },
                            {
                                "lightness": "98"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "lightness": 30
                            },
                            {
                                "saturation": 30
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "saturation": 20
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "lightness": 20
                            },
                            {
                                "saturation": -20
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "lightness": 10
                            },
                            {
                                "saturation": -30
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "saturation": 25
                            },
                            {
                                "lightness": 25
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station.airport",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "saturation": "-13"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": -20
                            }
                        ]
                    }]
            });


            var image = {
                url: '/thermevo_website_management/static/src/images/map_marker_front.png',
                size: new google.maps.Size(46, 46),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(23, 23)
            };

            var neighborhoods = [
                {lat: 45.4626482, lng: 9.0376489}, // Milan
                {lat: 55.7498598, lng: 37.3523222}, //Moscow
                {lat: 28.6471948, lng: 76.9531796}, //New Delhi
                {lat: 51.5287718, lng: -0.2416803}, //London
                {lat: 56.9715833, lng: 23.9890815}, //Riga
                {lat: 52.5076682, lng: 13.2850549}, //Germany
            ];
            var markers = [];
            var infowindow = new google.maps.InfoWindow();
            var info = [];
            var infoWindows = [];
            info[0] = $('.map_info .Milan').html();
            info[1] = $('.map_info .Moscow').html();
            info[2] = $('.map_info .New_Delhi').html();
            info[3] = $('.map_info .London').html();
            info[4] = $('.map_info .Riga').html();
            info[5] = $('.map_info .Germany').html();
            var droped = false;

            function drop() {
                for (var i = 0; i < neighborhoods.length; i++) {
                    addMarkerWithTimeout(neighborhoods[i], i * 200, i);
                }
                $('.map_info').remove();
                droped = true;
            }

            function addMarkerWithTimeout(position, timeout, count) {
                window.setTimeout(function () {
                    markers.push(new google.maps.Marker({
                        position: position,
                        map: map_front,
                        icon: image,
                        animation: google.maps.Animation.DROP
                    }));
                    var marker = new google.maps.Marker({
                        position: position,
                        map: map_front,
                        icon: image,
                        animation: google.maps.Animation.DROP,
                    });

                    var infobox = new InfoBox({
                        content: info[count],
                        disableAutoPan: false,
                        maxWidth: 150,
                        pixelOffset: new google.maps.Size(23, -23),
                        zIndex: null,
                        boxStyle: {
                            background: "#ffffff",
                            opacity: 1,
                            width: "454px"
                        },
                        closeBoxMargin: "12px 12px 12px 12px",
                        closeBoxURL: "/thermevo_website_management/static/src/images/map_popup_close.png",
                        infoBoxClearance: new google.maps.Size(1, 1)
                    });

                    infoWindows.push(infobox);

                    google.maps.event.addListener(marker, 'click', function () {

                        for (var i = 0; i < infoWindows.length; i++) {
                            infoWindows[i].close();
                        }
                        infobox.open(map_front, this);
                        map_front.panTo(position);
                    });

                }, timeout);
            }

            var scroll = $(window).scrollTop();
            var p = $("#front_map");
            var offset = p.offset();

            if ((scroll + jQuery(window).height() - 150) >= offset.top && droped == false) {
                drop();
            }
            $(window).scroll(function (event) {
                scroll = $(window).scrollTop();
                p = $("#front_map");
                offset = p.offset();

                if ((scroll + jQuery(window).height() - 150) >= offset.top && droped == false) {
                    drop();
                }
            });

        }

        $('.svg_img').each(function () {
            var svg_img = $(this);
            $.get($(this).find('img').attr('src'), function (data) {
                svg_img.html(new XMLSerializer().serializeToString(data.documentElement));
            });
        });


    });

})(jQuery);


