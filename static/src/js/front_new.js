/**
 * Created by Sergey on 30.05.2017.
 */
    $(window).load(function(){
        //$('.main_new_front_bg').height($(window).height()-$('#header').height());
        console.log($(window).height());
        console.log($('#header').height());
    });
    $(window).resize(function(){
        //$('.main_new_front_bg').height($(window).height()-$('#header').height());
    });
    $(document).ready(function(){
        //$('.main_new_front_bg').height($(window).height()-$('#header').height());
        $('.arrow_down a').click(function(){
            $('html,body').animate({ scrollTop: $('.two_blocks_after_new_front_bg').offset().top }, 400);
            return false;
        });
        var front_3_first = new Swiper('.front_3_first .front_3_first-container', {
            // Default parameters
            slidesPerView: 3,
            spaceBetween: 17,
            loop: true,
            nextButton: '.front_3_first .swiper-button-next',
            prevButton: '.front_3_first .swiper-button-prev',
            // Responsive breakpoints
            breakpoints: {
                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                959: {
                    slidesPerView: 2,
                    spaceBetween: 30
                }
            }
        });
        var front_3_tabs_subwrap1 = new Swiper('.front_3_tabs-subwrap1 .front_3_tabs-container', {
            // Default parameters
            slidesPerView: 3,
            spaceBetween: 17,
            loop: true,
            nextButton: '.front_3_tabs-subwrap1 .swiper-button-next',
            prevButton: '.front_3_tabs-subwrap1 .swiper-button-prev',
            // Responsive breakpoints
            breakpoints: {
                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                639: {
                    slidesPerView: 1,
                    spaceBetween: 30
                },
                959: {
                    slidesPerView: 2,
                    spaceBetween: 30
                }
            }
        });
        var front_3_tabs_subwrap2 = new Swiper('.front_3_tabs-subwrap2 .front_3_tabs-container', {
            // Default parameters
            slidesPerView: 3,
            spaceBetween: 17,
            loop: true,
            nextButton: '.front_3_tabs-subwrap2 .swiper-button-next',
            prevButton: '.front_3_tabs-subwrap2 .swiper-button-prev',
            // Responsive breakpoints
            breakpoints: {
                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                639: {
                    slidesPerView: 1,
                    spaceBetween: 30
                },
                959: {
                    slidesPerView: 2,
                    spaceBetween: 30
                }
            }
        });
        if($('.front_3_tabs_ul').length > 0) {
            $('.front_3_tabs-subwrap' + $('.front_3_tabs_ul li.active').attr('rel')).css('display','block');
            front_3_tabs_subwrap1.update();
            front_3_tabs_subwrap2.update();
        }
        $('.front_3_tabs_ul li').on('click',function(){
            $('.front_3_tabs_ul li').removeClass('active');
            $(this).addClass('active');
            $('.front_3_tabs-subwraps>div').css('display','none');
            $('.front_3_tabs-subwrap' + $('.front_3_tabs_ul li.active').attr('rel')).css('display','block');
            front_3_tabs_subwrap1.update();
            front_3_tabs_subwrap2.update();
        });
        /*$('.main_new_front_bg .play-video a').on('click', function() {
            var src = $(this).closest('.acquaintance-video-container').find('iframe').attr('src') + '?autoplay=1';
            $(this).closest('.acquaintance-video-container').find('iframe').attr('src',src);
            $(this).closest('.acquaintance-video-container').toggleClass('open-video');
            if ($(window).width() <= 1024) {
                var width = $('.acquaintance-video-container').find('iframe').width()/16;
                $('.acquaintance-video-container').find('iframe').css('height',(width*9));
            } else {
                $('.acquaintance-video-container').find('iframe').css('height','');
            }
            return false;
        });*/
        /*$('#video-explant .play-video a').on('click', function() {
            var src = $(this).closest('#video-explant').find('iframe').attr('src') + '?autoplay=1';
            $(this).closest('#video-explant').find('iframe').attr('src',src);
            $(this).closest('#video-explant').toggleClass('open-video');
            if ($(window).width() <= 1024) {
                var width = $('#video-explant').find('iframe').width()/16;
                $('#video-explant').find('iframe').css('height',(width*9));
            } else {
                $('#video-explant').find('iframe').css('height','');
            }
            return false;
        });*/
        $('.client .description-client a').on('click', function() {
            $(this).closest('.description-client').toggleClass('open-text');
            //$(this).closest('.open-text').find('a').text('Hidden');
             if($(this).closest('.description-client').hasClass('open-text')){
                 $(this).closest('.open-text').find('a').text('Hidden');
             } else {
                 $(this).closest('.description-client').find('a').text('Read more');
             }
            return false;
        });

        let src = "";
        $('#video-explant0 .play-video a').on('click', function() {
            src = $(this).closest('#video-explant0').find('iframe').attr('src');
            $(this).closest('#video-explant0').find('iframe').attr('src',src + '?autoplay=1');
            $(this).closest('#video-explant0').toggleClass('open-video');
            if ($(window).width() <= 1024) {
                var width = $('#video-explant0').find('iframe').width()/16;
                $('#video-explant0').find('iframe').css('height',(width*9));
            } else {
                $('#video-explant0').find('iframe').css('height','');
            }
            return false;
        });

        $('#video-explant0 .about-video a').on('click', function() {
            $(this).closest('#video-explant0').find('iframe').attr('src',src);
            $(this).closest('#video-explant0').toggleClass('open-video');
            return false;
        });

        $('#video-explant .play-video a').on('click', function() {
            src = $(this).closest('#video-explant').find('iframe').attr('src');
            $(this).closest('#video-explant').find('iframe').attr('src',src + '?autoplay=1');
            $(this).closest('#video-explant').toggleClass('open-video');
            if ($(window).width() <= 1024) {
                var width = $('#video-explant').find('iframe').width()/16;
                $('#video-explant').find('iframe').css('height',(width*9));
            } else {
                $('#video-explant').find('iframe').css('height','');
            }
            return false;
        });

        $('#video-explant .about-video a').on('click', function() {
            $(this).closest('#video-explant').find('iframe').attr('src',src);
            $(this).closest('#video-explant').toggleClass('open-video');
            return false;
        });

        $('.main_new_front_bg .play-video a').on('click', function() {
            src = $(this).closest('.acquaintance-video-container').find('iframe').attr('src');
            $(this).closest('.acquaintance-video-container').find('iframe').attr('src',src + '?autoplay=1');
            $(this).closest('.acquaintance-video-container').toggleClass('open-video');
            if ($(window).width() <= 1024) {
                var width = $('.acquaintance-video-container').find('iframe').width()/16;
                $('.acquaintance-video-container').find('iframe').css('height',(width*9));
            } else {
                $('.acquaintance-video-container').find('iframe').css('height','');
            }
            return false;
        });

        $('.main_new_front_bg .about-video a').on('click', function() {
            console.log(src);
            $(this).closest('.acquaintance-video-container').find('iframe').attr('src',src);
            $(this).closest('.acquaintance-video-container').toggleClass('open-video');
            return false;
        });

        $(window).resize(function(){
            if ($(window).width() <= 1024) {
                var width = $('#video-explant').find('iframe').width()/16;
                $('#video-explant').find('iframe').css('height',(width*9));
            } else {
                $('#video-explant').find('iframe').css('height','');
            }
        });
        $(window).resize(function(){
            if ($(window).width() <= 1024) {
                var width = $('.acquaintance-video-container').find('iframe').width()/16;
                $('.acquaintance-video-container').find('iframe').css('height',(width*9));
            } else {
                $('.acquaintance-video-container').find('iframe').css('height','');
            }
        });

    });
