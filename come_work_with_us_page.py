# -*- coding: utf-8 -*-

from openerp import models, fields


class ClientReview(models.Model):
    _name = 'portfolio.client_review'

    client_company = fields.Char(string="Client company name",
                                 required=True)
    client_full_name = fields.Char(string="Client name")
    client_image_id = fields.Many2one('ir.attachment',
                                      string='Client image')

    text = fields.Text(string="Comment",
                       required=True)

    def get_paragraphs(self):
        """ Return list of non empty paragraphs. """
        return [paragraph for paragraph in self.text.split('\n\n') if paragraph]