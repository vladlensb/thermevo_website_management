# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
import base64


class HomePageSlider(osv.osv):
    _name = "home.page.slider"
    _rec_name = "title"
    _order = 'sequence'

    _columns = {
        'image': fields.binary('Image'),
        'title': fields.char('Title', translate=True),
        'subtitle': fields.char('Subtitle', translate=True),
        'button': fields.char('Button Text', translate=True),
        'button_link': fields.char('Button Link'),
        'show': fields.boolean('Show'),
        'id_str': fields.char('ID String'),
        'sequence': fields.integer('Sequence')
    }

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('image'):
            imgdata = base64.b64decode(vals.get('image'))
            # f = open('/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_website_management/static/images/home_first_slider/%s.jpg' % ids[0], 'wb')
            f = open('/home/odoo/openerp/addons/thermevo_website_management/static/images/home_first_slider/%s.jpg' % ids[0], 'wb')
            f.write(imgdata)
            f.close()
            s = '/thermevo_website_management/static/images/home_first_slider/%s.jpg' % ids[0]
            vals['id_str'] = s
        return super(HomePageSlider, self).write(
            cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context=None):
        home_page_slider_id = super(HomePageSlider, self).create(
            cr, uid, vals, context)
        image = self.browse(cr, uid, home_page_slider_id, context=context).image
        imgdata = base64.b64decode(image)
        # f = open('/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_website_management/static/images/home_first_slider/%s.jpg' % home_page_slider_id, 'wb')
        f = open('/home/odoo/openerp/addons/thermevo_website_management/static/images/home_first_slider/%s.jpg' % home_page_slider_id, 'wb')
        f.write(imgdata)
        f.close()
        s = '/thermevo_website_management/static/images/home_first_slider/%s.jpg' % home_page_slider_id
        self.write(cr, uid, home_page_slider_id, {'id_str': s})
        return home_page_slider_id


class HomePageSliderIQUB(osv.osv):
    _name = "home.page.slider.iqub"
    _rec_name = "title"

    _columns = {
        'image': fields.binary('Image', ),
        'title': fields.char('Title', translate=True),
        'button_link': fields.char('Button Link'),
        'show': fields.boolean('Show'),
        'id_str': fields.char('str'),
    }

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('image'):
            imgdata = base64.b64decode(vals.get('image'))
            # f = open('/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % ids[0], 'wb')
            f = open('/home/odoo/openerp/addons/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % ids[0], 'wb')
            f.write(imgdata)
            f.close()
            s = '/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % ids[0]
            vals['id_str'] = s
        return super(HomePageSliderIQUB, self).write(
            cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context=None):
        home_page_slider_id = super(HomePageSliderIQUB, self).create(
            cr, uid, vals, context)
        image = self.browse(cr, uid, home_page_slider_id, context=context).image
        imgdata = base64.b64decode(image)
        # f = open('/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % home_page_slider_id, 'wb')
        f = open('/home/odoo/openerp/addons/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % home_page_slider_id, 'wb')
        f.write(imgdata)
        f.close()
        s = '/thermevo_website_management/static/images/home_iqub_slider/%s.jpg' % home_page_slider_id
        self.write(cr, uid, home_page_slider_id, {'id_str': s})
        return home_page_slider_id
