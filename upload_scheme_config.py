from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from openerp.tools.translate import _


class upload_scheme_config(osv.osv):
    _name = "upload.scheme.config"
    _columns = {
        'type_one': fields.many2one('product.category', 'Type One'),
        'type_two': fields.many2one('product.category', 'Type Two'),
        'type_three': fields.many2one('product.category', 'Type Three'),
        'unit_of_measure': fields.many2one('product.uom', 'Measure'),
        'public_product_category': fields.many2many('product.public.category', 'upload_your_scheme_public_catgory_rel',
                                                    'config_id', 'public_categ_id', 'Product Public Category'),
        'length_variant': fields.many2one('product.attribute', 'Length'),
        'length_values': fields.many2many('product.attribute.value', 'length_val_product_attr_rel', 'length_id',
                                          'attr_val_id', 'Length Values'),
        'gluewire_variant': fields.many2one('product.attribute', 'Gluewire'),
        'gluewire_values': fields.many2many('product.attribute.value', 'gluewire_val_product_attr_rel', 'gluewire_id',
                                            'attr_val_id', 'Gluewire Values'),
        'elowprofile_variant': fields.many2one('product.attribute', 'E-low Profile'),
        'elow_profile_values': fields.many2many('product.attribute.value', 'elowprofile_val_product_attr_rel',
                                                'elowprofile_id', 'attr_val_id', 'E-low Profile Values'),
        'product_weight_category': fields.many2many('product.weight.category',
                                                    'upload_scheme_config_product_weight_category_rel', 'config_id',
                                                    'product_weight_categ_id', 'Product Categories'),
        'foam_width_steps': fields.many2many('fixed.width.profile', 'foam_width_upload_scheme_config_rel', 'width_id',
                                             'config_id', 'Foam Exception Width Step'),
        'foam_height_steps': fields.many2many('fixed.height.profile', 'foam_height_upload_scheme_config_rel',
                                              'height_id', 'config_id', 'Foam Exception Height Step'),
    }

    def create(self, cr, uid, vals, context=None):
        total_ids = self.search(cr, SUPERUSER_ID, [], context=context)
        if len(total_ids) >= 1:
            raise osv.except_osv(_("Alert"), _("You are not allowed to create more than one configuration."))
        return super(upload_scheme_config, self).create(cr, uid, vals, context=context)


upload_scheme_config()


class scheme_price_config(osv.osv):
    _name = "scheme.price.config"
    _columns = {
        'density': fields.float('Density'),

        'matrixWidth_max': fields.float('matrixWidth Max 1'),
        'matrixHeight_max': fields.float('matrixHeight Max 2'),
        'matrixWidthDeltaChannel_max': fields.float('matrixWidthDeltaChannel Max 3'),
        'matrixWidthDelta_max': fields.float('matrixWidthDelta Max 4'),
        'matrixHeightDelta_max': fields.float('matrixHeightDelta Max 5'),

        'matrixWidth_med': fields.float('matrixWidth Med 1'),
        'matrixHeight_med': fields.float('matrixHeight Med 2'),
        'matrixWidthDeltaChannel_med': fields.float('matrixWidthDeltaChannel Med 3'),
        'matrixWidthDelta_med': fields.float('matrixWidthDelta Med 4'),
        'matrixHeightDelta_med': fields.float('matrixHeightDelta Med 5'),

        'matrixWidth_min': fields.float('matrixWidth Min 1'),
        'matrixHeight_min': fields.float('matrixHeight Min 2'),
        'matrixWidthDeltaChannel_min': fields.float('matrixWidthDeltaChannel Min 3'),
        'matrixWidthDelta_min': fields.float('matrixWidthDelta Min 4'),
        'matrixHeightDelta_min': fields.float('matrixHeightDelta Min 5'),

        'value_1_max': fields.float('Max Value 1'),
        'extrusionRate_max': fields.float('Max extrusionRate'),
        'extrusionLimit_max': fields.float('Max extrusionLimit'),

        'value_1_med': fields.float('Med Value 1'),
        'extrusionRate_med': fields.float('Med extrusionRate'),
        'extrusionLimit_med': fields.float('Med extrusionLimit'),

        'value_1_min': fields.float('Min Value 1'),
        'extrusionRate_min': fields.float('Min extrusionRate'),
        'extrusionLimit_min': fields.float('Min extrusionLimit'),

        'rawMaterialPrice_max': fields.float('rawMaterialPrice Max'),
        'waist_max': fields.float('Waist Max'),
        'extrusionCost_max': fields.float('ExtrusionCost Max'),

        'rawMaterialPrice_med': fields.float('rawMaterialPrice Med'),
        'waist_med': fields.float('Waist Med'),
        'extrusionCost_med': fields.float('ExtrusionCost Med'),

        'rawMaterialPrice_min': fields.float('rawMaterialPrice Min'),
        'waist_min': fields.float('Waist Min'),
        'extrusionCost_min': fields.float('ExtrusionCost Min'),

    }

    def create(self, cr, uid, vals, context=None):
        if self.search(cr, uid, [], context=context):
            raise osv.except_osv(_('Error!'), _("You can not create more config element"))
        res = super(scheme_price_config, self).create(cr, uid, vals, context=context)
        return res


scheme_price_config()
