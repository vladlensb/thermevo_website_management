from datetime import datetime
import difflib
import lxml
import random

from openerp import tools
from openerp import SUPERUSER_ID
from openerp.addons.website.models.website import slug
from openerp.osv import osv, fields
from openerp.tools.translate import _


class foam_comment(osv.osv):
    _name = "foam.comment"
    _columns = {
        'user_id': fields.many2one('res.users', 'User'),
        'parent_id': fields.integer('Parent comment ID', default=0),
        'comment_text': fields.text('Comment text', help="Body of the comment"),
        'state': fields.boolean('State', default=True),
        'page': fields.char('Page')
    }
