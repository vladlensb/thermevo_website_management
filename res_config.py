from openerp.osv import fields, osv

class website(osv.osv):
    _inherit = "website"
    _columns = {
        'asp_server': fields.char('Asp Server Connection Link'),
    }
class website_config_settings(osv.osv_memory):
    _inherit = 'website.config.settings'
    _columns = {
        'asp_server': fields.related('website_id', 'asp_server', type="char", string="ASP Server Link"),
    }