# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import math
from datetime import datetime
import time
import re
from openerp import netsvc
from openerp.addons.web.http import request

from openerp.addons.web.http import request
import operator
import openerp
import simplejson
import simplejson as json
import requests
from openerp.addons.web import http

import logging
import werkzeug
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from openerp import SUPERUSER_ID
from openerp.http import request

from openerp.http import Controller, route, request
from openerp import http

class thermevo_advance_filter(http.Controller):

    @http.route(['/page/product_catalog/advance_search_filter'], type='json', auth="public",methods=['POST'], website=True)
    def advance_saerch(self,minimu_width,maximum_width,category_id,product_data,foam_check, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        prodcut_public_category_obj = pool['product.public.category']
        product_template_obj = pool.get('product.template')
        attribute_range_obj=pool.get('attribute.range')
        product_weight_category=pool.get('product.weight.category')
        domain = request.website.sale_product_domain()

        if(category_id=="not_category"):
            category=None
        else:
            category=category_id

        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        if not category:
            default_category= pool.get('product.public.category')
            if default_category:
                default_cat_id=prodcut_public_category_obj.search(cr,SUPERUSER_ID,[('default','=','True')],context=context)
            domain += [('public_categ_ids','child_of',default_cat_id)]

        if request.website.user_id.id == request.uid:
            domain += [('res_id', '=', '')]
        else:
            domain += [('res_id', 'in', (int(request.uid),''))]

        cat_prod_dic={}

        product_ids=product_template_obj.search(cr,SUPERUSER_ID,domain,context=context)
        for prod in product_ids:
            key=prod
            product_obj=product_template_obj.browse(cr,uid,int(prod),context=context)
            if cat_prod_dic.has_key(key):
                cat_prod_dic[key]=product_obj
            else:
                cat_prod_dic[key]=product_obj

        foam_sorting={}
        if foam_check:
            for prod_temp_id,prod_temp_obj in cat_prod_dic.items():
                for attr_rang in prod_temp_obj['product_weight_category']:
                    product_weight_id=product_weight_category.browse(cr,SUPERUSER_ID,int(attr_rang),context=context)
                    has_foam=product_weight_id.attribute_name.foam
                    if has_foam:
                        key=prod_temp_obj['id']
                        if foam_sorting.has_key(key):
                            foam_sorting[key]=prod_temp_obj
                        else:
                            foam_sorting[key]=prod_temp_obj

        # ------------------------For Width filtering only---------------------
        if minimu_width and maximum_width and not product_data:
            prod_name={}
            prod_website={}
            prod_id={}
            product_details={}
            both_sorting=[]

            if foam_sorting:
                for p_id,p_obj in foam_sorting.items():
                    for cate in p_obj['public_categ_ids']:
                        if cate.width==True:
                            c_name=cate.name
                            c_name_number=c_name.split( )[0]
                            if float(c_name_number)>=float(minimu_width) and float(c_name_number)<=float(maximum_width):
                                product_data=product_template_obj.browse(cr, SUPERUSER_ID, p_id, context=context)
                                p_key=product_data.id
                                prod_arr = []
                                prod_arr.append(product_data.id)
                                prod_name['name']=product_data.name
                                prod_id['id']=product_data.id
                                prod_website['website_published']=product_data.website_published
                                product_details[p_key]=dict(prod_name.items() + prod_id.items()+prod_website.items())
                                if product_data not in both_sorting:
                                    both_sorting.append(product_data)
                get_all_dictionary=self.make_dictionary(both_sorting)
                prod_info=get_all_dictionary['prod_info']
                prod_cat=get_all_dictionary['prod_cat']
                cat_info=get_all_dictionary['cat_info']
                length=len(product_details)
                if length==0:
                    return {
                        'error':'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length':length,
                        'prod_cat':prod_cat,
                        'prod_info':prod_info,
                        'cat_info':cat_info,
                    }
            else:
                both_sorting=[]
                for p_id,prod_obj in cat_prod_dic.items():
                    for cate in prod_obj['public_categ_ids']:
                        if cate.width==True:
                            c_name=cate.name
                            c_name_number=c_name.split( )[0]
                            if float(c_name_number)>=float(minimu_width) and float(c_name_number)<=float(maximum_width):
                                product_data=product_template_obj.browse(cr, SUPERUSER_ID, p_id, context=context)
                                p_key=product_data.id
                                prod_arr = []
                                prod_arr.append(product_data.id)
                                prod_name['name']=product_data.name
                                prod_id['id']=product_data.id
                                prod_website['website_published']=product_data.website_published
                                product_details[p_key]=dict(prod_name.items() + prod_id.items()+prod_website.items())
                                if product_data not in both_sorting:
                                    both_sorting.append(product_data)
                get_all_dictionary=self.make_dictionary(both_sorting)
                prod_info=get_all_dictionary['prod_info']
                prod_cat=get_all_dictionary['prod_cat']
                cat_info=get_all_dictionary['cat_info']
                length=len(product_details)
                if length==0:
                    return {
                        'error':'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length':length,
                        'prod_cat':prod_cat,
                        'prod_info':prod_info,
                        'cat_info':cat_info,
                    }
        # -----------------------For both width and geomerty Filtering--------------------------
        if product_data and minimu_width and maximum_width:

            prod_name={}
            prod_website={}
            prod_id={}
            product_details={}

            attrib_list = product_data
            attrib_set = []

            for i in attrib_list:
                attrib_set.append(str(i))

            if attrib_list:
                post['attrib'] = attrib_list

            if foam_sorting:
                both_sorting=[]
                for p_id,p_obj in foam_sorting.items():
                    for cate in p_obj['public_categ_ids']:
                        if cate.width==True:
                            c_name=cate.name
                            c_name_number=c_name.split( )[0]
                            if float(c_name_number)>=float(minimu_width) and float(c_name_number)<=float(maximum_width):
                                product_data_obj=product_template_obj.browse(cr, SUPERUSER_ID, p_id, context=context)
                                for cat in product_data_obj['public_categ_ids']:
                                    for att_id in attrib_set:
                                            if (int(cat.id)==int(att_id)):
                                                p_key=product_data_obj.id
                                                prod_name['name']=product_data_obj.name
                                                prod_id['id']=product_data_obj.id
                                                prod_website['website_published']=product_data_obj.website_published
                                                product_details[p_key]=dict(prod_name.items() + prod_id.items()+prod_website.items())
                                                if product_data_obj not in both_sorting:
                                                    both_sorting.append(product_data_obj)
                get_all_dictionary=self.make_dictionary(both_sorting)
                prod_info=get_all_dictionary['prod_info']
                prod_cat=get_all_dictionary['prod_cat']
                cat_info=get_all_dictionary['cat_info']
                length=len(product_details)
                if length==0:
                    return {
                        'error':'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length':length,
                        'prod_cat':prod_cat,
                        'prod_info':prod_info,
                        'cat_info':cat_info,
                    }
            else:
                both_sorting=[]
                for p_id,prod_obj in cat_prod_dic.items():
                    for cate in prod_obj['public_categ_ids']:
                        if cate.width==True:
                            c_name=cate.name
                            c_name_number=c_name.split( )[0]
                            if float(c_name_number)>=float(minimu_width) and float(c_name_number)<=float(maximum_width):
                                product_data_obj=product_template_obj.browse(cr, SUPERUSER_ID, p_id, context=context)
                                for cat in product_data_obj['public_categ_ids']:
                                    for att_id in attrib_set:
                                        if (int(cat.id)==int(att_id)):
                                            p_key=product_data_obj.id
                                            prod_name['name']=product_data_obj.name
                                            prod_id['id']=product_data_obj.id
                                            prod_website['website_published']=product_data_obj.website_published
                                            product_details[p_key]=dict(prod_name.items() + prod_id.items()+prod_website.items())
                                            if product_data_obj not in both_sorting:
                                                    both_sorting.append(product_data_obj)
                get_all_dictionary=self.make_dictionary(both_sorting)
                prod_info=get_all_dictionary['prod_info']
                prod_cat=get_all_dictionary['prod_cat']
                cat_info=get_all_dictionary['cat_info']
                length=len(product_details)
                if length==0:
                    return {
                        'error':'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length':length,
                        'prod_cat':prod_cat,
                        'prod_info':prod_info,
                        'cat_info':cat_info,
                    }

    def make_dictionary(self,both_sorting):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        prodcut_public_category_obj = pool['product.public.category']

        data = both_sorting
        prod_cat={}
        prod_info={}
        cat_name={}
        cat_width={}
        cat_geometrie={}
        category_name={}
        cat_main={}
        cat_info={}
        for d in data:
            key=d['id']
            for dd in d['public_categ_ids']:
                cat_ids= prodcut_public_category_obj.search(cr, SUPERUSER_ID, [('id', '=', int(dd.id))], context=context)
                cat_id=prodcut_public_category_obj.browse(cr, SUPERUSER_ID, cat_ids, context=context)
                cat_key=cat_id.id
                category_name['name']=cat_id.name
                cat_width['width']=cat_id.width
                cat_geometrie['geometrie']=cat_id.geometrie
                cat_main['main_category']=cat_id.main_category
                cat_info[cat_key]=dict(category_name.items() + cat_width.items()+cat_geometrie.items()+cat_main.items())
                cat_name[dd]=cat_id.name
                if prod_cat.has_key(key):
                    old = []
                    old.extend(prod_cat[key])
                    old.append(cat_name[dd])
                    prod_cat[key]=old
                else:
                    prod_cat[key]=[cat_name[dd]]

                if prod_info.has_key(key):
                    old_info = []
                    old_info.extend(prod_info[key])
                    old_info.append(cat_key)
                    prod_info[key]=old_info
                else:
                    prod_info[key]=[cat_key]
        return {
            'prod_cat':prod_cat,
            'prod_info':prod_info,
            'cat_info':cat_info,
            }
