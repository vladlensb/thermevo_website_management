import math, time, logging, werkzeug, base64, os, sys, traceback, csv
import MultipartPostHandler, urllib, urllib2, cookielib
import operator, openerp, requests

from datetime import datetime
from openerp.addons.web.http import request

from openerp.addons.web import http
from openerp.addons.website.models.website import slug
from openerp.addons.website_sale.controllers.main import website_sale, QueryURL

from openerp import SUPERUSER_ID
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from openerp.osv import osv, orm, fields

from openerp.http import Controller, route, request
from openerp import http
from openerp import tools
from PIL import Image

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class thermevo_upload_scheme(http.Controller):
    _drawing_data_per_page = 10
    
    def getfilename(self, filename, extenstion):
        index =  filename.index(extenstion)
        return filename[:index] + '.input' + filename[index:]
    
    @http.route(['/page/upload_your_scheme',
                 '/page/upload_your_scheme/<string:processed>'], type='http', auth='user', website=True)
    def upload_your_scheme_page_contents(self, processed = 'not_processed', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        
        upload_scheme = pool.get('upload.scheme.content')
        categories_obj = pool.get('product.public.category')
        res_group = pool.get('res.groups')
        
        category_ids = categories_obj.search(cr, SUPERUSER_ID, [('main_category', '=', True)], context=context)
        categories = categories_obj.browse(cr, SUPERUSER_ID, category_ids, context=context)

        upload_scheme_content_id = upload_scheme.search(cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        upload_scheme_content=upload_scheme.browse(cr, SUPERUSER_ID, upload_scheme_content_id, context=context)
        
        group_id_search=res_group.search(cr,SUPERUSER_ID,[('name','=','Access Right 2')],context=context)
        group_browse = res_group.browse(cr, SUPERUSER_ID, group_id_search, context=context)
        all_users_with_group = group_browse.users

        all_user_group = []
        for one_user in all_users_with_group:
            one_user_id = one_user.id
            all_user_group.append(one_user_id)
        show_form = False
        if uid in all_user_group:
            show_form = True

        values = {
                  'upload_scheme_content':upload_scheme_content,
                  'categories':categories,
                  'show_form':show_form,
                  }
        
        if processed == 'processed':
            values.update({'file_processed':False})
        else:
            values.update({'file_processed':True})

        return request.website.render("website.upload_your_scheme",values)