# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import math

from openerp.addons.web.http import request

import openerp
import requests
from openerp.addons.web import http
from openerp.addons.website.models.website import slug
from openerp.addons.website_sale.controllers.main import website_sale

import logging
import werkzeug
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from openerp import SUPERUSER_ID
from openerp.http import request
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
#from openerp.addons.web.controllers.main import login_redirect
from openerp.osv import osv, orm, fields
from openerp.addons.profile_management.controllers.main import OAuthLogin_inherit
from openerp.addons.auth_oauth.controllers.main import OAuthLogin

from openerp.addons.web.controllers.main import db_monodb, ensure_db, set_cookie_and_redirect, login_and_redirect

from openerp.http import Controller, route, request, HttpRequest
from openerp import http
import logging
import werkzeug
from urlparse import urljoin

import openerp
from openerp.addons.auth_signup.res_users import SignupError
from openerp import http
from openerp.http import request
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

# overright web/login to page/login
from openerp.addons.web.controllers import main
def login_redirect():
    url = '/page/login?'
    if request.debug:
        url += 'debug&'
    return """<html><head><script>
        window.location = '%sredirect=' + encodeURIComponent(window.location);
    </script></head></html>
    """ % (url,)
main.login_redirect = login_redirect


# overright http/web to http/page
def _handle_exception(self, exception):
        """Called within an except block to allow converting exceptions
           to abitrary responses. Anything returned (except None) will
           be used as response."""
        try:
            return super(HttpRequest, self)._handle_exception(exception)
        except SessionExpiredException:
            if not request.params.get('noredirect'):
                query = werkzeug.urls.url_encode({
                    'redirect': request.httprequest.url,
                })
                return werkzeug.utils.redirect('/page/login?%s' % query)
        except werkzeug.exceptions.HTTPException, e:
            return e

db_list = http.db_list
db_monodb = http.db_monodb
def abort_and_redirect(url):
    r = request.httprequest
    response = werkzeug.utils.redirect(url, 302)
    response = r.app.get_response(r, response, explicit_session=False)
    werkzeug.exceptions.abort(response)

def ensure_db(redirect='/web/database/selector'):
    # This helper should be used in web client auth="none" routes
    # if those routes needs a db to work with.
    # If the heuristics does not find any database, then the users will be
    # redirected to db selector or any url specified by `redirect` argument.
    # If the db is taken out of a query parameter, it will be checked against
    # `http.db_filter()` in order to ensure it's legit and thus avoid db
    # forgering that could lead to xss attacks.
    db = request.params.get('db')

    # Ensure db is legit
    if db and db not in http.db_filter([db]):
        db = None

    if db and not request.session.db:
        # User asked a specific database on a new session.
        # That mean the nodb router has been used to find the route
        # Depending on installed module in the database, the rendering of the page
        # may depend on data injected by the database route dispatcher.
        # Thus, we redirect the user to the same page but with the session cookie set.
        # This will force using the database route dispatcher...
        r = request.httprequest
        url_redirect = r.base_url
        if r.query_string:
            # Can't use werkzeug.wrappers.BaseRequest.url with encoded hashes:
            # https://github.com/amigrave/werkzeug/commit/b4a62433f2f7678c234cdcac6247a869f90a7eb7
            url_redirect += '?' + r.query_string
        response = werkzeug.utils.redirect(url_redirect, 302)
        request.session.db = db
        abort_and_redirect(url_redirect)

    # if db not provided, use the session one
    if not db and request.session.db and http.db_filter([request.session.db]):
        db = request.session.db

    # if no database provided and no database in session, use monodb
    if not db:
        db = db_monodb(request.httprequest)

    # if no db can be found til here, send to the database selector
    # the database selector will redirect to database manager if needed
    if not db:
        werkzeug.exceptions.abort(werkzeug.utils.redirect(redirect, 303))

    # always switch the session to the computed db
    if db != request.session.db:
        request.session.logout()
        abort_and_redirect(request.httprequest.url)

    request.session.db = db
def redirect_with_hash(*args, **kw):
    """
        .. deprecated:: 8.0

        Use the ``http.redirect_with_hash()`` function instead.
    """
    return http.redirect_with_hash(*args, **kw)


class ThermevoLogin(openerp.addons.web.controllers.main.Home):

    @http.route()
    def web_login(self, *args, **kw):
        ensure_db()
        response = super(ThermevoLogin, self).web_login(*args, **kw)
        response.qcontext.update(self.get_auth_signup_config())
        if request.httprequest.method == 'GET' and request.session.uid and request.params.get('redirect'):
            # Redirect if already logged in and redirect param is present
            return http.redirect_with_hash(request.params.get('redirect'))
        return response

    @route(['/page/login','/web/login'],
           auth="public", type='http', website=True)
    def login_page(self,redirect=None,**kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        login_email=''
        if kw.get('login') and kw.get('token'):
            login_email = kw.get('login')
            res_user_obj = pool.get('res.users')
            res_user_ids = res_user_obj.search(cr, SUPERUSER_ID,[('active','=',False),('login','=',login_email)],context=context)
            users = res_user_obj.browse(cr, SUPERUSER_ID, res_user_ids, context=context)
            if users:
                res_user_obj.action_send_welcome_email(cr, SUPERUSER_ID, res_user_ids, context=context)
                write_user_detail = res_user_obj.write(cr,SUPERUSER_ID,res_user_ids[0],{'active':True},context=context)

        qcontext = self.get_auth_signup_qcontext()
        values1 = request.params.copy()
        if 'oauth_error' in qcontext:
            error = request.params.get('oauth_error')
            if error == '1':
                error = _("Sign up is not allowed on this database.")
            elif error == '2':
                error = _("Access Denied")
            elif error == '3':
                error = _("Sorry, but a user with the same email has already registered.")
            else:
                error = None
            if error:
                values1['error'] = error
        redirect_token=qcontext.get('token')
        login=qcontext.get('login')

        if not qcontext.get('token') and not qcontext.get('reset_password_enabled'):
            raise werkzeug.exceptions.NotFound()

        if kw.get('confirm_password'):
            self.do_signup(qcontext)

        ensure_db()
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)

        if not request.uid:
            request.uid = openerp.SUPERUSER_ID

        if not redirect:
            #redirect = '/web?' + request.httprequest.query_string
            redirect = '/page/personal_profile?' + request.httprequest.query_string
        values1['redirect'] = redirect

        try:
            values1['databases'] = http.db_list()
        except openerp.exceptions.AccessDenied:
            values1['databases'] = None

        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(request.session.db, request.params['login'], request.params['password'])
            if uid is not False:
                return http.redirect_with_hash(redirect)
            request.uid = old_uid
            values1['error'] = "Wrong login/password"

        providers = OAuthLogin_inherit().list_providers()
        providers = sorted(providers, key=lambda k: k['sequence_website'])
        
        question_obj = request.registry['login.faq']
        question_ids = question_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
        login_questions = question_obj.browse(request.cr, SUPERUSER_ID, question_ids,context=context)
        
        content_obj = request.registry['login.common.content']
        content_ids = content_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        login_content = content_obj.browse(request.cr, SUPERUSER_ID, content_ids, context=context)

        values = {
            'login_email':login_email,
            'providers' : providers,
            'token':redirect_token,
            'login':login,
            'login_questions':login_questions,
            'login_content': login_content,
            'user_login': True,
        }
        values.update(values1)
        if login_email:
            values['token'] = None
        return http.request.render('website.login', values)

    def get_auth_signup_config(self):
        """retrieve the module config (which features are enabled) for the login page"""

        icp = request.registry.get('ir.config_parameter')
        return {
            'signup_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID, 'auth_signup.allow_uninvited') == 'True',
            'reset_password_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID, 'auth_signup.reset_password') == 'True',
        }

    def get_auth_signup_qcontext(self):
        """ Shared helper returning the rendering context for signup and reset password """
        qcontext = request.params.copy()
        qcontext.update(self.get_auth_signup_config())
        if qcontext.get('token'):
            try:
                # retrieve the user info (name, login or email) corresponding to a signup token
                res_partner = request.registry.get('res.partner')
                token_infos = res_partner.signup_retrieve_info(request.cr, openerp.SUPERUSER_ID, qcontext.get('token'))
                for k, v in token_infos.items():
                    qcontext.setdefault(k, v)
            except:
                qcontext['error'] = _("Invalid signup token")
        return qcontext

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key)) for key in ('login', 'name', 'password'))
        assert any([k for k in values.values()]), "The form was not properly filled in."
        #assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()

    def _signup_with_values(self, token, values):
        db, login, password = request.registry['res.users'].signup(request.cr, openerp.SUPERUSER_ID, values, token)
        request.cr.commit()     # as authenticate will use its own cursor we need to commit the current transaction
        uid = request.session.authenticate(db, login, password)
        if not uid:
            raise SignupError(_('Authentification failed'))



class res_partner(osv.Model):
    _inherit = 'res.partner'

    def _get_signup_url_for_action(self, cr, uid, ids, action=None, view_type=None, menu_id=None, res_id=None, model=None, context=None):
        """ generate a signup url for the given partner ids and action, possibly overriding
            the url state components (menu_id, id, view_type) """
        if context is None:
            context= {}
        res = dict.fromkeys(ids, False)
        base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')
        for partner in self.browse(cr, uid, ids, context):
            # when required, make sure the partner has a valid signup token
            if context.get('signup_valid') and not partner.user_ids:
                self.signup_prepare(cr, uid, [partner.id], context=context)

            route = 'login'
            # the parameters to encode for the query
            query = dict(db=cr.dbname)
            signup_type = context.get('signup_force_type_in_url', partner.signup_type or '')
            if signup_type:
                route = 'login' if signup_type == 'reset' else signup_type

            if partner.signup_token and signup_type:
                query['token'] = partner.signup_token
            elif partner.user_ids:
                query['login'] = partner.user_ids[0].login
            else:
                continue        # no signup token, no user, thus no signup url!

            fragment = dict()
            if action:
                fragment['action'] = action
            if view_type:
                fragment['view_type'] = view_type
            if menu_id:
                fragment['menu_id'] = menu_id
            if model:
                fragment['model'] = model
            if res_id:
                fragment['id'] = res_id

            if fragment:
                query['redirect'] = '/page#' + werkzeug.url_encode(fragment)

            res[partner.id] = urljoin(base_url, "/page/%s?%s" % (route, werkzeug.url_encode(query)))

        return res

    def _get_signup_url(self, cr, uid, ids, name, arg, context=None):
        """ proxy for function field towards actual implementation """
        return self._get_signup_url_for_action(cr, uid, ids, context=context)

class ThermevoRegister(openerp.addons.web.controllers.main.Home):

    @http.route()
    def web_login(self, *args, **kw):
        ensure_db()
        response = super(ThermevoRegister, self).web_login(*args, **kw)
        response.qcontext.update(self.get_auth_signup_config())
        if request.httprequest.method == 'GET' and request.session.uid and request.params.get('redirect'):
            # Redirect if already logged in and redirect param is present
            return http.redirect_with_hash(request.params.get('redirect')) 
        return response

    @http.route(['/page/registration',
                  '/web/signup'], type='http', auth='public', website=True)
    def web_page_auth_signup(self, *args, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        qcontext = self.get_auth_signup_qcontext()
        #print"----------------------------------args====",args
        #print"---------------------------------kw=====",kw
        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                # code to send verification mail
                self._send_verification_mail()
                #return request.redirect('/page/sign_in')
                #print "\n\n\n===========>", args
                #return super(ThermevoRegister, self).web_login(*args, **kw)
                return request.redirect('/page/sign_in')
            except (SignupError, AssertionError), e:
                qcontext['error'] = _(e.message)
        providers = OAuthLogin_inherit().list_providers()
        providers = sorted(providers, key=lambda k: k['sequence_website'])

        question_obj = request.registry['registration.faq']
        question_ids = question_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
        registration_questions = question_obj.browse(request.cr, SUPERUSER_ID, question_ids,context=context)
        
        content_obj = request.registry['login.common.content']
        content_ids = content_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1, context=request.context)
        login_content = content_obj.browse(request.cr, SUPERUSER_ID, content_ids, context=request.context)
        
        values = {
            'providers' : providers,
            'qcontext'  : qcontext,
            'registration_questions': registration_questions,
            'login_content': login_content,
            'user_login': True,
        }
        #print"-------------------values--------------------------",values
        return request.render('website.registration',values)

    @http.route('/page/forgot_password', type='http', auth='public', website=True)
    def web_page_auth_reset_password(self, *args, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        qcontext = self.get_auth_signup_qcontext()
        
        question_obj = request.registry['forgot.password.faq']
        question_ids = question_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
        forgot_pwd_questions = question_obj.browse(request.cr, SUPERUSER_ID, question_ids,context=context)
        
        content_obj = request.registry['login.common.content']
        content_ids = content_obj.search(request.cr, SUPERUSER_ID, [], offset=0, limit=1, context=request.context)
        login_content = content_obj.browse(request.cr, SUPERUSER_ID, content_ids, context=request.context)

        values = {
                  'forgot_pwd_questions':forgot_pwd_questions,
                  'login_content':login_content,
                  'user_login': True,
                }

        if not qcontext.get('token') and not qcontext.get('reset_password_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                if qcontext.get('token'):
                    self.do_signup(qcontext)
                    return super(ThermevoRegister, self).web_login(*args, **kw)
                else:
                    login = qcontext.get('login')
                    assert login, "No login provided."
                    res_users = request.registry.get('res.users')
                    res_users.reset_password(request.cr, openerp.SUPERUSER_ID, login)
                    qcontext['message'] = _("An email has been sent with credentials to reset your password")
            except SignupError:
                qcontext['error'] = _("Could not reset your password")
                values.update({'error':qcontext['error']})
                _logger.exception('error when resetting password')
            except Exception, e:
                qcontext['error'] = _(e.message)
                values.update({'error':qcontext['error']})
        return request.render('website.forgot_password', values)

    def get_auth_signup_config(self):
        """retrieve the module config (which features are enabled) for the login page"""

        icp = request.registry.get('ir.config_parameter')
        return {
            'signup_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID, 'auth_signup.allow_uninvited') == 'True',
            'reset_password_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID, 'auth_signup.reset_password') == 'True',
        }

    def get_auth_signup_qcontext(self):
        """ Shared helper returning the rendering context for signup and reset password """
        qcontext = request.params.copy()
        qcontext.update(self.get_auth_signup_config())
        if qcontext.get('token'):
            try:
                # retrieve the user info (name, login or email) corresponding to a signup token
                res_partner = request.registry.get('res.partner')
                token_infos = res_partner.signup_retrieve_info(request.cr, openerp.SUPERUSER_ID, qcontext.get('token'))
                for k, v in token_infos.items():
                    qcontext.setdefault(k, v)
            except:
                qcontext['error'] = _("Invalid signup token")
        return qcontext

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key)) for key in ('login', 'name', 'password','last_name'))
        assert any([k for k in values.values()]), "The form was not properly filled in."
        #assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()

    def _signup_with_values(self, token, values):
        db, login, password = request.registry['res.users'].signup(request.cr, openerp.SUPERUSER_ID, values, token)
        request.cr.commit()     # as authenticate will use its own cursor we need to commit the current transaction
        # uid = request.session.authenticate(db, login, password)
        # if not uid:
        #     raise SignupError(_('Authentification Failed.'))
    
    def _send_verification_mail(self):
        print "\n\n\n\n"
        qcontext = self.get_auth_signup_qcontext()
        login = qcontext.get('login')
        res_users = request.registry.get('res.users')
        res_users.user_varification_mail(request.cr, openerp.SUPERUSER_ID, login)
        qcontext['message'] = _("An email has been sent with credentials to reset your password")
        print "this is the qcontext=====>", qcontext
        
    @http.route(['/page/registration/checkemail'], type='json', auth="public", methods=['POST'], website=True)
    def check_user_email(self, **kw):
        email = kw.get('email')
        cr, context, pool = request.cr, request.context, request.registry
        res_partner = pool.get('res.partner')
        res_users = pool.get('res.users')
        email_ids=res_partner.search(cr, SUPERUSER_ID,[('email','=',email)],context=context)
        login_ids=res_users.search(cr, SUPERUSER_ID,[('login','=',email)],context=context)
        if(login_ids):
            return False
            if(email_ids):
                return False
        return True