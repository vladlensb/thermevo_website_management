# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-Today OpenERP SA (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import math
from datetime import datetime
from openerp.addons.profile_management.controllers.main \
    import OAuthLogin_inherit
import operator

import logging
import werkzeug

from openerp import SUPERUSER_ID
from openerp.addons.website.models.website import slug
from werkzeug.utils import redirect

from openerp.http import Controller, route, request
from openerp import http
import base64
from geoip import geolite2

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger(__name__)

PPG = 500  # Products Per Page
PPR = 4  # Products Per Row
dup_unit_price = {}

try:
    from openerp.addons.thermevo_blog_v2.controllers.main \
        import get_home_page_data
except:
    pass


class ThermevoWebsiteManagement(http.Controller):
    @http.route(['/page/therm_website'],
                type='http', auth="public", website=True)
    def get_home_page_data(self, enable_editor=None):
        cr = request.cr
        context = request.context
        pool = request.registry
        values = {}
        try:
            values = get_home_page_data(values)
        except:
            pass

        home_content_obj = pool.get('home.content')
        home_content_obj_id = home_content_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        home_content = home_content_obj.browse(
            cr, SUPERUSER_ID, home_content_obj_id, context=context)

        home_page_slider_pool = pool.get('home.page.slider')
        home_page_slider_ids = home_page_slider_pool.search(
            cr, SUPERUSER_ID, [('show', '=', True)], context=context)
        home_page_slider_objects = home_page_slider_pool.browse(
            cr, SUPERUSER_ID, home_page_slider_ids, context=context)

        home_page_slider_iqub_pool = pool.get('home.page.slider.iqub')
        home_page_slider_iqub_ids = home_page_slider_iqub_pool.search(
            cr, SUPERUSER_ID, [('show', '=', True)], context=context)
        home_page_slider_iqub_objects = home_page_slider_iqub_pool.browse(
            cr, SUPERUSER_ID, home_page_slider_iqub_ids, context=context)

        blog_post_pool = pool.get('blog.post')
        home_page_tech_slider_ids = blog_post_pool.search(
            cr, SUPERUSER_ID, [('blog_type','=', 'tech'),
                               ('website_published', '=', True)])
        home_page_tech_slider_objects = blog_post_pool.browse(
            cr, SUPERUSER_ID, home_page_tech_slider_ids,context=context)

        blog_post_pool = pool.get('blog.post')
        home_page_market_slider_ids = blog_post_pool.search(
            cr, SUPERUSER_ID, [('blog_type', '=', 'market'),
                               ('website_published', '=', True)])
        home_page_market_slider_objects = blog_post_pool.browse(
            cr, SUPERUSER_ID, home_page_market_slider_ids, context=context)

        values['home_content'] = home_content
        values['home_page_slider_objects'] =home_page_slider_objects
        values['home_page_slider_iqub_objects'] = home_page_slider_iqub_objects
        values['home_page_tech_slider_objects'] = home_page_tech_slider_objects
        values['home_page_market_slider_objects'] = home_page_market_slider_objects


        return request.website.render("website.thermevo_home_page", values)

    @http.route(['/shop/cart/update_custom_profile_length'],
                type='json', auth="public", methods=['POST'], website=True)
    def cart_update_custom_profile_length(self, product_data, **kw):
        cr = request.cr
        uid = request.uid
        context = request.context
        registry = request.registry

        product_attribute_line = registry.get('product.attribute.line')
        product_attribute_value = registry.get('product.attribute.value')
        product_template = registry.get('product.template')

        custom_variant_value = product_data[0].replace(",", ".")
        product_temp_id = product_data[2]
        product_attr = product_data[3]
        product_attr_line = product_data[4]

        product_attr_line = product_attribute_line.browse(
            cr, uid, int(product_attr_line), context=context)
        product_length_variants = product_attr_line.value_ids.ids

        product_attribute_value_data = {
            'name': custom_variant_value,
            'attribute_id': product_attr,
            'res_id': uid
        }
        vals = {}
        try:
            val_id = product_attribute_value.create(
                cr, uid, product_attribute_value_data)
            product_length_variants.append(val_id)
            vals['attribute_line_ids'] = [[
                1, int(product_attr_line),
                {'value_ids':
                     [[6, False, product_length_variants]]}]]
            product_template.write(cr, uid, int(product_temp_id), vals, context)
        except Exception:
            print "\n\nIntegrity Error - Entered length already exists.\n\n"
            return {'error': 'length_not_permitted'}
        return {'error': 'no_error'}

    def dump(self, obj):
        for attr in dir(obj):
            print "obj.%s = %s" % (attr, getattr(obj, attr))

    @http.route(['/shop/get_unit_price'],
                type='json', auth="public", methods=['POST'], website=True)
    def get_unit_price(self, product_ids, add_qty, line_id, **kw):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        if line_id:
            print "add_qty==>>", add_qty

            sale_order_line = pool.get('sale.order.line')
            sale_order_line_data = sale_order_line.browse(
                cr, uid, int(line_id),context=context)
            print "\n\n\nCurrent Line id is ====> ",line_id
            products = pool['product.product'].browse(
                cr, uid, product_ids, context=context)
            partner = pool['res.users'].browse(
                cr, uid, uid, context=context).partner_id
            pricelist_id = request.session.get('sale_order_code_pricelist_id')\
                           or partner.property_product_pricelist.id
            prices = pool['product.pricelist'].price_rule_get_multi(
                cr, uid, [],
                [(product, add_qty, partner) for product in products],
                context=context)
            print "\n\n\n Actualy Return from get_unit_price ===========> ",\
                {product_id: prices[product_id][pricelist_id][0]
                 for product_id in product_ids}
            print "\n\n\n Formaly Return from get_unit_price ===========> ",\
                {products.id: sale_order_line_data.price_unit}
            return {products.id: sale_order_line_data.price_unit}
        else:

            products = pool['product.product'].browse(
                cr, uid, product_ids, context=context)
            partner = pool['res.users'].browse(
                cr, uid, uid, context=context).partner_id
            pricelist_id = request.session.get('sale_order_code_pricelist_id') \
                           or partner.property_product_pricelist.id
            prices = pool['product.pricelist'].price_rule_get_multi(
                cr, uid, [],
                [(product, add_qty, partner) for product in products],
                context=context)
            return {product_id: prices[product_id][pricelist_id][0]
                    for product_id in product_ids}


class Website(Controller):
    @route('/page/therm_website', auth="public", type='http', website=True)
    def index(self, **kw):
        return http.request.render('website.thermevo_home_page', {})

    @http.route('/thermevo_and_clients/<model("portfolio.project"):project>/',
                auth="public", type='http', website=True)
    def project_page(self, project):
        project.image_ids.sorted(key=lambda r: r.create_date)

        values = {'project': project}
        return http.request.render('website.portfolio_project_page', values)

    @http.route('/portfolio', auth="public", type="http", website=True)
    def portfolio_list(self):
        portfolio_projects = request.env['portfolio.project'].search([])
        category_list = list(
            set([portfolio_project.category
                 for portfolio_project in portfolio_projects]))
        # This dictionary will contain the category name
        # and portfolio_project IDs
        filtered_category = {}

        for portfolio_project in portfolio_projects:
            for unic_category in category_list:
                if str(unic_category) == str(portfolio_project.category):
                    if unic_category in filtered_category:
                        filtered_category[
                            unic_category].append(portfolio_project)
                    else:
                        filtered_category[unic_category] = [portfolio_project]

        context_dir = {'categorys': category_list,
                       'f_categorys': filtered_category
                       }

        return http.request.render('website.portfolio_page', context_dir)

    def render_promo_page(self, base_url, value=None):
        if not value:
            value = {}
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        promo_pool = pool.get('promo.page')
        page_id = promo_pool.search(cr, uid, [('link', '=', base_url)], limit=1)
        page = promo_pool.browse(cr, uid, page_id, context=context)
        try:
            page= page[0]
        except:
            pass
        link = 'website.{0}'.format(base_url)
        value['promo_page'] = page
        return http.request.render(link, value)

    @http.route('/come_work_with_us', auth="public", type="http", website=True)
    def come_work_with_us(self):
        value = {'client_reviews': request.env['portfolio.client_review'].search([])}
        url_root = request.httprequest.url_root
        base_url = request.httprequest.url.replace(url_root, '')
        return self.render_promo_page(base_url, value)

    @http.route('/prize', auth="public", type="http", website=True)
    def promo_prize_page(self):
        url_root = request.httprequest.url_root
        base_url = request.httprequest.url.replace(url_root, '')
        return self.render_promo_page(base_url=base_url)

    @http.route('/thermal_break_community', auth="public", type="http", website=True)
    def thermal_break_community_page(self):
        url_root = request.httprequest.url_root
        base_url = request.httprequest.url.replace(url_root, '')
        return self.render_promo_page(base_url)



class ProductCatalog(http.Controller):
    _post_per_page = 10

    @http.route(['/page/product_catalog/get_unit_price'],
                type='json', auth="public", methods=['POST'], website=True)
    def get_unit_price(self, product_ids, add_qty, line_id, **kw):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        if line_id:
            sale_order_line = pool.get('sale.order.line')
            sale_order_line_data = sale_order_line.browse(
                cr, uid, int(line_id),context=context)
            products = pool['product.product'].browse(
                cr, uid, product_ids, context=context)
            partner = pool['res.users'].browse(
                cr, uid, uid, context=context).partner_id
            pricelist_id = request.session.get('sale_order_code_pricelist_id')\
                           or partner.property_product_pricelist.id
            prices = pool['product.pricelist'].price_rule_get_multi(
                cr, uid, [],
                [(product, add_qty, partner) for product in products],
                context=context)
            return {products.id: sale_order_line_data.price_unit}
        else:
            products = pool['product.product'].browse(
                cr, uid, product_ids, context=context)
            partner = pool['res.users'].browse(
                cr, uid, uid, context=context).partner_id
            pricelist_id = request.session.get('sale_order_code_pricelist_id') \
                           or partner.property_product_pricelist.id
            prices = pool['product.pricelist'].price_rule_get_multi(
                cr, uid, [],
                [(product, add_qty, partner) for product in products],
                context=context)
            return {product_id: prices[product_id][pricelist_id][0]
                    for product_id in product_ids}

    @http.route(['/page/validate'],
                type='http', auth="public", methods=['POST'], website=True)
    def confirm_annual_quantity(self, total_quantity=0, annual_total=0, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        if not annual_total:
            annual_total = total_quantity
        res_partner = pool.get('res.partner')
        user_obj = pool.get('res.users')
        msg = "invalid"
        if int(total_quantity) > int(annual_total) or annual_total == 0:

            redirect_url = "/page/confirmation/?data=" + msg
            return request.redirect(redirect_url)
        else:
            if request.uid != request.website.user_id.id:
                partner_id = user_obj.browse(
                    cr, SUPERUSER_ID, uid, context=context).partner_id.id
                annual_vol = int(annual_total) - int(total_quantity)
                res_partner.write(cr, uid, [partner_id],
                                  {'annual_volume': annual_vol},
                                  context=context)
            cart = request.website.sale_get_order(context=context)
            if cart:
                status = request.website.sale_get_order(force_create=1)
                redirect_url = "/page/commercial_proposal/%s" % slug(status)
            else:
                redirect_url = "/page/confirmation/?data=" + msg
            return request.redirect(redirect_url)

    def get_pricelist(self):
        return get_pricelist()

    def get_attribute_value_ids(self, product):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        currency_obj = pool['res.currency']
        attribute_value_ids = []
        visible_attrs = set(l.attribute_id.id
                                for l in product.attribute_line_ids
                                    if len(l.value_ids) > 1)
        if request.website.pricelist_id.id != context['pricelist']:
            website_currency_id = request.website.currency_id.id
            currency_id = self.get_pricelist().currency_id.id
            for p in product.product_variant_ids:
                price = currency_obj.compute(
                    cr, uid, website_currency_id, currency_id, p.lst_price)
                attribute_value_ids.append(
                    [p.id,
                     [v.id for v in p.attribute_value_ids
                      if v.attribute_id.id in visible_attrs], p.price, price])
        else:
            attribute_value_ids = [
                [p.id, [v.id for v in p.attribute_value_ids
                        if v.attribute_id.id in visible_attrs],
                 p.price, p.lst_price]
                for p in product.product_variant_ids]

        return attribute_value_ids

    @http.route(['/page/userlocation'],
                type='json', auth="public", methods=['POST'], website=True)
    def get_user_location(self, **kw):
        ip_addr = kw.get('ipaddr')
        iplookup = geolite2.lookup(ip_addr)
        lat_long = None
        if iplookup is not None:
            lat_long = iplookup.location
            return {'lat':lat_long[0],'long':lat_long[1]}

    @http.route(
        ['/page/product_catalog/product/comment/<int:product_template_id>'],
        type='http', auth="public", methods=['POST'], website=True)
    def product_comment(self, product_template_id, **post):
        if not request.session.uid:
            return login_redirect()
        cr, uid, context = request.cr, request.uid, request.context
        if post.get('comment'):
            request.registry['product.template'].message_post(
                cr, uid, product_template_id,
                body=post.get('comment'),
                type='comment',
                subtype='mt_comment',
                context=dict(context, mail_create_nosubscribe=True))
        return werkzeug.utils.redirect(
            request.httprequest.referrer + "#comments")

#------------------------For options.xml page-------------------------

    def get_order_link(self, cr, uid, ids, context=None):
        crm_case_section_pool = request.registry.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)
        user = request.registry.get('res.users').browse(
            cr, uid, uid, context=context)
        order_link = ''
        for crm_case_section in crm_case_sections:
            if user in crm_case_section.member_ids:
                order_link = 'web#id=%s&view_type=form&model=sale.order' % ids
        return order_link

    # ------------------------For delivery_address.xml page-------------------------
    def checkout_redirection(self, order):
        context = request.context

        # must have a draft sale order with lines at this point, otherwise reset
        if not order or order.state != 'draft':
            request.session['sale_order_id'] = None
            request.session['sale_transaction_id'] = None
            return request.redirect('/page/product_catalog')

        # if transaction pending / done: redirect to confirmation
        tx = context.get('website_sale_transaction')
        if tx and tx.state != 'draft':
            return request.redirect('/page/confirmation')

    def checkout_values(self, data=None):
        cr = request.cr
        uid = request.uid
        context = request.context
        registry = request.registry
        orm_partner = registry.get('res.partner')
        orm_user = registry.get('res.users')
        orm_country = registry.get('res.country')
        state_orm = registry.get('res.country.state')

        country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        partner = orm_user.browse(
            cr, SUPERUSER_ID, request.uid, context).partner_id

        order = None

        shipping_id = None
        shipping_ids = []
        checkout = {}
        if not data:
            if request.uid != request.website.user_id.id:
                checkout.update(self.checkout_parse("billing", partner))
                shipping_ids = orm_partner.search(
                    cr, SUPERUSER_ID,
                    [("parent_id", "=", partner.id),
                     ('type', "=", 'delivery')],
                    context=context)
            else:
                order = request.website.sale_get_order(
                    force_create=1, context=context)
                if order.partner_id:
                    domain = [("partner_id", "=", order.partner_id.id)]
                    user_ids = request.registry['res.users'].search(
                        cr, SUPERUSER_ID, domain,
                        context=dict(context or {}, active_test=False))
                    if not user_ids \
                            or request.website.user_id.id not in user_ids:
                        checkout.update(
                            self.checkout_parse("billing", order.partner_id))
        else:
            checkout = self.checkout_parse('billing', data)
            try:
                shipping_id = int(data["shipping_id"])
            except ValueError:
                pass
            if shipping_id == -1:
                checkout.update(self.checkout_parse('shipping', data))

        if shipping_id is None:
            if not order:
                order = request.website.sale_get_order(context=context)
            if order and order.partner_shipping_id:
                shipping_id = order.partner_shipping_id.id

        shipping_ids = list(set(shipping_ids) - set([partner.id]))

        if shipping_id == partner.id:
            shipping_id = 0
        elif shipping_id > 0 and shipping_id not in shipping_ids:
            shipping_ids.append(shipping_id)
        elif shipping_id is None and shipping_ids:
            shipping_id = shipping_ids[0]

        ctx = dict(context, show_address=1)
        shippings = []
        if shipping_ids:
            shippings = shipping_ids and orm_partner.browse(
                cr, SUPERUSER_ID, list(shipping_ids), ctx) or []
        if shipping_id > 0:
            shipping = orm_partner.browse(cr, SUPERUSER_ID, shipping_id, ctx)
            checkout.update(self.checkout_parse("shipping", shipping))

        checkout['shipping_id'] = shipping_id
        checkout['property_account_position'] = \
            partner.property_account_position.name

        # Default search by user country
        if not checkout.get('country_id'):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                country_ids = request.registry.get('res.country').search(
                    cr, uid, [('code', '=', country_code)], context=context)
                if country_ids:
                    checkout['country_id'] = country_ids[0]

        values = {
            'countries': countries,
            'states': states,
            'checkout': checkout,
            'shipping_id': partner.id != shipping_id and shipping_id or 0,
            'shippings': shippings,
            'error': {},
            'has_check_vat': hasattr(registry['res.partner'], 'check_vat')
        }

        return values

    # ------------------------For confirmation.xml page-------------------------

    @http.route(['/page/messages/check_unread_messages'],
                type='json', auth="user", website=True)
    def display_all_unread_messages(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        email_message = pool.get('mail.message')

        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(
            cr, uid, res_users_data, context=context)
        all_unread_email = len(email_message.search(
            cr, uid, [('author_id', '=', int(data.partner_id.id)),
                      ('to_read', '=', True)],
            context=context))
        return all_unread_email

    @http.route(['/page/messages/page/<int:page>', '/page/messages'],
                type='http', auth="user", website=True)
    def display_messages(self, page=1, search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        res_partner = pool.get('res.partner')
        email_message = pool.get('mail.message')

        keep = QueryURL('/page/messages', search=search)

        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(
            cr, uid, res_users_data, context=context)
        res_partner_data = res_partner.search(
            cr, uid, [('id', '=', data.partner_id.id)], context=context)
        widget_mail = res_partner.browse(
            cr, uid, res_partner_data, context=context)
        email_data = None
        if search == 'unread' or search == '':
            all_email_data_unread_ids = email_message.search(
                cr, uid, [('author_id', '=', int(data.partner_id.id)),
                          ('to_read', '=', True)],
                context=context)
            all_email_data_read_ids = email_message.search(
                cr, uid, [('author_id', '=', int(data.partner_id.id)),
                          ('to_read', '=', False)],
                context=context)
            all_email_data_unread_ids.extend(all_email_data_read_ids)
            all_read_unread_data = all_email_data_unread_ids
            email_data = email_message.browse(
                cr, uid,
                all_read_unread_data[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)], context=context)
        if search == 'read':
            all_email_data_read_ids = email_message.search(
                cr, uid, [('author_id', '=', int(data.partner_id.id)),
                          ('to_read', '=', False)],
                context=context)
            all_email_data_unread_ids = email_message.search(
                cr, uid, [('author_id', '=', int(data.partner_id.id)),
                          ('to_read', '=', True)],
                context=context)
            all_email_data_read_ids.extend(all_email_data_unread_ids)
            all_read_unread_data = all_email_data_read_ids
            email_data = email_message.browse(
                cr, uid,
                all_read_unread_data[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)],
                context=context)
        if search == 'bydate':
            all_email_data_by_date = email_message.search(
                cr, uid, [('author_id', '=', int(data.partner_id.id))],
                context=context)
            email_data = email_message.browse(
                cr, uid,
                all_email_data_by_date[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)],
                context=context)

        total = len(email_message.search(
            cr, uid, [('author_id', '=', int(data.partner_id.id))],
            context=context))
        if search:
            post["search"] = search
        pageUrl = "/page/messages"
        pager = request.website.pager(
            url=pageUrl,
            total=total,
            page=page,
            step=self._post_per_page,
            url_args=post
        )
        values = {
            'widget_mail': widget_mail,
            'email_data': email_data,
            'pager': pager,
            'keep': keep,
            'search': search,
        }
        return request.website.render("website.messages", values)

    @http.route(['/page/messages/read_message'],
                type='json', auth="user", website=True)
    def display_messages_save_confirm(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        email_message = pool.get('mail.message')

        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(
            cr, uid, res_users_data, context=context)
        email_id = post.get('mail_id')
        print "-------eamil id -------------", email_id
        email_data_ids = email_message.search(
            cr, uid, [('id', '=', email_id)], context=context)
        email_data = email_message.browse(
            cr, uid, email_data_ids, context=context)

        mail_notification = pool.get('mail.notification')
        mail_notification_ids = mail_notification.search(
            cr, uid, [('message_id', '=', email_data.id)], context=context)
        values = {
            'is_read': True,
        }
        mail_notification.write(
            cr, SUPERUSER_ID, int(mail_notification_ids[0]), values,
            context=context)
        values = {
            'to_read': False,
        }
        email_message.write(
            cr, SUPERUSER_ID, int(email_data_ids[0]), values, context=context)

        all_unread_email = len(email_message.search(
            cr, uid, [('author_id', '=', int(data.partner_id.id)),
                      ('to_read', '=', True)],
            context=context))

        return all_unread_email

    @http.route(['/page/messages/unread_message'],
                table_computetype='json', auth="user", website=True)
    def display_unread_messages_save_confirm(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        email_message = pool.get('mail.message')

        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(cr, uid, res_users_data, context=context)
        email_id = post.get('mail_id')
        email_data_ids = email_message.search(
            cr, uid, [('id', '=', email_id)], context=context)
        email_data = email_message.browse(
            cr, uid, email_data_ids, context=context)

        mail_notification = pool.get('mail.notification')
        mail_notification_ids = mail_notification.search(
            cr, uid, [('message_id', '=', email_data.id)], context=context)
        values = {
            'is_read': False,
        }
        mail_notification.write(
            cr, SUPERUSER_ID, int(mail_notification_ids[0]), values,
            context=context)
        values = {
            'to_read': True,
        }
        email_message.write(
            cr, SUPERUSER_ID, int(email_data_ids[0]), values, context=context)
        all_unread_email = len(email_message.search(
            cr, uid, [('author_id', '=', int(data.partner_id.id)),
                      ('to_read', '=', True)],
            context=context))

        return all_unread_email

    @http.route(['/page/personal_profile'],
                type='http', auth="user", website=True)
    def personal_profile(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        res_partner = pool.get('res.partner')
        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(cr, uid, res_users_data, context=context)
        res_partner_data = res_partner.search(
            cr, uid, [('id', '=', data.partner_id.id)], context=context)
        partner_data = res_partner.browse(
            cr, uid, res_partner_data, context=context)

        question_obj = pool['personal.profile.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        forum_categories = request.registry['forum.categories']
        obj_ids = forum_categories.search(
            cr, SUPERUSER_ID, [('label_for', '=', 'pa')], context=context)
        if obj_ids:
            forums_categories = forum_categories.browse(
                cr, SUPERUSER_ID, obj_ids, context=context)
        else:
            forums_categories = None

        flixo_promo_action_ids = pool.get('flixo.promo.action').search(
            cr, uid, [], context=context)
        have_promo = False
        if flixo_promo_action_ids:
            have_promo = True
        values = {
            'questions': questions,
            'partner_data': partner_data,
            'forums_categories': forums_categories,
            'have_promo': have_promo
        }
        return request.website.render("website.personal_profile", values)

    @http.route(['/page/save_profile'],
                type='http', auth="user", website=True)
    def save_profile(self, **form_data):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_partner = pool.get('res.partner')
        res_id = form_data.get('profile-id')
        img_data = form_data.get('hidn_fld')
        str = ";base64"
        if str in img_data:
            coded_image = img_data.split(";base64,")
            img_data = coded_image[1]
        values = {
            'name': form_data.get('profile-first-name'),
            'last_name': form_data.get('profile-surname'),
            'company_name': form_data.get('profile-company'),
            'email': form_data.get('profile-email'),
            'phone': form_data.get('profile-phone'),
            'mobile': form_data.get('profile-cell'),
            'function': form_data.get('profile-post'),
            'image': img_data
        }
        res_partner.write(cr, uid, int(res_id), values, context=context)
        return request.redirect("/page/personal_profile?data=saved")

    @http.route(['/page/contact_call'],
                type='http', auth="public", website=True)
    def contact_call_display(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_users = pool.get('res.users')
        res_partner = pool.get('res.partner')
        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(
            cr, uid, res_users_data, context=context)
        res_partner_data = res_partner.search(
            cr, uid, [('id', '=', data.partner_id.id)], context=context)
        partner_data = res_partner.browse(
            cr, uid, res_partner_data, context=context)
        values = {
            'partner_data': partner_data
        }
        return request.website.render("website.contact_call", values)

    @http.route(['/page/confirmation'],
                type='http', auth="user", website=True)
    def category_dispaly_confirmation(self, page=0, category=None,
                                      search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        cart = request.website.sale_get_order(context=context)
        orm_user = pool.get('res.users')
        res_partner = pool.get('res.partner')

        partner_id = None
        if request.uid != request.website.user_id.id:
            partner_id = orm_user.browse(
                cr, SUPERUSER_ID, uid, context=context).partner_id.id
            res_partner_data = res_partner.search(
                cr, uid, [('id', '=', partner_id)], context=context)
            browse_data = res_partner.browse(
                cr, uid, res_partner_data[0], context=context)
        status = None
        if not cart:
            status = False
        if cart:
            status = request.website.sale_get_order(force_create=1)

        domain = request.website.sale_product_domain()
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        # ---------------display default category's product----------------
        default_cat_id = None
        if not category:
            default_category = pool.get('product.public.category')
            if default_category:
                default_cat_id = default_category.search(
                    cr, uid, [('default', '=', 'True')], context=context)
            domain += [('public_categ_ids', 'child_of', default_cat_id)]
        keep = QueryURL('/page/product_catalog',
                        category=category and int(category),
                        search=search, attrib=attrib_list)
        if category:
            category = pool['product.public.category'].browse(
                cr, uid, int(category), context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(
            cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        #----------dispaly total quantity of cart items category wise------
        res = dict()

        user_id = uid
        total = 0
        if categs:
            status = request.website.sale_get_order(force_create=1)
            user_id = pool.get('res.partner').browse(
                cr, SUPERUSER_ID, uid, context=context)
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            public_categ_ids = \
                                line.product_id.product_tmpl_id.public_categ_ids
                            for prod_cat_id in public_categ_ids:
                                if prod_cat_id.id == c.id:
                                    key = prod_cat_id.id
                                    if res.has_key(key):
                                        temp = int(line.product_uom_qty)
                                        res[key] = res[key] + temp
                                    else:
                                        res[key] = int(line.product_uom_qty)

            category_total_amount = {}
            category_tax_dic = {}
            total_amount_dic = {}

            total = 0
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            public_categ_ids = \
                                line.product_id.product_tmpl_id.public_categ_ids
                            for prod_cat_id in public_categ_ids:
                                if prod_cat_id.id == c.id:
                                    total += line.product_uom_qty

                                    key_cat = c.id

                                    tax = (line.product_uom_qty *
                                           line.tax_id.amount * line.price_unit)
                                    amount = (line.product_uom_qty *
                                              line.price_unit)
                                    total_cat = tax + amount

                                    if category_total_amount.has_key(key_cat):
                                        category_total_amount[key_cat] += amount
                                        category_tax_dic[c.id] += tax
                                        total_amount_dic[c.id] += total_cat
                                    else:
                                        category_total_amount[key_cat] = amount
                                        category_tax_dic[c.id] = tax
                                        total_amount_dic[c.id] = total_cat

        question_obj = pool['confirmation.page.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        # fetch page content
        order_confirmation = pool.get('order.confirmation')

        order_confirmation_id = order_confirmation.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        order_confirmation_content = order_confirmation.browse(
            cr, SUPERUSER_ID, order_confirmation_id, context=context)

        values = {
            'questions': questions,
            'category': category,
            'categories': categs,
            'keep': keep,
            'status': status,
            'annual': user_id,
            'res': res,
            'total_qnty': total,
            'annual_volume': browse_data.annual_volume,
            'order_confirmation_content': order_confirmation_content,
        }
        return request.website.render("website.confirmation", values)

    @http.route(['/page/commercial_proposal/invoice_send'],
                type='json', auth='user', website=True)
    def commercial_proposal_page_mail_send(self, page=0, category=None,
                                           search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(
            cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(
            cr, uid, sale_order_id_search, context=context)
        status_id = status.id
        email_template_obj = pool['email.template']

        template_ids = email_template_obj.search(
            cr, uid, [('name', '=', 'Sales Order - Send by Email')],
            context=context)

        email = email_template_obj.browse(cr, uid, template_ids[0])
        email_template_obj.write(cr, uid, template_ids,
                                 {'email_from': email.email_from,
                                  'email_to': email.email_to,
                                  'subject': email.subject, })

        email_template_obj.send_mail(
            cr, uid, template_ids[0], status_id, True, context=context)
        return True

    @http.route(['/page/order_confirmation/invoice_send'],
                type='json', auth='user', website=True)
    def order_confirmation_page_mail_send(self, page=0, category=None,
                                          search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(
            cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(
            cr, uid, sale_order_id_search, context=context)
        status_id = status.id
        email_template_obj = pool['email.template']

        template_ids = email_template_obj.search(
            cr, uid, [('name', '=', 'Sales Order - Send by Email')],
            context=context)

        email = email_template_obj.browse(cr, uid, template_ids[0])
        email_template_obj.write(cr, uid, template_ids,
                                 {'email_from': email.email_from,
                                  'email_to': email.email_to,
                                  'subject': email.subject, })

        email_template_obj.send_mail(
            cr, uid, template_ids[0], status_id, True, context=context)
        return True

    #--------------------------commercial page--------------------------------
    @http.route(['/page/commercial_proposal'
                 ], type='http', auth="user", website=True)
    def commercial_proposal_page(self, page=0, category=None,
                                 search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        cart = request.website.sale_get_order(context=context)
        if not cart:
            status = False
        if cart:
            status = request.website.sale_get_order(force_create=1)
        domain = request.website.sale_product_domain()
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        # ----------------display default category's product------------------
        if not category:
            default_category = pool.get('product.public.category')
            if default_category:
                default_cat_id = default_category.search(
                    cr, uid, [('default', '=', 'True')], context=context)
            domain += [('public_categ_ids', 'child_of', default_cat_id)]
        keep = QueryURL('/page/product_catalog',
                        category=category and int(category),
                        search=search, attrib=attrib_list)
        if category:
            category = pool['product.public.category'].browse(
                cr, uid, int(category), context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(
            cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        # ----------dispaly total quantity of cart items category wise------
        res = dict()
        if categs:
            status = request.website.sale_get_order(force_create=1)
            user_id = pool.get('res.partner').browse(
                cr, SUPERUSER_ID, uid, context=context)
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            public_categ_ids = \
                                line.product_id.product_tmpl_id.public_categ_ids
                            for prod_cat_id in public_categ_ids:
                                if prod_cat_id.id == c.id:
                                    key = prod_cat_id.id
                                    if res.has_key(key):
                                        temp = int(line.product_uom_qty)
                                        res[key] = res[key] + temp
                                    else:
                                        res[key] = int(line.product_uom_qty)

            category_total_amount = {}

            category_tax_dic = {}
            total_amount_dic = {}

            total = 0
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            public_categ_ids = \
                                line.product_id.product_tmpl_id.public_categ_ids
                            for prod_cat_id in public_categ_ids:
                                if prod_cat_id.id == c.id:

                                    key_cat = c.id

                                    tax = (line.product_uom_qty *
                                           line.tax_id.amount *
                                           line.price_unit)
                                    amount = line.price_subtotal
                                    total_cat = tax + amount

                                    if category_total_amount.has_key(key_cat):
                                        category_total_amount[key_cat] += amount
                                        category_tax_dic[c.id] += tax
                                        total_amount_dic[c.id] += total_cat
                                    else:
                                        category_total_amount[key_cat] = amount
                                        category_tax_dic[c.id] = tax
                                        total_amount_dic[c.id] = total_cat

        res_user = pool.get('res.users')
        res_user_ids = res_user.search(cr, uid, [('id', '=', uid)],
                                       context=context)
        res_user_odj = res_user.browse(cr, uid, res_user_ids, context=context)

        question_obj = pool['commercial.proposal.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        # fetch the content data
        commercial_proposal = pool.get('commercial.proposal')

        commercial_proposal_id = commercial_proposal.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        commercial_proposal_content = commercial_proposal.browse(
            cr, SUPERUSER_ID, commercial_proposal_id, context=context)

        values = {
            'questions': questions,
            'user': res_user_odj,
            'category': category,
            'categories': categs,
            'keep': keep,
            'status': status,
            'annual': user_id,
            'res': res,
            'total_qnty': total,
            'categoty_total': category_total_amount,
            'category_tax_dic': category_tax_dic,
            'total_amount_dic': total_amount_dic,
            'commercial_proposal_content': commercial_proposal_content,
        }
        return request.website.render("website.commercial_proposal", values)

    @http.route('/page/commercial_proosal/save_attchment', methods=['POST'],
                type='http', auth="public", website=True)
    def commercial_proposal_page_save_attchment(self, page=0, category=None,
                                                search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        cart = request.website.sale_get_order(context=context)
        if not cart:
            status = False
        if cart:
            status = request.website.sale_get_order(force_create=1)
        category_obj = pool['product.public.category']
        category_ids = category_obj.search(
            cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)
        res = dict()
        if categs:
            status = request.website.sale_get_order(force_create=1)

        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(
            cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(
            cr, uid, sale_order_id_search, context=context)

        mail_message = pool.get('mail.message')
        res_users = pool.get('res.users')
        res_partner = pool.get('res.partner')
        sale_order = pool.get('sale.order')
        attachment_obj = pool.get('ir.attachment')
        mail_notification = pool.get('mail.notification')

        res_users_data = res_users.search(
            cr, uid, [('id', '=', uid)], context=context)
        data = res_users.browse(
            cr, uid, res_users_data, context=context)
        res_partner_data = res_partner.search(
            cr, uid, [('id', '=', data.partner_id.id)], context=context)

        if post:
            if post.get('uploaded_file'):
                mail_massage_values = {
                    'body': post.get('your_comment'),
                    'model': 'sale.order',
                    'record_name': status.name,
                    'type': 'comment',
                    'res_id': status.id,
                    'to_read': True,
                    'attachment_ids': [
                        (0, 0, {
                            'name': post['uploaded_file'].filename,
                            'res_model': 'sale.order',
                            'res_id': status.id,
                            'datas': base64.encodestring(
                                post['uploaded_file'].read()),
                            'datas_fname': post['uploaded_file'].filename,
                        })],
                }
                created_mail_message_id = mail_message.create(
                    cr, uid, mail_massage_values, context=context)
                created_mail_message_obj = mail_message.browse(
                    cr, uid, created_mail_message_id, context=context)
                mail_notification_data = {
                    'message_id': int(created_mail_message_obj.id),
                    'is_read': False,
                    'partner_id': int(data.partner_id.id),
                    'starred': False
                }
                mail_notification.create(
                    cr, SUPERUSER_ID, mail_notification_data, context=context)
                status_id = status.id
                sale_order.write(
                    cr, uid, status_id,
                    {'message_ids': created_mail_message_obj},
                    context=context)
            else:
                mail_massage_values = {
                    'body': post.get('your_comment'),
                    'model': 'sale.order',
                    'record_name': status.name,
                    'type': 'comment',
                    'res_id': status.id,
                    'to_read': True
                }
                created_mail_message_id = mail_message.create(
                    cr, uid, mail_massage_values, context=context)
                created_mail_message_obj = mail_message.browse(
                    cr, uid, created_mail_message_id, context=context)
                mail_notification_data = {
                    'message_id': int(created_mail_message_obj.id),
                    'is_read': False,
                    'partner_id': int(data.partner_id.id),
                    'starred': False
                }
                save_data = mail_notification.create(
                    cr, SUPERUSER_ID, mail_notification_data, context=context)
                status_id = status.id
                sale_order.write(cr, uid, status_id,
                                 {'message_ids': created_mail_message_obj},
                                 context=context)

        url_commercial_page = "/page/commercial_proposal/%s" % slug(status)
        return request.redirect(url_commercial_page)


    #---------------------------Billing information page--------------------------------------
    @http.route(['/page/billing_information'
                 ], type='http', auth="user", website=True)
    def billing_information_page(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        cart = request.website.sale_get_order(context=context)
        if not cart:
            status=False
        if cart:
            status=request.website.sale_get_order(force_create=1)

        domain = request.website.sale_product_domain()
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])
         #--------------------display default category's product---------------------
        if not category:
            default_category= pool.get('product.public.category')
            if default_category:
                default_cat_id=default_category.search(cr, uid,[('default','=','True')],context=context)
            domain += [('public_categ_ids','child_of',default_cat_id)]
        keep = QueryURL('/page/product_catalog', category=category and int(category), search=search, attrib=attrib_list)
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/page/product_catalog/category/%s" % slug(category)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        #----------dispaly total quantity of cart items category wise------
        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(cr, uid, sale_order_id_search, context=context)
        res = dict()
        if categs:
            #status=request.website.sale_get_order(force_create=1)
            user_id = pool.get('res.partner').browse(cr, SUPERUSER_ID, uid, context=context)
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            for prod_cat_id in line.product_id.product_tmpl_id.public_categ_ids:
                                if prod_cat_id.id == c.id:
                                    key=prod_cat_id.id
                                    if res.has_key(key):
                                        temp=int(line.product_uom_qty)
                                        res[key]=res[key]+temp
                                    else:
                                       res[key]=int(line.product_uom_qty)
        total=0
        category_total_amount = {}

        category_tax_dic = {}
        total_amount_dic ={}
        for c in categs:
            if c.main_category:
                for s in status:
                    for line in s.order_line:
                        for prod_cat_id in line.product_id.product_tmpl_id.public_categ_ids:
                            if prod_cat_id.id == c.id:

                                key_cat = c.id

                                tax = (line.product_uom_qty) * (line.tax_id.amount) * (line.price_unit)
                                amount = line.price_subtotal
                                total_cat = tax + amount

                                if category_total_amount.has_key(key_cat):
                                    category_total_amount[key_cat] += amount
                                    category_tax_dic[c.id]+= tax
                                    total_amount_dic[c.id] += total_cat
                                else:
                                    category_total_amount[key_cat] = amount
                                    category_tax_dic[c.id]= tax
                                    total_amount_dic[c.id] = total_cat

        res_user = pool.get('res.users')
        res_user_ids=res_user.search(cr, uid,[('id','=',uid)],context=context)
        res_user_odj = res_user.browse(cr, uid, res_user_ids, context=context)

        values = {
            'user':res_user_odj,
            'category': category,
            'categories': categs,
            'keep': keep,
            'status':status,
            'annual':user_id,
            'res':res,
            'state':status.state,
            'order_no':status.name,
            'total_qnty':total,
            'categoty_total': category_total_amount,
            'category_tax_dic':category_tax_dic,
            'total_amount_dic' :total_amount_dic,
        }
        if status.state=='draft':
            question_obj = pool['billing.information.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)
            values.update({'questions':questions})
            return request.website.render("website.billing_information",values)
        elif status.state=='manual':
            question_obj = pool['order.confirmation.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)
            values.update({'questions':questions})
            return request.website.render("website.order_confirmation",values)

    @http.route(['/page/billing_savedata/<model("sale.order"):sale>'
                 ], type='http', auth="user", website=True)
    def billing_information_save_data_page(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        cart = request.website.sale_get_order(context=context)
        if not cart:
            status=False
        if cart:
            status=request.website.sale_get_order(force_create=1)

        domain = request.website.sale_product_domain()
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])
         #--------------------display default category's product---------------------
        if not category:
            default_category= pool.get('product.public.category')
            if default_category:
                default_cat_id=default_category.search(cr, uid,[('default','=','True')],context=context)
            domain += [('public_categ_ids','child_of',default_cat_id)]
        keep = QueryURL('/page/product_catalog', category=category and int(category), search=search, attrib=attrib_list)
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/page/product_catalog/category/%s" % slug(category)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        #----------dispaly total quantity of cart items category wise------
        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(cr, uid, sale_order_id_search, context=context)
        status = post.get('sale')
        print "--------------status--------------",status
        res = dict()
        if categs:
            #status=request.website.sale_get_order(force_create=1)
            user_id = pool.get('res.partner').browse(cr, SUPERUSER_ID, uid, context=context)
            for c in categs:
                if c.main_category:
                    for s in status:
                        for line in s.order_line:
                            for prod_cat_id in line.product_id.product_tmpl_id.public_categ_ids:
                                if prod_cat_id.id == c.id:
                                    key=prod_cat_id.id
                                    if res.has_key(key):
                                        temp=int(line.product_uom_qty)
                                        res[key]=res[key]+temp
                                    else:
                                       res[key]=int(line.product_uom_qty)
        total=0
        category_total_amount = {}

        category_tax_dic = {}
        total_amount_dic ={}
        for c in categs:
            if c.main_category:
                for s in status:
                    for line in s.order_line:
                        for prod_cat_id in line.product_id.product_tmpl_id.public_categ_ids:
                            if prod_cat_id.id == c.id:

                                key_cat = c.id

                                tax = (line.product_uom_qty) * (line.tax_id.amount) * (line.price_unit)
                                amount = (line.product_uom_qty) * (line.price_unit)
                                total_cat = tax + amount

                                if category_total_amount.has_key(key_cat):
                                    category_total_amount[key_cat] += amount
                                    category_tax_dic[c.id]+= tax
                                    total_amount_dic[c.id] += total_cat
                                else:
                                    category_total_amount[key_cat] = amount
                                    category_tax_dic[c.id]= tax
                                    total_amount_dic[c.id] = total_cat

        res_user = pool.get('res.users')
        res_user_ids=res_user.search(cr, uid,[('id','=',uid)],context=context)
        res_user_odj = res_user.browse(cr, uid, res_user_ids, context=context)

        values = {
            'user':res_user_odj,
            'category': category,
            'categories': categs,
            'keep': keep,
            'status':status,
            'annual':user_id,
            'res':res,
            'state':status.state,
            'order_no':status.name,
            'total_qnty':total,
            'categoty_total': category_total_amount,
            'category_tax_dic':category_tax_dic,
            'total_amount_dic' :total_amount_dic,
        }
        if status.state=='draft':
            question_obj = pool['billing.information.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)
            values.update({'questions':questions})
            return request.website.render("website.billing_information",values)
        elif status.state=='manual':
            question_obj = pool['order.confirmation.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)
            values.update({'questions':questions})
            return request.website.render("website.order_confirmation",values)

    @http.route('/page/billing_information/save_attchment', methods=['POST'], type='http', auth="public", website=True)
    def billing_information_save_attchment(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        cart = request.website.sale_get_order(context=context)
        if not cart:
            status=False
        if cart:
            status=request.website.sale_get_order(force_create=1)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        #----------dispaly total quantity of cart items category wise------
        res = dict()
        if categs:
            status=request.website.sale_get_order(force_create=1)

        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(cr, uid, sale_order_id_search, context=context)

        mail_message= pool.get('mail.message')
        sale_order= pool.get('sale.order')
        attachment_obj= pool.get('ir.attachment')
        res_user = pool.get('res.users')
        if post:
            if post.get('uploaded_file'):
                mail_massage_values = {
                    'body' : post.get('your_comment'),
                    'model' : 'sale.order',
                    'record_name' :status.name,
                    'type' :'comment',
                    'res_id':status.id,
                    'attachment_ids':[(0, 0, {
                                        'name': post['uploaded_file'].filename,
                                        'res_model' : 'sale.order',
                                        'res_id': status.id,
                                        'datas': base64.encodestring(post['uploaded_file'].read()),
                                        'datas_fname': post['uploaded_file'].filename,
                                        })],
                 }
                created_mail_message_id = mail_message.create(cr,uid,mail_massage_values,context=context)
                created_mail_message_obj = mail_message.browse(cr, uid, created_mail_message_id, context=context)
                status_id = status.id
                write_id = sale_order.write(cr,uid,status_id,{'message_ids':created_mail_message_obj},context=context)
            else :
                mail_massage_values = {
                    'body' : post.get('your_comment'),
                    'model' : 'sale.order',
                    'record_name' :status.name,
                    'type' :'comment',
                    'res_id':status.id,
                 }
                created_mail_message_id = mail_message.create(cr,uid,mail_massage_values,context=context)
                created_mail_message_obj = mail_message.browse(cr, uid, created_mail_message_id, context=context)
                status_id = status.id
                write_id = sale_order.write(cr,uid,status_id,{'message_ids':created_mail_message_obj},context=context)
        url_billing_information_page = "/page/billing_savedata/%s" % slug(status)
        return request.redirect(url_billing_information_page)

    #---------------------------Billing information page--------------------------------------
    @http.route(['/page/billing_information/send_mail'
                 ], type='json', auth="user", website=True)
    def billing_information_send_quotation_page(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        status = post.get('sale_order')
        sale_order_obj = pool.get('sale.order')
        sale_order_id_search = sale_order_obj.search(cr, uid, [('name', '=', status)], context=context)
        status = sale_order_obj.browse(cr, uid, sale_order_id_search, context=context)
        status_id = status.id
        email_template_obj = pool['email.template']

        template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Sales Order - Send by Email')], context=context)

        email = email_template_obj.browse(cr, uid, template_ids[0])
        email_template_obj.write(cr, uid, template_ids, {'email_from': email.email_from,
                                                'email_to': email.email_to,
                                                'subject': email.subject,
                                                })

        email_template_obj.send_mail(cr, uid, template_ids[0], status_id, True, context=context)

        return True

    @http.route(['/page/contact_us/save_message'], methods=['POST'],
                type='http', auth="public", website=True)
    def save_message(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        crm_lead = pool.get('crm.lead')
        fullname = post.get('first_name') + " " + post.get('last_name')
        file_data = post.get('file')
        if file_data:
            file_name = file_data.filename
            vals = {
                'name': fullname,
                'contact_name': fullname,
                'partner_name': fullname,
                'email_from': post.get('email'),
                'description': post.get('messege'),
                'user_id': uid,
                'attachment_id': [(0, 0, {
                    'model': 'crm.lead',
                    'res_name': file_name,
                    'datas': file_data.read().encode('base64'),
                    'name': file_name,
                    'datas_fname': file_name,
                })],
            }
            crm_lead.create(cr, SUPERUSER_ID, vals, context=context)
        else:
            vals = {
                'name': fullname,
                'partner_name': fullname,
                'contact_name': fullname,
                'email_from': post.get('email'),
                'user_id': uid,
                'description': post.get('messege'),
            }
            crm_lead.create(cr, SUPERUSER_ID, vals, context=context)
        return request.website.render("website.contact_us")


    #-------------contact Question save page-----------------
    @http.route(['/page/contact_question/save_data'
                 ], type='json', auth="public", website=True)
    def contact_question_save_data_page(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        crm_lead = pool.get('crm.lead')
        fullname = post.get('first_name') + " " + post.get('last_name')
        img_data = post.get('file_name')
        print "---img data-----",img_data
        str=";base64"
        if img_data:
            if str in img_data:
                coded_image = img_data.split(";base64,")
                img_data = coded_image[1]

            vals = {
                'contact_name': fullname,
                'partner_name': post.get('company'),
                'phone': post.get('phone'),
                'email_from': post.get('email'),
                'name': post.get('subject'),
                'description': post.get('messege'),
                'user_id': uid,
                'attachment_id': [(0, 0, {
                    'model': 'crm.lead',
                    'res_name': post.get('subject'),
                    'datas': img_data,
                    'name': post.get('file_name_name'),
                    'datas_fname': post.get('file_name_name'),

                })],
            }
            crm_lead.create(cr,SUPERUSER_ID,vals,context=context)
        else:
            vals = {
                'contact_name': fullname,
                'partner_name': post.get('company'),
                'phone': post.get('phone'),
                'email_from': post.get('email'),
                'name': post.get('subject'),
                'user_id': uid,
                'description': post.get('messege'),

            }
            crm_lead.create(cr,SUPERUSER_ID,vals,context=context)
        return True


    #------------------------For company_info_delivary_address.xml page-------------------------

    def checkout_values_company(self, data=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        orm_partner = registry.get('res.partner')
        orm_user = registry.get('res.users')
        orm_country = registry.get('res.country')
        state_orm = registry.get('res.country.state')

        country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        partner = orm_user.browse(cr, SUPERUSER_ID, request.uid, context).partner_id

        order = None

        shipping_id = None
        shipping_ids = []
        checkout = {}
        if not data:
            if request.uid != request.website.user_id.id:
                checkout.update( self.checkout_parse("billing", partner) )
                shipping_ids = orm_partner.search(cr, SUPERUSER_ID, [("parent_id", "=", partner.id), ('type', "=", 'delivery')], context=context)
            else:
                order = request.website.sale_get_order(force_create=1, context=context)
                if order.partner_id:
                    domain = [("partner_id", "=", order.partner_id.id)]
                    user_ids = request.registry['res.users'].search(cr, SUPERUSER_ID, domain, context=dict(context or {}, active_test=False))
                    if not user_ids or request.website.user_id.id not in user_ids:
                        checkout.update( self.checkout_parse("billing", order.partner_id) )
        else:
            checkout = self.checkout_parse('billing', data)
            try:
                shipping_id = int(data["shipping_id"])
            except ValueError:
                pass
            if shipping_id == -1:
                checkout.update(self.checkout_parse('shipping', data))

        if shipping_id is None:
            if not order:
                order = request.website.sale_get_order(context=context)
            if order and order.partner_shipping_id:
                shipping_id = order.partner_shipping_id.id

        shipping_ids = list(set(shipping_ids) - set([partner.id]))

        if shipping_id == partner.id:
            shipping_id = 0
        elif shipping_id > 0 and shipping_id not in shipping_ids:
            shipping_ids.append(shipping_id)
        elif shipping_id is None and shipping_ids:
            shipping_id = shipping_ids[0]

        ctx = dict(context, show_address=1)
        shippings = []
        if shipping_ids:
            shippings_unorder = shipping_ids and orm_partner.browse(cr, SUPERUSER_ID, list(shipping_ids), ctx) or []
            _logger.info('shippings_unorder = %s', shippings_unorder)
            if shippings_unorder:
                try:
                    shippings = sorted(
                        shippings_unorder, key=lambda ship: ship.city.lower())
                except:
                    pass

        if shipping_id > 0:
            shipping = orm_partner.browse(cr, SUPERUSER_ID, shipping_id, ctx)
            checkout.update( self.checkout_parse("shipping", shipping) )

        checkout['shipping_id'] = shipping_id

        # Default search by user country
        if not checkout.get('country_id'):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                country_ids = request.registry.get('res.country').search(cr, uid, [('code', '=', country_code)], context=context)
                if country_ids:
                    checkout['country_id'] = country_ids[0]
        values = {
            'countries': countries,
            'states': states,
            'checkout': checkout,
            'shipping_id': partner.id != shipping_id and shipping_id or 0,
            'shippings': shippings,
            'error': {},
            'has_check_vat': hasattr(registry['res.partner'], 'check_vat'),
            'property_account_position': partner.property_account_position.name
        }

        return values

    mandatory_billing_fields = ["name", "phone", "email", "street2", "city", "country_id","last_name","company_name"]
    optional_billing_fields = ["street", "state_id", "vat", "vat_subjected", "zip","id","create_date"]
    mandatory_shipping_fields = ["name", "phone", "street", "city", "country_id","last_name","company_name"]
    optional_shipping_fields = ["state_id", "zip","create_date"]

    def checkout_parse(self, address_type, data, remove_prefix=False):
        """ data is a dict OR a partner browse record
        """
        # set mandatory and optional fields
        assert address_type in ('billing', 'shipping')
        if address_type == 'billing':
            all_fields = self.mandatory_billing_fields + self.optional_billing_fields
            prefix = ''
        else:
            all_fields = self.mandatory_shipping_fields + self.optional_shipping_fields
            prefix = 'shipping_'

        # set data
        if isinstance(data, dict):
            query = dict((prefix + field_name, data[prefix + field_name])
                for field_name in all_fields if data.get(prefix + field_name))
        else:
            query = dict((prefix + field_name, getattr(data, field_name))
                for field_name in all_fields if getattr(data, field_name))
            if address_type == 'billing' and data.parent_id:
                query[prefix + 'street'] = data.parent_id.name

        if query.get(prefix + 'state_id'):
            query[prefix + 'state_id'] = int(query[prefix + 'state_id'])

        if query.get(prefix + 'last_name'):
            query[prefix + 'last_name'] = (query[prefix + 'last_name'])

        if query.get(prefix + 'country_id'):
            query[prefix + 'country_id'] = int(query[prefix + 'country_id'])

        if query.get(prefix + 'vat'):
            query[prefix + 'vat_subjected'] = True

        if not remove_prefix:
            return query

        return dict((field_name, data[prefix + field_name]) for field_name in all_fields if data.get(prefix + field_name))

    @http.route(['/page/company_info_delivary_address'], type='http', auth="user", website=True)
    def company_dispaly_delivery_address(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        res_partner= pool.get('res.partner')
        if len(post) > 0 :
            shipping_partner_id = post.get('shipping_partner_id')
            create_new_part_add = post.get('new_address')
            edit_old_part_add = post.get('link_partner')
            if len(create_new_part_add) > 1 :
                partner_parent_id = post.get('partner_id')
                partner_parent_id = int(partner_parent_id)
                partner_parent_name = post.get('partner_name')
                values = {
                 'name' : partner_parent_name,
                 'type' : 'delivery',
                 'parent_id' : partner_parent_id,
                 'street' : post.get('shipping_street'),
                 'country_id' : post.get('country_id'),
                 'city' :post.get('shipping_city'),
                 'zip' :post.get('shipping_zip'),
                            }
                save_data = res_partner.create(cr,uid,values,context=context)
                return request.redirect("/page/company_info_delivary_address")
            elif len(edit_old_part_add) > 1:
                partner_id = post.get('shipping_partner_id')
                partner_id = int(partner_id)
                values = {
                     'street' : post.get('shipping_street'),
                     'country_id' : post.get('country_id'),
                     'city' :post.get('shipping_city'),
                     'zip' :post.get('shipping_zip'),
                }
                save_data = res_partner.write(cr,uid,partner_id,values,context=context)
                return request.redirect("/page/company_info_delivary_address")
            else:
                partner_id = post.get('partner_id')
                partner_id = int(partner_id)
                values = {
                     'street' : post.get('shipping_street'),
                     'country_id' : post.get('country_id'),
                     'city' :post.get('shipping_city'),
                     'zip' :post.get('shipping_zip'),
                }
                save_data = res_partner.write(cr,uid,partner_id,values,context=context)
                return request.redirect("/page/company_info_delivary_address")
        values = {}
        values.update(self.checkout_values_company())
        return request.website.render("website.company_info_delivary_address", values)


        #--------------------------for company's registered office-------------------------

    @http.route(['/page/company_registered_office'], type='http', auth="user", website=True)
    def company_dispaly_registered_office_address(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        res_partner= pool.get('res.partner')
        if len(post) > 0 :
            shipping_partner_id = post.get('shipping_partner_id')
            create_new_part_add = post.get('new_address')
            edit_old_part_add = post.get('link_partner')
            if len(create_new_part_add) > 1 :
                partner_parent_id = post.get('partner_id')
                partner_parent_id = int(partner_parent_id)
                partner_parent_name = post.get('partner_name')
                values = {
                 'name' : partner_parent_name,
                 'type' : 'delivery',
                 'parent_id' : partner_parent_id,
                 'street' : post.get('shipping_street'),
                 'country_id' : post.get('country_id'),
                 'city' :post.get('shipping_city'),
                 'zip' :post.get('shipping_zip'),
                 'company_name' :post.get('c_name'),
                            }
                save_data = res_partner.create(cr,uid,values,context=context)
                return request.redirect("/page/company_registered_office")
            elif len(edit_old_part_add) > 1:
                partner_id = post.get('shipping_partner_id')
                partner_id = int(partner_id)
                values = {
                     'street' : post.get('shipping_street'),
                     'country_id' : post.get('country_id'),
                     'city' :post.get('shipping_city'),
                     'zip' :post.get('shipping_zip'),
                     'company_name' :post.get('c_name'),
                }
                save_data = res_partner.write(cr,uid,partner_id,values,context=context)
                return request.redirect("/page/company_registered_office")
            else:
                partner_id = post.get('partner_id')
                partner_id = int(partner_id)
                values = {
                     'street' : post.get('shipping_street'),
                     'country_id' : post.get('country_id'),
                     'city' :post.get('shipping_city'),
                     'zip' :post.get('shipping_zip'),
                     'company_name' :post.get('c_name'),
                }
                save_data = res_partner.write(cr,uid,partner_id,values,context=context)
                return request.redirect("/page/company_registered_office")
        values = {}
        values.update(self.checkout_values_company())
        return request.website.render("website.company_registered_office", values)



        #------------------------For employee_info_page.xml page-------------------------

    @http.route(['/page/employee_info_page'], type='http', auth="user", website=True)
    def employee_detail(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        hr_employee= pool.get('hr.employee')
        # ir_rule = pool.get('ir.rule')
        #
        # ir_rule_data = ir_rule.search(cr,uid,[('name', '=', 'Job multi company rule')],context=context);
        # multi_company_rule = ir_rule.browse(cr,uid,ir_rule_data,context=context);
        # ir_rule_overwrite = ir_rule.write(cr,uid,multi_company_rule.id,{'domain_force':''},context=context);
        #
        # print"------------------------multi_company_rule---------------------------",ir_rule_overwrite
        login_user_data = hr_employee.search(cr,uid,[('user_id', '=', uid)],context=context);
        login_user = hr_employee.browse(cr,uid,login_user_data,context=context);

        company_user_data = hr_employee.search(cr,uid,[('company_id', '=', login_user.company_id.id)],context=context);
        employee_obj_list =[]
        for employee in company_user_data:
            company_user = hr_employee.browse(cr,uid,employee,context=context);
            employee_obj_list.append(company_user)
        values ={
            'employee_details' : employee_obj_list
        }

        return request.website.render("website.employee_info_page", values)

    #------------------------For bank_info_page.xml page-------------------------
    @http.route(['/page/banking_detail'], type='http', auth="user", website=True)
    def banking_detail(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        values={}

        #---fetch login employee detail-------------to update company detail
        hr_employee= pool.get('hr.employee')
        login_user_data = hr_employee.search(cr,uid,[('user_id', '=', uid)],context=context)
        
        if login_user_data:
            login_user = hr_employee.browse(cr,uid,login_user_data,context=context)

            if login_user.bank_account_id:
                bank_account_id = login_user.bank_account_id

                values.update({
                     'acc_number':bank_account_id.acc_number,
                     'bank_name':bank_account_id.bank_name,
                     'bank_bic':bank_account_id.bank_bic,
                     'vat_number':bank_account_id.vat_number,
                })

            if login_user.company_id:
                company_id = login_user.company_id

                if company_id.partner_id.mobile:
                    values.update({
                     'mobile':company_id.partner_id.mobile})

                if company_id.partner_id.company_name:
                    values.update({
                     'com_name':company_id.partner_id.company_name})

                if company_id.partner_id.industry:
                    values.update({
                     'industry':company_id.partner_id.industry})

                if company_id.name:
                    values.update({
                     'name':company_id.name})

                if company_id.website:
                    values.update({
                     'website_name':company_id.website})

                if company_id.email:
                    values.update({
                     'email':company_id.email})

                if company_id.phone:
                    values.update({
                     'phone':company_id.phone})

                if company_id.fax:
                    values.update({
                     'fax':company_id.fax})

        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        values['partner'] = res_users_object.partner_id

        return request.website.render("website.banking_detail",values)

    #------------------------For saving bank_info_page.xml page-------------------------
    @http.route(['/page/save_banking_info_page'], type='http', auth="user", website=True)
    def save_banking_detail(self, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        com_name = form_data.get('company_legal_name')
        vat_number = form_data.get('vat_number')
        bank_name = form_data.get('bank_name')
        e_bank = form_data.get('e-bank')
        account_number = form_data.get('account_number')
        website = form_data.get('website')
        phone = form_data.get('phone')
        mobile = form_data.get('mobile')
        fax = form_data.get('fax')
        company_name = form_data.get('company_name')
        industry = form_data.get('industry')
        email = form_data.get('email')

        #---fetch login employee detail-------------to update company detail
        hr_employee= pool.get('hr.employee')
        login_user_data = hr_employee.search(cr,uid,[('user_id', '=', uid)],context=context)
        login_user = hr_employee.browse(cr,uid,login_user_data,context=context)

        # create bank account
        values_for_bank = {
            'state': 'iban',
            'acc_number':e_bank,
            'bank_name':bank_name,
            'bank_bic':account_number,
            'vat_number':vat_number
        }
        updated_id = False;
        res_partner_bank = pool.get('res.partner.bank')
        if login_user.bank_account_id:
            try:
                updated_id = res_partner_bank.write(cr,uid,login_user.bank_account_id.id,values_for_bank,context=context)
                write_bank_detail = hr_employee.write(cr,uid,login_user.id,{'bank_account_id':login_user.bank_account_id.id},context=context)
            except:
                return request.redirect('/page/banking_detail')
        else:
            try:
                created_id = res_partner_bank.create(cr,uid,values_for_bank)
                write_bank_detail = hr_employee.write(cr,uid,login_user.id,{'bank_account_id':created_id},context=context)
            except:
                return request.redirect('/page/banking_detail')


        #create res partner company--------------------
        values_res_partner_company ={
            'is_company':True,
            'name':industry,
            'mobile':mobile,
            'company_name':company_name,
            'industry':industry
        }

        res_partner_company = pool.get('res.partner')
        if login_user.company_id:
            if login_user.company_id.partner_id:
                partner_company = login_user.company_id.partner_id.id
                updated_res_par_company_id = res_partner_company.write(cr,uid,partner_company,values_res_partner_company,context=context)
            else:
                created_res_par_company_id = res_partner_company.create(cr,uid,values_res_partner_company)

        #Update res company detail----------------------------------------
        company_id = login_user.company_id.id
        values2 ={
            'name':com_name,
            'website':website,
            'email':email,
            'phone':phone,
            'fax':fax,
        }

        if updated_res_par_company_id:
            values2.update({'partner_id':partner_company})
        else:
            values2.update({'partner_id':created_res_par_company_id})
        res_company = pool.get('res.company')
        company_data = res_company.write(cr,uid,company_id,values2,context=context)

        #update employee detail
        if updated_id:
            write_bank_detail = hr_employee.write(cr,uid,login_user.id,{'bank_account_id':login_user.bank_account_id.id},context=context)
        else:
            write_bank_detail = hr_employee.write(cr,uid,login_user.id,{'bank_account_id':created_id},context=context)

        return request.redirect('/page/banking_detail')

    @http.route(['/page/print'], type='http', auth="public", website=True)
    def print_saleorder(self,**form_data):
        cr, uid, context = request.cr, SUPERUSER_ID, request.context

        status=request.website.sale_get_order(force_create=1)
        sale_order_id = status.id
        if sale_order_id:
            pdf = request.registry['report'].get_pdf(cr, uid, [sale_order_id], 'profile_management.report_saleorder_inherit', data=None, context=context)
            pdfhttpheaders = [('Content-Type', 'application/pdf'), ('Content-Length', len(pdf))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/page/commercial_proposal')

    @http.route(['/page/settings'], type='http', auth="user", website=True)
    def settings_page(self,**post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        res_users= pool.get('res.users')
        res_partner= pool.get('res.partner')
        res_users_data=res_users.search(cr, uid,[('id','=',uid)],context=context)
        data = res_users.browse(cr,uid,res_users_data,context=context)
        res_partner_data=res_partner.search(cr, uid,[('id','=',data.partner_id.id)],context=context)
        partner_data = res_partner.browse(cr,uid,res_partner_data,context=context)
        if not partner_data.company_name:
            partner_data.write({'company_name':'Unknown company'})
        question_obj = pool['settings.page.questions.faq']
        question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
        questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)

        user_setting_obj=pool.get('user.setting')
        setting_data_ids=user_setting_obj.search(cr,SUPERUSER_ID,[('res_id','=',uid)],context=context)
        user_setting_data=user_setting_obj.browse(cr, SUPERUSER_ID, setting_data_ids,context=context)

        forum_categories = request.registry['forum.categories']
        obj_ids = forum_categories.search(cr, SUPERUSER_ID, [('label_for','=','pa')], context=context)
        if obj_ids:
            forums_categories=forum_categories.browse(cr, SUPERUSER_ID, obj_ids, context=context)
        else:
            forums_categories =None
        values ={
            'questions':questions,
            'partner_data':partner_data,
            'forums_categories':forums_categories,
            'user_setting_data' : user_setting_data[0] if user_setting_data else False,
        }
        return request.website.render("website.settings", values)

    @http.route('/page/settings/validate', type='http', auth="public", website=True)
    def setting_change_password(self,  **fields):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        main_list1 = []
        main_dict1 = {}
        main_dict2 = {}
        main_dict3 = {}
        main_dict1['name'] = 'old_pwd'
        main_dict1['value'] = fields.get('old_pwd')
        main_list1.append(main_dict1)
        main_dict2['name'] = 'new_password'
        main_dict2['value'] = fields.get('new_password')
        main_list1.append(main_dict2)
        main_dict3['name'] = 'confirm_pwd'
        main_dict3['value'] = fields.get('confirm_pwd')
        main_list1.append(main_dict3)

        old_password, new_password,confirm_password = operator.itemgetter('old_pwd', 'new_password','confirm_pwd')(
                dict(map(operator.itemgetter('name', 'value'), main_list1)))
        if new_password != confirm_password:
            question_obj = pool['settings.page.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)

            values={
                'questions':questions,
                'pass_not_match':'New password and Confirm password do not match!',
            }
            return request.website.render("website.settings", values)
        try:
            if request.session.model('res.users').change_password(
                old_password, new_password):
                return werkzeug.utils.redirect('/page/login')
        except Exception:
            question_obj = pool['settings.page.questions.faq']
            question_ids = question_obj.search(cr, SUPERUSER_ID, [], offset=0, limit=1,context=context)
            questions = question_obj.browse(cr, SUPERUSER_ID, question_ids,context=context)

            values={
                'questions':questions,
                'old_pass_false':'Wrong Current Password!',
            }
            return request.website.render("website.settings", values)
        return werkzeug.utils.redirect('/page/login')

    @http.route('/page/settings/validate_signature', type='http', auth="public", website=True)
    def setting_save_signature(self,  **fields):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res_users= pool.get('res.users')
        res_partner= pool.get('res.partner')
        res_users_data=res_users.search(cr, uid,[('id','=',uid)],context=context)
        data = res_users.browse(cr,uid,res_users_data,context=context)
        res_partner_data=res_partner.search(cr, uid,[('id','=',data.partner_id.id)],context=context)
        partner_data = res_partner.browse(cr,uid,res_partner_data,context=context)

        user_setting=pool.get('user.setting')

        current_signature=fields.get('sample_signature')
        first_name=fields.get('chk_first_name_sign')
        last_name=fields.get('chk_surname_sign')
        company_name=fields.get('chk_company_sign')
        hdn_user_id=fields.get('hdn_user_id')
        values1={
            'res_id':int(uid),
            'first_name':first_name,
            'last_name':last_name,
            'company_name':company_name,
        }
        if hdn_user_id:
            edit_data=user_setting.write(cr,SUPERUSER_ID,int(hdn_user_id),values1,context=context)
            values={
                'signature':current_signature,
            }
            save_sign=res_partner.write(cr,SUPERUSER_ID,int(partner_data.id),values,context=context)
            return request.redirect("/page/settings")
        else:
            save_data=user_setting.create(cr,SUPERUSER_ID,values1,context=context)
            values={
                'signature':current_signature,
            }
            save_sign=res_partner.write(cr,SUPERUSER_ID,int(partner_data.id),values,context=context)
            return request.redirect("/page/settings")

    @http.route('/page/settings/notification', type='http', auth="public", website=True)
    def setting_save_notification(self,  **fields):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user_setting=pool.get('user.setting')
        res_user_obj=pool.get('res.users')
        res_group=pool.get('res.groups')
        blog_post=pool.get('blog.post')
        res_partner= pool.get('res.partner')

        res_users_search=res_user_obj.search(cr,uid,[('id','=',int(uid))],context=context)
        print"----------------------res_users_search----------",res_users_search
        res_users_data=res_user_obj.search(cr, uid,[('id','=',uid)],context=context)
        data = res_user_obj.browse(cr,uid,res_users_data,context=context)
        res_partner_data=res_partner.search(cr, uid,[('id','=',data.partner_id.id)],context=context)
        partner_data = res_partner.browse(cr,uid,res_partner_data,context=context)


        new_post_chk_value=fields.get('new_post_chk')
        already_comment_chk_value=fields.get('already_comment_chk')
        new_comment_chk_value=fields.get('new_comment_chk')
        hdn_user_id=fields.get('hdn_user_id')
        user_id=int(uid)

        values={
            'res_id':user_id,
            'new_post_notice':new_post_chk_value,
            'new_comment_notice':new_comment_chk_value,
            'already_comment_notice':already_comment_chk_value,
        }
        if hdn_user_id:
            edit_data=user_setting.write(cr,SUPERUSER_ID,int(hdn_user_id),values,context=context)
            group_id_search=res_group.search(cr,SUPERUSER_ID,[('name','=','Blog Notifications')],context=context)

            # --------Added follower when a new comment on a blog---------------
            user_setting_ids=user_setting.search(cr,SUPERUSER_ID,[('res_id','=',int(uid)),('new_comment_notice','=',True)])
            if user_setting_ids:
                blog_ids= blog_post.search(cr, SUPERUSER_ID, [('author_id','=',int(partner_data.id)),('state','=','published')], context=context)
                blog_id= blog_post.browse(cr, SUPERUSER_ID, blog_ids, context=context)
                follower_list=[]
                for blog_number in blog_id:
                    for follow_id in blog_number.message_follower_ids:
                        follower_list.append(int(follow_id.id))
                    if int(partner_data.id) not in follower_list:
                        blog_follower_values={
                            'message_follower_ids':[(4, int(partner_data.id))],
                        }
                        add_follower=blog_post.write(cr,SUPERUSER_ID,int(blog_number.id),blog_follower_values,context=context)
            if new_post_chk_value or new_comment_chk_value or already_comment_chk_value:
                test=res_group.write(cr, SUPERUSER_ID,group_id_search, {'users': [(4,user) for user in res_users_search]}, context=context)
            return request.redirect("/page/settings")
        else:
            save_data=user_setting.create(cr,SUPERUSER_ID,values,context=context)
            group_id_search=res_group.search(cr,SUPERUSER_ID,[('name','=','Blog Notifications')],context=context)

            # --------Added follower when a new comment on a blog---------------
            user_setting_ids=user_setting.search(cr,SUPERUSER_ID,[('res_id','=',int(uid)),('new_comment_notice','=',True)])

            if user_setting_ids:
                blog_ids= blog_post.search(cr, SUPERUSER_ID, [('author_id','=',int(partner_data.id)),('state','=','published')], context=context)
                blog_id= blog_post.browse(cr, SUPERUSER_ID, blog_ids, context=context)
                follower_list=[]
                for blog_number in blog_id:
                    for follow_id in blog_number.message_follower_ids:
                        follower_list.append(int(follow_id.id))
                    if int(partner_data.id) not in follower_list:
                        blog_follower_values={
                            'message_follower_ids':[(4, int(partner_data.id))],
                        }
                        add_follower=blog_post.write(cr,SUPERUSER_ID,int(blog_number.id),blog_follower_values,context=context)

            if new_post_chk_value or new_comment_chk_value or already_comment_chk_value:
                test=res_group.write(cr, SUPERUSER_ID,group_id_search, {'users': [(4,user) for user in res_users_search]}, context=context)
            return request.redirect("/page/settings")

    @http.route(['/page/test/quantity'], type='json', auth="public", website=True)
    def update_order_cart_quantity(self, page=0, category=None, search='', **post):
        cr,uid,context,pool = request.cr, request.uid, request.context, request.registry

        quantity_call = post.get('quantity')
        product_product = pool.get('product.product')
        product_template = pool.get('product.template')
        product_pricelist=pool.get('product.pricelist')
        sale_order_line_obj = pool.get('sale.order.line')
        sale_order_line_id = post.get('sale_order_line')
        sale_order_line_id = int(sale_order_line_id)
        sale_order_line_data = sale_order_line_obj.browse(cr,uid,sale_order_line_id,context=context)
        product_id = sale_order_line_data.product_id
        product_attribute_value_ids = product_id.attribute_value_ids
        for one_id in product_id.attribute_value_ids:
            if(one_id.attribute_id.name=='Length'):
                length_value = one_id.name
        adjusted_qty = float(quantity_call)/float(length_value)
        rounded_qty = math.ceil(adjusted_qty)*float(length_value)
        rounded_qty = math.ceil(rounded_qty)
        test = sale_order_line_obj.write(cr, uid, int(sale_order_line_id), {'product_uom_qty':rounded_qty}, context=context)

        ## Set price data based on rule -- START
        product_sale_order_line=sale_order_line_obj.search(cr,uid,[('id','=',int(sale_order_line_id))],context=context)
        get_product_sale_order_line=sale_order_line_obj.browse(cr,uid,int(product_sale_order_line[0]),context=context)

        product_id_sale_order_line=product_product.search(cr,uid,[('id','=',int(get_product_sale_order_line.product_id))],context=context)
        get_product_id=product_product.browse(cr,uid,int(product_id_sale_order_line[0]),context=context)

        product_template_id=product_template.search(cr,uid,[('id','=',int(get_product_id.product_tmpl_id))],context=context)
        get_product_template_id=product_template.browse(cr,uid,int(product_template_id[0]),context=context)
        product_template_categ_id=get_product_template_id.categ_id.id

        default_priclist=product_pricelist.search(cr,uid,[('default','=',True)],context=context)
        get_default_priclist=product_pricelist.browse(cr,uid,int(default_priclist[0]),context=context)
        for ll in get_default_priclist.version_id:
            for vv in ll.items_id:
                if int(vv.categ_id.id)==int(product_template_categ_id) and float(vv.min_quantity)<float(get_product_sale_order_line.product_uom_qty):
                    new_price=float(get_product_sale_order_line.duplicate_unit_price)*(1+float(vv.price_discount))+float(vv.price_surcharge)
                    sale_order_line_obj.write(cr, uid, int(sale_order_line_id), {'price_unit':float(new_price),'additional_amount':float(vv.fixed_amount)}, context=context)
                    break
        # get_default_priclist.version_id.
        return rounded_qty

    @http.route(['/page/about_us'], type='http', auth="public", website=True)
    def about_us_page_contents(self, **post):
        cr,context,pool = request.cr, request.context, request.registry
        about_us = pool.get('aboutus.content')

        about_us_content_id = about_us.search(cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        about_us_content=about_us.browse(cr, SUPERUSER_ID, about_us_content_id, context=context)

        values = {
                  'about_us_content':about_us_content,
                  }
        return request.website.render("website.about_us",values)

    @http.route(['/page/legal_stuff'], type='http', auth="public", website=True)
    def legal_stuff_page_contents(self, **post):
        cr,context,pool = request.cr, request.context, request.registry
        legal_stuff = pool.get('legal.stuff.content')

        legal_stuff_content_id = legal_stuff.search(cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        legal_stuff_content=legal_stuff.browse(cr, SUPERUSER_ID, legal_stuff_content_id, context=context)

        values = {
                  'legal_stuff_content':legal_stuff_content,
                  }
        return request.website.render("website.legal_stuff",values)

    @http.route(['/page/privacy_policy'], type='http', auth="public", website=True)
    def privacy_policy_page_contents(self, **post):
        cr,context,pool = request.cr, request.context, request.registry
        privacy_policy = pool.get('privacy.policy.content')

        privacy_policy_content_id = privacy_policy.search(cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        privacy_policy_content=privacy_policy.browse(cr, SUPERUSER_ID, privacy_policy_content_id, context=context)

        values = {
                  'privacy_policy_content':privacy_policy_content,
                  }
        return request.website.render("website.privacy_policy",values)

    @http.route(['/page/ultimate-thermal-performance-of-aluminium-windows-with-thermevo-Evobreak-Pur-FOAM-solutions'], type='http', auth="public", website=True)
    def view_foam_page(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        comment_pool = pool.get('foam.comment')
        comment_ids = comment_pool.search(cr, uid, [('state','=', True), ('parent_id', '=', '0'), ('page','=', 'foam')], context=context)
        comments = comment_pool.browse(cr, uid, comment_ids, context=context)
        d_comment_ids = comment_pool.search(cr, uid, [('state','=', True), ('parent_id', '!=', '0')], context=context)
        d_comments = comment_pool.browse(cr, uid, d_comment_ids, context=context)

        values = {'comments': comments,
                  'd_comments': d_comments}

        return request.website.render("website.foam", values)

    @http.route(['/page/ultimate-thermal-performance-of-aluminium-windows-with-thermevo-Evobreak-Pur-FOAM-solutions/get_sample_foam'], type='http', auth="public", website=True)
    def get_sample_foam(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res_user_pool =pool.get('res.users')
        user = res_user_pool.browse(cr, uid, uid, context=context)
        partner = user.partner_id
        if user.login == 'public':
            uid = SUPERUSER_ID

        crm_lead_pool = pool.get('crm.lead')
        fullname = post.get('first_name') + " " + post.get('last_name')
        vals = {
            'contact_name': fullname,
            'email_from': post.get('email'),
            'user_id': uid,
            'partner_name': 'THERMEVO',
            'name': 'Interested with foam samples',
            'description': 'New lead is interested with foam samples',
        }
        crm_lead_pool.create(cr, uid, vals, context=context)

        res_partner_pool = pool.get('res.partner')
        res_partner_id = res_partner_pool.search(cr, SUPERUSER_ID, [('email', '=', post.get('email'))])
        if res_partner_id:
            res_user_id = res_user_pool.search(cr, SUPERUSER_ID, [('partner_id','in', res_partner_id), ('active', '=', True)])
            if res_user_id and user.login == 'public':
                user_id = res_user_id
            else:
                user_id = SUPERUSER_ID
        else:
            vals = {
                'email': post.get('email'),
                'name': fullname
            }
            res_partner_pool = pool.get('res.partner')
            res_partner_pool.create(cr, SUPERUSER_ID, vals, context=context)
            user_id = SUPERUSER_ID
        template_name = 'TriggerToFoamLongread'
        # mail = partner.subscribe_verification_mail(user_id, template_name, context=context)

    @http.route(['/page/ultimate-thermal-performance-of-aluminium-windows-with-thermevo-Evobreak-Pur-FOAM-solutions/add_foam_comment'], type='http', auth='user', website=True)
    def add_foam_comment(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        pool_comment = pool.get('foam.comment')
        vals = {
            'user_id': uid,
            'comment_text': post.get('comment_text'),
            'parent_id': post.get('parent_id'),
            'page': 'foam',
        }
        pool_comment.create(cr, uid, vals, context=context)
        return redirect("/page/ultimate-thermal-performance-of-aluminium-windows-with-thermevo-Evobreak-Pur-FOAM-solutions")

    @http.route(['/promo_page'], type='http', auth='public', website=True)
    def promo_page(self):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        promo_pool = pool.get('promo.page')

        promo_id = promo_pool.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        promo_page_content = promo_pool.browse(
            cr, SUPERUSER_ID, promo_id, context=context)

        providers = OAuthLogin_inherit().list_providers()
        providers = sorted(providers, key=lambda k: k['sequence_website'])

        question_obj = request.registry['login.faq']
        question_ids = question_obj.search(request.cr, SUPERUSER_ID, [],
                                           offset=0, limit=1, context=context)
        login_questions = question_obj.browse(request.cr, SUPERUSER_ID,
                                              question_ids, context=context)

        content_obj = request.registry['login.common.content']
        content_ids = content_obj.search(request.cr, SUPERUSER_ID, [], offset=0,
                                         limit=1, context=context)
        login_content = content_obj.browse(request.cr, SUPERUSER_ID,
                                           content_ids, context=context)

        values = {
            'promo_page_content': promo_page_content,
            'providers': providers,
            'token': '',
            'login': '',
            'login_questions': login_questions,
            'login_content': login_content,
            'user_login': True,
        }
        return request.website.render("website.promo_page", values)


class table_compute(object):
    def __init__(self):
        self.table = {}

    def _check_place(self, posx, posy, sizex, sizey):
        res = True
        for y in range(sizey):
            for x in range(sizex):
                if posx+x>=PPR:
                    res = False
                    break
                row = self.table.setdefault(posy+y, {})
                if row.setdefault(posx+x) is not None:
                    res = False
                    break
            for x in range(PPR):
                self.table[posy+y].setdefault(x, None)
        return res

    def process(self, products):
        # Compute products positions on the grid
        minpos = 0
        index = 0
        maxy = 0
        for p in products:
            x = min(max(p.website_size_x, 1), PPR)
            y = min(max(p.website_size_y, 1), PPR)
            if index >= PPG:
                x = y = 1

            pos = minpos
            while not self._check_place(pos % PPR, pos / PPR, x, y):
                pos += 1
            # if 21st products (index 20) and the last line
            # is full (PPR products in it), break
            # (pos + 1.0) / PPR is the line where the product would be inserted
            # maxy is the number of existing lines
            # + 1.0 is because pos begins at 0, thus pos 20
            # is actually the 21st block
            # and to force python to not round the division operation
            if index >= PPG and ((pos + 1.0) / PPR) > maxy:
                break

            if x == 1 and y == 1:  # simple heuristic for CPU optimization
                minpos = pos / PPR

            for y2 in range(y):
                for x2 in range(x):
                    self.table[(pos / PPR) + y2][(pos % PPR) + x2] = False
            self.table[pos / PPR][pos % PPR] = {
                'product': p, 'x': x, 'y': y,
                'class': " ".join(map(
                    lambda x: x.html_class or '', p.website_style_ids))
            }
            if index <= PPG:
                maxy = max(maxy, y + (pos / PPR))
            index += 1

        # Format table according to HTML needs
        rows = self.table.items()
        rows.sort()
        rows = map(lambda x: x[1], rows)
        for col in range(len(rows)):
            cols = rows[col].items()
            cols.sort()
            x += len(cols)
            rows[col] = [c for c in map(lambda x: x[1], cols) if c != False]

        return rows

        # TODO keep with input type hidden


class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k, v in self.args.items():
            kw.setdefault(k, v)
        l = []
        for k, v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k, i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k, v)]))
        if l:
            path += '?' + '&'.join(l)
        return path


def get_pricelist():
    cr = request.cr
    uid = request.uid
    context = request.context
    pool = request.registry
    sale_order = context.get('sale_order')
    if sale_order:
        pricelist = sale_order.pricelist_id
    else:
        partner = pool['res.users'].browse(
            cr, SUPERUSER_ID, uid, context=context).partner_id
        pricelist = partner.property_product_pricelist
    return pricelist
