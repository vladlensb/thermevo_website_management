# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import fields, models, api


class PromoPage(models.Model):
    _name = 'promo.page'
    _rec_name = 'link'

    link = fields.Char(string='Link',
                       required=True)

    first_content = fields.Html(string='First Content',
                                translate=True,
                                default='First Content')
    second_content = fields.Html(string='Second Content',
                                 translate=True,
                                 default='Second Content')
    third_content = fields.Html(string='Third Content',
                                translate=True,
                                default='Third Content')
    four_content = fields.Html(string='Four Content',
                               translate=True,
                               default='Four Content')

    five_content = fields.Html(string='Five Content',
                               translate=True,
                               default='Five Content')
    six_content = fields.Html(string='Six Content',
                              translate=True,
                              default='Six Content')
    seven_content = fields.Html(string='Seven Content',
                                translate=True,
                                default='Seven Content')
    eight_content = fields.Html(string='Eight Content',
                                translate=True,
                                default='Eight Content')

    header_phone_1 = fields.Char(string='Header Phone 1',
                                 translate=True,
                                 default='1234567890')
    header_phone_2 = fields.Char(string='Header Phone 2',
                                 translate=True,
                                 default='0987654321')

    text_1 = fields.Char(string='Text 1',
                         translate=True,
                         default='Text 1')
    text_2 = fields.Char(string='Text 2',
                         translate=True,
                         default='Text 2')
    text_3 = fields.Char(string='Text 3',
                         translate=True,
                         default='Text 3')
    text_4 = fields.Char(string='Text 4',
                         translate=True,
                         default='Text 4')
    text_5 = fields.Char(string='Text 5',
                         translate=True,
                         default='Text 5')
    text_6 = fields.Char(string='Text 6',
                         translate=True,
                         default='Text 6')
    text_7 = fields.Char(string='Text 7',
                         translate=True,
                         default='Text 7')
    text_8 = fields.Char(string='Text 8',
                         translate=True,
                         default='Text 8')
    text_9 = fields.Char(string='Text 9',
                         translate=True,
                         default='Text 9')
    text_10 = fields.Char(string='Text 10',
                          translate=True,
                          default='Text 10')
    text_11 = fields.Char(string='Text 11',
                          translate=True,
                          default='Text 11')
    text_12 = fields.Char(string='Text 12',
                          translate=True,
                          default='Text 12')
    text_13 = fields.Char(string='Text 13',
                          translate=True,
                          default='Text 13')
    text_14 = fields.Char(string='Text 14',
                          translate=True,
                          default='Text 14')

    button_1 = fields.Char(string='Button 1',
                           translate=True,
                           default='Button 1')
    button_2 = fields.Char(string='Button 2',
                           translate=True,
                           default='Button 2')
    button_3 = fields.Char(string='Button 3',
                           translate=True,
                           default='TeButtonxt 3')

    link_1 = fields.Char(string='Link 1',
                         translate=True,
                         default='Link 1')
    link_2 = fields.Char(string='Link 2',
                         translate=True,
                         default='Link 2')
    link_3 = fields.Char(string='Link 3',
                         translate=True,
                         default='Link 3')


PromoPage()
